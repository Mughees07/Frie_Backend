-- ************************************** "user_auth"

CREATE TABLE "user_auth"
(
 "id"           int NOT NULL,
 "email"        varchar(100) NOT NULL,
 "password"     varchar(100) NOT NULL,
 "social_login" varchar(200) NOT NULL,
 "social_value" varchar(500) NOT NULL

);

CREATE UNIQUE INDEX "PK_user_auth" ON "user_auth"
(
 "id"
);

-- ************************************** "customer_card"

CREATE TABLE "customer_card"
(
 "id"           int NOT NULL,
 "stripe_token" varchar(500) NOT NULL,
 "last_4digits" int NOT NULL,
 "card_type"    varchar(50) NOT NULL

);

CREATE UNIQUE INDEX "PK_customer_cardtype" ON "customer_card"
(
 "id"
);

-- ************************************** "customer_profile"

CREATE TABLE "customer_profile"
(
 "id"               int NOT NULL,
 "name"             varchar(200) NOT NULL,
 "username"         varchar(200) NOT NULL,
 "address"          varchar(500) NOT NULL,
 "about"            varchar(500) NOT NULL,
 "profile_image"    varchar(500) NOT NULL,
 "user_auth_id"     int NOT NULL,
 "customer_card_id" int NOT NULL,
 CONSTRAINT "FK_214" FOREIGN KEY ( "customer_card_id" ) REFERENCES "customer_card" ( "id" ),
 CONSTRAINT "FK_42" FOREIGN KEY ( "user_auth_id" ) REFERENCES "user_auth" ( "id" )
);

CREATE UNIQUE INDEX "PK_customer_profile" ON "customer_profile"
(
 "id"
);

CREATE INDEX "fkIdx_214" ON "customer_profile"
(
 "customer_card_id"
);

CREATE INDEX "fkIdx_42" ON "customer_profile"
(
 "user_auth_id"
);

-- ************************************** "addresses"

CREATE TABLE "addresses"
(
 "id"      int NOT NULL,
 "street"  varchar(100) NOT NULL,
 "area"    varchar(500) NOT NULL,
 "city"    varchar(200) NOT NULL,
 "state"   varchar(200) NOT NULL,
 "zipcode" int NOT NULL,
 "country" varchar(200) NOT NULL,
 "long"    decimal NOT NULL,
 "lat"     decimal NOT NULL

);

CREATE UNIQUE INDEX "PK_addresses" ON "addresses"
(
 "id"
);

-- ************************************** "customer_addresses"

CREATE TABLE "customer_addresses"
(
 "id"           int NOT NULL,
 "addresses_id" int NOT NULL,
 "customer_id"  int NOT NULL,
 CONSTRAINT "FK_411" FOREIGN KEY ( "addresses_id" ) REFERENCES "addresses" ( "id" ),
 CONSTRAINT "FK_414" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" )
);

CREATE UNIQUE INDEX "PK_customer_addresses" ON "customer_addresses"
(
 "id"
);

CREATE INDEX "fkIdx_411" ON "customer_addresses"
(
 "addresses_id"
);

CREATE INDEX "fkIdx_414" ON "customer_addresses"
(
 "customer_id"
);

-- ************************************** "driver_payment"

CREATE TABLE "driver_payment"
(
 "id"         int NOT NULL,
 "routing_no" int NOT NULL

);

CREATE UNIQUE INDEX "PK_driver_payment" ON "driver_payment"
(
 "id"
);

-- ************************************** "areas"

CREATE TABLE "areas"
(
 "id"        int NOT NULL,
 "area_name" varchar(200) NOT NULL

);

CREATE UNIQUE INDEX "PK_areas" ON "areas"
(
 "id"
);

-- ************************************** "delivery_areas"

CREATE TABLE "delivery_areas"
(
 "id"      int NOT NULL,
 "area_id" int NOT NULL,
 CONSTRAINT "FK_48" FOREIGN KEY ( "area_id" ) REFERENCES "areas" ( "id" )
);

CREATE UNIQUE INDEX "PK_delivery_areas" ON "delivery_areas"
(
 "id"
);

CREATE INDEX "fkIdx_48" ON "delivery_areas"
(
 "area_id"
);

-- ************************************** "driver_profile"

CREATE TABLE "driver_profile"
(
 "Id"                int NOT NULL,
 "ref_code"          varchar(50) NOT NULL,
 "profile_image"     varchar(500) NOT NULL,
 "vehicle"           int NOT NULL,
 "first_name"        varchar(200) NOT NULL,
 "last_name"         varchar(200) NOT NULL,
 "photo_verified"    boolean NOT NULL,
 "background_check"  boolean NOT NULL,
 "driver_payment_id" int NOT NULL,
 CONSTRAINT "FK_396" FOREIGN KEY ( "driver_payment_id" ) REFERENCES "driver_payment" ( "id" )
);

CREATE UNIQUE INDEX "PK_driver_profile" ON "driver_profile"
(
 "Id"
);

CREATE INDEX "fkIdx_396" ON "driver_profile"
(
 "driver_payment_id"
);

-- ************************************** "restaurant"

CREATE TABLE "restaurant"
(
 "id"           int NOT NULL,
 "name"         varchar(200) NOT NULL,
 "address"      varchar(500) NOT NULL,
 "long"         decimal NOT NULL,
 "lat"          decimal NOT NULL,
 "rating"       decimal NOT NULL,
 "opening_time" time NOT NULL,
 "closing_time" time NOT NULL,
 "phone"        int NOT NULL,
 "image_url"    varchar(500) NOT NULL,
 "capacity"     int NOT NULL

);

CREATE UNIQUE INDEX "PK_restaurant" ON "restaurant"
(
 "id"
);








-- ************************************** "restaurant_addresses"

CREATE TABLE "restaurant_addresses"
(
 "id"            int NOT NULL,
 "addresses_id"  int NOT NULL,
 "restaurant_id" int NOT NULL,
 CONSTRAINT "FK_420" FOREIGN KEY ( "addresses_id" ) REFERENCES "addresses" ( "id" ),
 CONSTRAINT "FK_423" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" )
);

CREATE UNIQUE INDEX "PK_restaurant_addresses" ON "restaurant_addresses"
(
 "id"
);

CREATE INDEX "fkIdx_420" ON "restaurant_addresses"
(
 "addresses_id"
);

CREATE INDEX "fkIdx_423" ON "restaurant_addresses"
(
 "restaurant_id"
);

-- ************************************** "cuisine"

CREATE TABLE "cuisine"
(
 "id"        int NOT NULL,
 "name"      varchar(200) NOT NULL,
 "image_url" varchar(500) NOT NULL

);

CREATE UNIQUE INDEX "PK_cuisine" ON "cuisine"
(
 "id"
);

-- ************************************** "dishes"

CREATE TABLE "dishes"
(
 "id"         int NOT NULL,
 "name"       varchar(200) NOT NULL,
 "image_url"  varchar(500) NOT NULL,
 "cuisine_id" int NOT NULL,
 CONSTRAINT "FK_130" FOREIGN KEY ( "cuisine_id" ) REFERENCES "cuisine" ( "id" )
);

CREATE UNIQUE INDEX "PK_Dishes" ON "dishes"
(
 "id"
);

CREATE INDEX "fkIdx_130" ON "dishes"
(
 "cuisine_id"
);

-- ************************************** "restaurant_cuisines"

CREATE TABLE "restaurant_cuisines"
(
 "id"            int NOT NULL,
 "cuisine_id"    int NOT NULL,
 "restaurant_id" int NOT NULL,
 CONSTRAINT "FK_102" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" ),
 CONSTRAINT "FK_99" FOREIGN KEY ( "cuisine_id" ) REFERENCES "cuisine" ( "id" )
);

CREATE UNIQUE INDEX "PK_restaurant_cuisines" ON "restaurant_cuisines"
(
 "id"
);

CREATE INDEX "fkIdx_102" ON "restaurant_cuisines"
(
 "restaurant_id"
);

CREATE INDEX "fkIdx_99" ON "restaurant_cuisines"
(
 "cuisine_id"
);

-- ************************************** "restaurant_dishes"

CREATE TABLE "restaurant_dishes"
(
 "id"            int NOT NULL,
 "description"   varchar(500) NOT NULL,
 "price"         int NOT NULL,
 "dish_id"       int NOT NULL,
 "restaurant_id" int NOT NULL,
 CONSTRAINT "FK_108" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" ),
 CONSTRAINT "FK_88" FOREIGN KEY ( "dish_id" ) REFERENCES "dishes" ( "id" )
);

CREATE UNIQUE INDEX "PK_restaurant_dishes" ON "restaurant_dishes"
(
 "id"
);

CREATE INDEX "fkIdx_108" ON "restaurant_dishes"
(
 "restaurant_id"
);

CREATE INDEX "fkIdx_88" ON "restaurant_dishes"
(
 "dish_id"
);

-- ************************************** "order"

CREATE TABLE "order"
(
 "id"                    int NOT NULL,
 "price"                 decimal NOT NULL,
 "instructions"          varchar(500) NOT NULL,
 "status"                int NOT NULL,
 "restaurant_id"         int NOT NULL,
 "customer_id"           int NOT NULL,
 "customer_addresses_id" int NOT NULL,
 "dish_id"               int NOT NULL,
 CONSTRAINT "FK_139" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" ),
 CONSTRAINT "FK_142" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" ),
 CONSTRAINT "FK_145" FOREIGN KEY ( "dish_id" ) REFERENCES "dishes" ( "id" ),
 CONSTRAINT "FK_445" FOREIGN KEY ( "customer_addresses_id" ) REFERENCES "customer_addresses" ( "id" )
);

CREATE UNIQUE INDEX "PK_order" ON "order"
(
 "id"
);

CREATE INDEX "fkIdx_139" ON "order"
(
 "restaurant_id"
);

CREATE INDEX "fkIdx_142" ON "order"
(
 "customer_id"
);

CREATE INDEX "fkIdx_145" ON "order"
(
 "dish_id"
);

CREATE INDEX "fkIdx_445" ON "order"
(
 "customer_addresses_id"
);

COMMENT ON TABLE "order" IS 'Status {0:Placed,1:Ready,2:Picked,3:Delivered}';

COMMENT ON COLUMN "order"."status" IS '0:Placed
1:Ready
2:Picked
3:Delivered';


CREATE TABLE "cuisine_topping"
(
 "id"      int NOT NULL,
 "topping" varchar(200) NOT NULL

);

CREATE UNIQUE INDEX "PK_cuisine_topping" ON "cuisine_topping"
(
 "id"
);

-- ************************************** "cuisine_style"

CREATE TABLE "cuisine_style"
(
 "id"    int NOT NULL,
 "style" varchar(200) NOT NULL

);

CREATE UNIQUE INDEX "PK_cuisine_style" ON "cuisine_style"
(
 "id"
);

-- ************************************** "dishes_collection"

CREATE TABLE "dishes_collection"
(
 "id"   int NOT NULL,
 "name" varchar(200) NOT NULL

);

CREATE UNIQUE INDEX "PK_collections" ON "dishes_collection"
(
 "id"
);

-- ************************************** "dishes_collections_categories"

CREATE TABLE "dishes_collections_categories"
(
 "id"                   int NOT NULL,
 "dishes_collection_id" int NOT NULL,
 "restaurant_dishes_id" int NOT NULL,
 CONSTRAINT "FK_189" FOREIGN KEY ( "dishes_collection_id" ) REFERENCES "dishes_collection" ( "id" ),
 CONSTRAINT "FK_192" FOREIGN KEY ( "restaurant_dishes_id" ) REFERENCES "restaurant_dishes" ( "id" )
);

CREATE UNIQUE INDEX "PK_restaurant_dishes_colletion" ON "dishes_collections_categories"
(
 "id"
);

CREATE INDEX "fkIdx_189" ON "dishes_collections_categories"
(
 "dishes_collection_id"
);

CREATE INDEX "fkIdx_192" ON "dishes_collections_categories"
(
 "restaurant_dishes_id"
);

-- ************************************** "trips"

CREATE TABLE "trips"
(
 "id"        int NOT NULL,
 "status"    int NOT NULL,
 "distance"  decimal NOT NULL,
 "earning"   decimal NOT NULL,
 "order_id"  int NOT NULL,
 "driver_id" int NOT NULL,
 CONSTRAINT "FK_219" FOREIGN KEY ( "order_id" ) REFERENCES "order" ( "id" ),
 CONSTRAINT "FK_222" FOREIGN KEY ( "driver_id" ) REFERENCES "driver_profile" ( "Id" )
);

CREATE UNIQUE INDEX "PK_trips" ON "trips"
(
 "id"
);

CREATE INDEX "fkIdx_219" ON "trips"
(
 "order_id"
);

CREATE INDEX "fkIdx_222" ON "trips"
(
 "driver_id"
);



COMMENT ON COLUMN "trips"."status" IS '0:Arrived
1:On Route
2:Completed';

-- ************************************** "order_dishes"

CREATE TABLE "order_dishes"
(
 "id"         int NOT NULL,
 "quantity"   int NOT NULL,
 "order_id"   int NOT NULL,
 "dish_id"    int NOT NULL,
 "topping_id" int NOT NULL,
 "style_id"   int NOT NULL,
 CONSTRAINT "FK_202" FOREIGN KEY ( "order_id" ) REFERENCES "order" ( "id" ),
 CONSTRAINT "FK_205" FOREIGN KEY ( "dish_id" ) REFERENCES "dishes" ( "id" ),
 CONSTRAINT "FK_208" FOREIGN KEY ( "topping_id" ) REFERENCES "cuisine_topping" ( "id" ),
 CONSTRAINT "FK_211" FOREIGN KEY ( "style_id" ) REFERENCES "cuisine_style" ( "id" )
);

CREATE UNIQUE INDEX "PK_order_dishes" ON "order_dishes"
(
 "id"
);

CREATE INDEX "fkIdx_202" ON "order_dishes"
(
 "order_id"
);

CREATE INDEX "fkIdx_205" ON "order_dishes"
(
 "dish_id"
);

CREATE INDEX "fkIdx_208" ON "order_dishes"
(
 "topping_id"
);

CREATE INDEX "fkIdx_211" ON "order_dishes"
(
 "style_id"
);

-- ************************************** "restaurant_collections_categories"

CREATE TABLE "restaurant_collections_categories"
(
 "id"   int NOT NULL,
 "name" varchar(200) NOT NULL

);

CREATE UNIQUE INDEX "PK_restaurant_collections_categories" ON "restaurant_collections_categories"
(
 "id"
);

-- ************************************** "restaurant_collection"

CREATE TABLE "restaurant_collection"
(
 "id"                                   int NOT NULL,
 "restaurant_collections_categories_id" int NOT NULL,
 "restaurant_id"                        int NOT NULL,
 CONSTRAINT "FK_232" FOREIGN KEY ( "restaurant_collections_categories_id" ) REFERENCES "restaurant_collections_categories" ( "id" ),
 CONSTRAINT "FK_235" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" )
);

CREATE UNIQUE INDEX "PK_restaurant_collection" ON "restaurant_collection"
(
 "id"
);

CREATE INDEX "fkIdx_232" ON "restaurant_collection"
(
 "restaurant_collections_categories_id"
);

CREATE INDEX "fkIdx_235" ON "restaurant_collection"
(
 "restaurant_id"
);

-- ************************************** "events"

CREATE TABLE "events"
(
 "id"            int NOT NULL,
 "ritle"         varchar(200) NOT NULL,
 "rime"          time NOT NULL,
 "date"          date NOT NULL,
 "ticket_price"  decimal NOT NULL,
 "description"   varchar(500) NOT NULL,
 "address"       varchar(500) NOT NULL,
 "long"          decimal NOT NULL,
 "lat"           decimal NOT NULL,
 "image_url"     varchar(500) NOT NULL,
 "restaurant_id" int NOT NULL,
 CONSTRAINT "FK_384" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" )
);

CREATE UNIQUE INDEX "PK_events" ON "events"
(
 "id"
);

CREATE INDEX "fkIdx_384" ON "events"
(
 "restaurant_id"
);

-- ************************************** "reviews_event"

CREATE TABLE "reviews_event"
(
 "id"          int NOT NULL,
 "image_url"   varchar(500) NOT NULL,
 "rating"      int NOT NULL,
 "comment"     varchar(500) NOT NULL,
 "customer_id" int NOT NULL,
 "event_id"    int NOT NULL,
 CONSTRAINT "FK_253" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" ),
 CONSTRAINT "FK_284" FOREIGN KEY ( "event_id" ) REFERENCES "events" ( "id" )
);

CREATE UNIQUE INDEX "PK_reviews_event" ON "reviews_event"
(
 "id"
);

CREATE INDEX "fkIdx_2543" ON "reviews_event"
(
 "customer_id"
);

CREATE INDEX "fkIdx_284" ON "reviews_event"
(
 "event_id"
);

-- ************************************** "reviews_restaurant"

CREATE TABLE "reviews_restaurant"
(
 "id"            int NOT NULL,
 "image_url"     varchar(500) NOT NULL,
 "rating"        int NOT NULL,
 "comment"       varchar(500) NOT NULL,
 "restaurant_id" int NOT NULL,
 "customer_id"   int NOT NULL,
 CONSTRAINT "FK_250" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" ),
 CONSTRAINT "FK_253" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" )
);

CREATE UNIQUE INDEX "PK_reviews" ON "reviews_restaurant"
(
 "id"
);

CREATE INDEX "fkIdx_250" ON "reviews_restaurant"
(
 "restaurant_id"
);

CREATE INDEX "fkIdx_253" ON "reviews_restaurant"
(
 "customer_id"
);

-- ************************************** "table_booking"

CREATE TABLE "table_booking"
(
 "id"              int NOT NULL,
 "time"            time NOT NULL,
 "date"            date NOT NULL,
 "number_of_guest" int NOT NULL,
 "status"          int NOT NULL,
 "restaurant_id"   int NOT NULL,
 "customer_id"     int NOT NULL,
 CONSTRAINT "FK_329" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" ),
 CONSTRAINT "FK_332" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" )
);

CREATE UNIQUE INDEX "PK_table_booking" ON "table_booking"
(
 "id"
);

CREATE INDEX "fkIdx_329" ON "table_booking"
(
 "restaurant_id"
);

CREATE INDEX "fkIdx_332" ON "table_booking"
(
 "customer_id"
);



COMMENT ON COLUMN "table_booking"."status" IS '0:Requested
1:Reserved';

-- ************************************** "favourite_restaurants"

CREATE TABLE "favourite_restaurants"
(
 "id"          int NOT NULL,
 "ref_type"    varchar(200) NOT NULL,
 "ref_id"      int NOT NULL,
 "type"        varchar(200) NOT NULL,
 "customer_id" int NOT NULL,
 CONSTRAINT "FK_290" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" )
);

CREATE UNIQUE INDEX "PK_favourite_restaurants" ON "favourite_restaurants"
(
 "id"
);

CREATE INDEX "fkIdx_293" ON "favourite_restaurants"
(
 "customer_id"
);



COMMENT ON COLUMN "favourite_restaurants"."ref_type" IS 'Restaurant, Album, Event';
COMMENT ON COLUMN "favourite_restaurants"."type" IS 'Bookmark,Favorite';

-- ************************************** "rewards_type"

CREATE TABLE "rewards_type"
(
 "id"     int NOT NULL,
 "type"   varchar(200) NOT NULL,
 "points" int NOT NULL

);

CREATE UNIQUE INDEX "PK_rewards_type" ON "rewards_type"
(
 "id"
);

-- ************************************** "reward_users"

CREATE TABLE "reward_users"
(
 "id"            int NOT NULL,
 "points_scored" int NOT NULL,
 "customer_id"   int NOT NULL,
 CONSTRAINT "FK_347" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" )
);

CREATE UNIQUE INDEX "PK_reward_users" ON "reward_users"
(
 "id"
);

CREATE INDEX "fkIdx_347" ON "reward_users"
(
 "customer_id"
);

-- ************************************** "filters"

CREATE TABLE "filters"
(
 "id"   int NOT NULL,
 "name" varchar(200) NOT NULL

);

CREATE UNIQUE INDEX "PK_filters" ON "filters"
(
 "id"
);

-- ************************************** "restaurant_filters"

CREATE TABLE "restaurant_filters"
(
 "id"            int NOT NULL,
 "name"          varchar(200) NOT NULL,
 "restaurant_id" int NOT NULL,
 "filter_id"     int NOT NULL,
 CONSTRAINT "FK_371" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" ),
 CONSTRAINT "FK_374" FOREIGN KEY ( "filter_id" ) REFERENCES "filters" ( "id" )
);

CREATE UNIQUE INDEX "PK_restaurant_filters" ON "restaurant_filters"
(
 "id"
);

CREATE INDEX "fkIdx_371" ON "restaurant_filters"
(
 "restaurant_id"
);

CREATE INDEX "fkIdx_374" ON "restaurant_filters"
(
 "filter_id"
);

-- ************************************** "event_photos"

CREATE TABLE "event_photos"
(
 "id"        int NOT NULL,
 "image_url" varchar(500) NOT NULL,
 "event_id"  int NOT NULL,
 CONSTRAINT "FK_357" FOREIGN KEY ( "event_id" ) REFERENCES "events" ( "id" )
);

CREATE UNIQUE INDEX "PK_event_photos" ON "event_photos"
(
 "id"
);

CREATE INDEX "fkIdx_357" ON "event_photos"
(
 "event_id"
);

-- ************************************** "food_albums"

CREATE TABLE "food_albums"
(
 "id"          int NOT NULL,
 "name"        varchar(200) NOT NULL,
 "description" varchar(500) NOT NULL,
 "type"        int NOT NULL,
 "image_url"   varchar(500) NOT NULL,
 "customer_id" int NOT NULL,
 CONSTRAINT "FK_402" FOREIGN KEY ( "customer_id" ) REFERENCES "customer_profile" ( "id" )
);

CREATE UNIQUE INDEX "PK_food_albums" ON "food_albums"
(
 "id"
);

CREATE INDEX "fkIdx_402" ON "food_albums"
(
 "customer_id"
);

COMMENT ON COLUMN "food_albums"."type" IS '0:Private,1:Public';

-- ************************************** "food_album_photos"

CREATE TABLE "food_album_photos"
(
 "id"            int NOT NULL,
 "image_url"     varchar(500) NOT NULL,
 "food_album_id" int NOT NULL,
 CONSTRAINT "FK_381" FOREIGN KEY ( "food_album_id" ) REFERENCES "food_albums" ( "id" )
);

CREATE UNIQUE INDEX "PK_food_album_photos" ON "food_album_photos"
(
 "id"
);

CREATE INDEX "fkIdx_381" ON "food_album_photos"
(
 "food_album_id"
);

-- ************************************** "food_albums_restaurant"

CREATE TABLE "food_albums_restaurant"
(
 "id"            int NOT NULL,
 "name"          varchar(200) NOT NULL,
 "description"   varchar(500) NOT NULL,
 "type"          int NOT NULL,
 "image_url"     varchar(500) NOT NULL,
 "restaurant_id" int NOT NULL,
 CONSTRAINT "FK_405" FOREIGN KEY ( "restaurant_id" ) REFERENCES "restaurant" ( "id" )
);

CREATE UNIQUE INDEX "PK_food_albums_restaurant" ON "food_albums_restaurant"
(
 "id"
);

CREATE INDEX "fkIdx_405" ON "food_albums_restaurant"
(
 "restaurant_id"
);



COMMENT ON COLUMN "food_albums_restaurant"."type" IS '0:Private,1:Public';

-- ************************************** "home_sliders"

CREATE TABLE "home_sliders"
(
 "id"          int NOT NULL,
 "title"       varchar(250) NOT NULL,
 "description" varchar(500) NOT NULL,
 "image_url"   varchar(500) NOT NULL

);

CREATE UNIQUE INDEX "PK_home_sliders" ON "home_sliders"
(
 "id"
);

-- ************************************** "user_followings"

CREATE TABLE "user_followings"
(
 "id"          int NOT NULL,
 "follower_id" int NOT NULL,
 "followed_id" int NOT NULL

);

CREATE UNIQUE INDEX "PK_user_followings" ON "user_followings"
(
 "id"
);