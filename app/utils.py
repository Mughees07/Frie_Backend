import json
import traceback
import requests
import smtplib
from flask import jsonify
from sqlalchemy import exc

from app import db, constants


def send_email(email, password):
    server = smtplib.SMTP('mail.datics.ai', 2525)

    # Next, log in to the server
    server.login(constants.EMAIL_LOGIN,constants.EMAIL_PASSWORD)

    # Send the mail
    msg = "Your New Password for Frie is {}".format(password)
    server.sendmail(constants.EMAIL_LOGIN, email, msg)
    server.close()
    return True


def success_status(key=None, value=None):
    status = {'status': 1, "message": "success"}

    if key:
        status[key] = value

    return status

def error_status(status_code, msg):

    status = {'status': status_code, "message": msg}

    return status




def AddObject(obj):
    try:
        db.session.add(obj)
        db.session.commit()
    except exc.SQLAlchemyError as e:

        print(str(e))
        return error_status(constants.ERROR.DB_ERROR, str(e))

    try:
        return success_status("public_id",obj.public_id)
    except Exception as e:
        print(str(e))
        return success_status()

def AddObjects(obj):

    for o in obj:
        try:
            db.session.add(o)
        except Exception as e:
            return jsonify({"status": 0, 'message': str(e)})

    db.session.commit()


    return success_status()


def DeleteObject(obj):

    if not obj:
        return jsonify({"status": 0, 'message': 'No user found!'})

    try:
        db.session.delete(obj)
        db.session.commit()
    except Exception as e:
        traceback.print_exc()
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()


def commit_changes():
    try:
        db.session.commit()
    except exc.SQLAlchemyError as e:
        db.session.rollback()
        print(str(e))
        return error_status(constants.ERROR.DB_ERROR, str(e))

    return success_status()


def commit_changes_bool():
    try:
        db.session.commit()
    except exc.SQLAlchemyError as e:
        db.session.rollback()
        print(str(e))
        return str(e)

    return None


def DeleteObjects(obj):
    """
    Given the public_id this function, deletes the user associated with that id
    :return:
    """
    if not obj:
        return jsonify({"status": 0, 'message': 'No user found!'})
    for o in obj:
        try:
            db.session.delete(o)
        except Exception as e:
            return jsonify({"status": 0, 'message': str(e)})

    db.session.commit()
    return success_status()


def google_directions_api(curr_lat, curr_long, dest_lat, dest_long, waypoint_lat=None, waypoint_long=None):
    api_keys = constants.GOOGLE_DIRECTIONS_API_KEY

    if waypoint_lat:
        url = "https://maps.googleapis.com/maps/api/directions/json?origin={curr_lat},{curr_long}&destination={dest_lat}" \
              ",{dest_long}&waypoints={waypoint_lat},{waypoint_long}&key={api_key}".format(curr_lat=curr_lat,
                                                                                          curr_long=curr_long,
                                                                                          dest_lat=dest_lat,
                                                                                          dest_long=dest_long,
                                                                                          waypoint_lat=waypoint_lat,
                                                                                          waypoint_long=waypoint_long,
                                                                                           api_key=api_keys)

    else:
        url = "https://maps.googleapis.com/maps/api/directions/json?origin={curr_lat},{curr_long}&destination={dest_lat}" \
              ",{dest_long}&key={api_key}".format(curr_lat=curr_lat,curr_long=curr_long,dest_lat=dest_lat,dest_long=dest_long,
                                                  api_key=api_keys)

    routes = requests.get(url)
    data = json.loads(routes.content.decode('utf-8'))

    return data

# print(google_directions_api(31.526776,74.356757,31.528166,74.356038))