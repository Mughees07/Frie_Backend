from sqlalchemy.orm import backref

from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp
from geoalchemy2 import Geometry


class Driver_Location_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "driver_location"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    long = db.Column(db.Float)
    lat = db.Column(db.Float)
    driver_location = db.Column(Geometry(geometry_type="POINT"))
    trip_id = db.Column(db.Integer, db.ForeignKey("trips.id"))


class Trips_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "trips"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    status = db.Column(db.Integer)
    distance = db.Column(db.Float)
    earning = db.Column(db.Float)
    driver_id = db.Column(db.Integer, db.ForeignKey("driver_profile.id"))
    order_id = db.Column(db.Integer, db.ForeignKey("order.id"), unique=True)
    location = db.relationship(Driver_Location_Model, backref="trip", cascade="all,delete")


