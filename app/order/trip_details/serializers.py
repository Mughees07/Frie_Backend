from flask_restplus import fields
from app.order import rest_api_order


pagination = rest_api_order.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

add_trip_api_model = rest_api_order.model('add driver trips', {
    'status': fields.Integer(required=True),
    'distance': fields.Float(required=True),
    'earning': fields.Float(required=True)
})

update_trip_api_model = rest_api_order.model('update driver trip', {
    'status': fields.Integer(required=True),
    'distance': fields.Float(required=True),
    'earning': fields.Float(required=True)
})

get_trips_api_model = rest_api_order.model('get driver trips', {
    'public_id': fields.String(),
    'status': fields.Integer,
    'distance': fields.Float(),
    'earning': fields.Float(),
    'time': fields.DateTime()
})

get_all_trips_api_model = rest_api_order.model('List of all Trips for Driver', {
    'status': fields.Integer(),
    'message': fields.String(),
    'trips': fields.List(fields.Nested(get_trips_api_model))
})

location_api_model = rest_api_order.model('get driver current location', {
    'current_long': fields.Float(required=True),
    'current_lat': fields.Float(required=True),
    'trip_status': fields.Integer(required=True)
})

get_trips_id_api_model = rest_api_order.model('get driver trip id', {
    'public_id': fields.String()
})

get_one_trips_api_model = rest_api_order.model('Trip for Driver', {
    'status': fields.Integer(),
    'message': fields.String(),
    'trip_id': fields.String()
})

update_trip_status = rest_api_order.model('update trip status', {
    'trip_status': fields.Integer()
})

get_trip_credit = rest_api_order.model('get trip credit', {
    'credit': fields.Integer()
})


