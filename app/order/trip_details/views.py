import uuid
import random
from flask import request
from flask_restplus import Resource
from sqlalchemy import func
from app import db

from app.order.order_details.models import Order_Model
from app.order.trip_details.controllers import get_driver_trips, daily_activities, weekly_activities, \
    monthly_activities, calc_earnings_by_month

from app.order.trip_details.models import Trips_Model, Driver_Location_Model
from app.order.trip_details.serializers import *
from app.user.driver_profile.models import Driver_Profile_Model
from app.user.addresses.models import Addresses_Model
from app.user.users_auth.decorators import token_required, token_required_user
from app.utils import success_status, AddObject, DeleteObject, google_directions_api, error_status


trips_namespace = rest_api_order.namespace('driver_trips', description='Operations related to Driver Trips')
location_namespace = rest_api_order.namespace('location', description='Operations related to Driver Location')


@trips_namespace.route('/')
class DriverTripsUser(Resource):
    @rest_api_order.marshal_with(get_all_trips_api_model)
    @trips_namespace.doc(security='apikey')
    @token_required_user
    def get(self,current_user):
        """
        return the Trips associated with the driver
        """
        # driver = Driver_Profile_Model.query.filter_by(public_id=id).first()
        driver = current_user.driver
        if not driver:
            return error_status(0, "No Driver Found")

        trips = driver.trips_child
        if trips.count() == 0:
            return error_status(0, "No Trips Found")

        return get_driver_trips(trips)

@trips_namespace.route('/<string:id>')
class DriverTrips(Resource):
    @rest_api_order.marshal_with(get_one_trips_api_model)
    @trips_namespace.doc(security='apikey',params={'id': 'Order ID'})
    @token_required_user
    def post(self,id,current_user):
        """
        Given the public id of the order, This method adds a trip to the current driver
        """
        # data = request.get_json()
        order = Order_Model.query.filter_by(public_id=id).first()
        if not order:
            return error_status(0, "No Order Found")

        driver = current_user.driver
        if not driver:
            return error_status(0, "No Driver Found")

        new_trip = Trips_Model(public_id=str(uuid.uuid4()),status=0,distance=0,earning=0,
                               driver_id=driver.id, order_id=order.id)

        try:
            order.status = 1
            db.session.add(new_trip)
            db.session.commit()
        except Exception as e:
            return error_status(0,str(e))

        return success_status("trip_id",new_trip.public_id)

    @rest_api_order.expect(update_trip_api_model)
    @trips_namespace.doc(security='apikey',params={'id': 'Trip ID'})
    @token_required
    def put(self,id):
        """
        Given the public id of the trip, this method updates the trip
        """
        data = request.get_json()
        trip = Trips_Model.query.filter_by(public_id=id).first()

        if not trip:
            return error_status(0, "No Trip Found")

        trip.status = data["status"]
        trip.distance = data["distance"]
        trip.earning = data["earning"]

        db.session.commit()
        return success_status()

    @trips_namespace.doc(security='apikey',params={'id': 'Trip ID'})
    @token_required
    def delete(self,id):
        """
        Given public_id, this method deletes restaurant profile,
        authentication token is required
        """
        trip = Trips_Model.query.filter_by(public_id=id).first()
        if not trip:
            return error_status(0, "No Trip Found")

        return DeleteObject(trip)

@trips_namespace.route('/earnings/')
class EarningsByMonth(Resource):
    @trips_namespace.doc(security='apikey')
    @token_required_user
    def get(self,current_user):
        driver = current_user.driver
        if not driver:
            return error_status(0, "No Driver Found")

        trips = Trips_Model.query.filter_by(driver_id=driver.id, status=3).all()
        if not trips:
            return error_status(0, "No Trips Found")

        month = func.date_trunc('month', Trips_Model.created_at)

        total_earnings = db.session.query(func.sum(Trips_Model.earning).label('Earnings'), month).group_by(month)\
            .filter(Trips_Model.status == 3).all()
        return calc_earnings_by_month(total_earnings)


@trips_namespace.route('/activity/<int:id>')
class AllActivity(Resource):
    @trips_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user, id):
        driver = current_user.driver
        if not driver:
            return error_status(0, "No Driver Found")

        flag = {0: "day",1: "week",2: "month"}

        month = func.date_trunc(flag[id], Trips_Model.created_at)

        count = db.session.query(func.count(Trips_Model.status == 2).label('Count'), month).group_by(month).all()
        total_earnings = db.session.query(func.sum(Trips_Model.earning).label('Earnings'), month).group_by(month)\
            .filter(Trips_Model.status == 3).all()

        if id == 0:
            return daily_activities(count, total_earnings)
        elif id == 1:
            return weekly_activities(count, total_earnings)
        elif id == 2:
            return monthly_activities(count, total_earnings)
        else:
            return error_status()

@trips_namespace.route('/completion/<string:id>')
class TripCompletion(Resource):
    @rest_api_order.expect(update_trip_status)
    @trips_namespace.doc(security='apikey',params={'id': 'Trip ID'})
    @token_required
    def put(self,id):
        """
        Given the public id of the trip and trip status, this method updates trip and order status and return credit
        """
        data = request.get_json()
        trip = Trips_Model.query.filter_by(public_id=id).first()
        if not trip:
            return error_status(0, "No Trip Found")

        order = trip.trip_order
        if not order:
            return error_status(0, "No Order Found")

        try:
            trip.status = data['trip_status']
            order.status = 5
            db.session.commit()
        except Exception as e:
            return error_status(0,str(e))

        credit = random.randint(10,20)
        return success_status('credit',credit)

@location_namespace.route('/<string:id>')
class Location(Resource):
    @rest_api_order.expect(location_api_model)
    @location_namespace.doc(params={'id': 'Trip ID'})
    def post(self, id):
        """
        Given the public id of the trip and current lat, long of driver, this method returns distance and polyline
        """
        data = request.get_json()
        trip = Trips_Model.query.filter_by(public_id=id).first()
        if not trip:
            return error_status(0, "No Trip Found")

        point = 'POINT({} {})'.format(data['current_long'], data['current_lat'])
        new_location = Driver_Location_Model(public_id=str(uuid.uuid4()),long=data['current_long'],
                                             lat=data['current_lat'],driver_location=point,trip_id=trip.id)

        if trip.status == 3:
            return error_status(0, "Trip is already completed")

        else:

            if trip.trip_order.status == 1 and data['trip_status'] == 1:
                waypoints = None
                # restaurant_address = Addresses_Model.query.filter_by(id=trip.trip_order.restaurant_address_id).first()
                restaurant_address = trip.trip_order.restaurant.addresses_child[0]
                route = google_directions_api(data['current_lat'], data['current_long'],restaurant_address.lat,
                                              restaurant_address.long)
                if route['status'] == 'OK':

                    try:
                        trip.status = data['trip_status']
                        db.session.add(new_location)
                        db.session.commit()
                    except Exception as e:
                        return error_status(0,str(e))

                    directions = {'polyline': route['routes'][0]['overview_polyline']['points'],
                                  'distance': route['routes'][0]['legs'][0]['distance'],
                                  'duration': route['routes'][0]['legs'][0]['duration']}

                    return success_status('route', directions)
                else:
                    return error_status(0, "Google Direction APIs not responding, Check Longitude and Latitude Value")

            if trip.trip_order.status == 4 and data['trip_status'] == 2:
                customer_address = Addresses_Model.query.filter_by(id=trip.trip_order.customer_address_id).first()
                route = google_directions_api(data['current_lat'],data['current_long'],customer_address.lat,customer_address.long)

                if route['status'] == 'OK':

                    try:
                        trip.status = data['trip_status']
                        db.session.add(new_location)
                        db.session.commit()
                    except Exception as e:
                        return error_status(0,str(e))

                    directions = {'polyline': route['routes'][0]['overview_polyline']['points'],
                                  'distance': route['routes'][0]['legs'][0]['distance'],
                                  'duration': route['routes'][0]['legs'][0]['duration']}

                    return success_status('route', directions)
                else:
                    return error_status(0, "Google Direction APIs not responding, Check Longitude and Latitude Value")

            if trip.trip_order.status == 5:
                return error_status(0, "Order is already Delivered")