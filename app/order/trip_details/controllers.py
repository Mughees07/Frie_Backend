import datetime
from flask import jsonify

from app import db

from app.utils import success_status


def get_driver_trips(trips):
    output = []
    for trip in trips:
        trip_data = dict()
        trip_data['public_id'] = trip.public_id
        trip_data['status'] = trip.status
        trip_data['distance'] = trip.distance
        trip_data['earning'] = trip.earning
        trip_data['time'] = trip.created_at
        output.append(trip_data)

    return success_status("trips", output)


def calc_earnings_by_month(total_earnings):
    
    earnings_by_month = {"current_month": total_earnings[-1][0]}
    monthly_invoices = list()
    for monthly in total_earnings[:-1]:
        earning = {'earning': monthly[0], 'month': str(monthly[1].month) + "," + str(monthly[1].year)}
        monthly_invoices.append(earning)

    monthly_invoices.reverse()
    earnings_by_month['monthly_invoices'] = monthly_invoices

    return success_status('earnings_by_month', earnings_by_month)


def daily_activities(count,total_earnings):

    months = {1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct",
              11: "Nov", 12: "Dec"}
    counter = 0

    all_activities = list()
    for earning in total_earnings:
        period = str(earning[1].day) + ' ' + months[earning[1].month]
        activity_data = {'date':period,'earning': earning[0],'count': count[counter][0]}
        all_activities.append(activity_data)
        counter += 1

    return success_status("all_activities",all_activities)


def weekly_activities(count,total_earnings):

    months = {1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct",
              11: "Nov", 12: "Dec"}
    counter = 0

    all_activities = list()
    for earning in total_earnings:
        next_week = earning[1] + datetime.timedelta(days=7)
        period = str(earning[1].day) + ' ' + months[earning[1].month] + " - " + str(next_week.day) + ' ' + months[next_week.month]
        activity_data = {'date':period,'earning': earning[0], 'count': count[counter][0]}
        all_activities.append(activity_data)
        counter += 1

    return success_status("all_activities",all_activities)


def monthly_activities(count,total_earnings):

    months = {1: "Jan", 2: "Feb", 3: "Mar", 4: "Apr", 5: "May", 6: "Jun", 7: "Jul", 8: "Aug", 9: "Sep", 10: "Oct",
              11: "Nov", 12: "Dec"}
    counter = 0

    all_activities = list()
    for earning in total_earnings:
        period = months[earning[1].month] + '-' + str(earning[1].year)
        activity_data = {"date":period,'earning': earning[0],'count': count[counter][0]}
        all_activities.append(activity_data)
        counter += 1

    return success_status("all_activities",all_activities)