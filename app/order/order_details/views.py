import uuid

from flask import request
from flask_restplus import Resource

from app.order import rest_api_order
from app.order.order_details.controllers import getOrders, AddOrder, getOrder, UpdateOrder, getOrderIds
from app.order.order_details.models import Order_Model, Order_Dishes_Model

from app.order.order_details.serializers import update_order_status_api_model, get_one_orders_api_model

from app.order.order_details.serializers import get_all_orders_api_model, add_order_api_model, get_orders_api_model
from app.payment.customer_payment.controllers import charge_payment_method
from app.payment.customer_payment.models import Customer_Payment_Model
from app.restaurant.restaurant_food_categories.models import *
from app.user.addresses.models import Addresses_Model
from app.user.customer_profile.models import Customer_Profile_Model
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.users_auth.decorators import token_required_user
from app.utils import success_status, DeleteObject, error_status

order_namespace = rest_api_order.namespace('order_details', description='Operations related to order and orders dishes')



@order_namespace.route('')
class Order(Resource):
    """
    Operations related to Restaurants Orders
    """
    @rest_api_order.marshal_with(get_all_orders_api_model)
    def get(self):
        """
        Returns all the orders
        """
        orders = Order_Model.query.all()
        try:
            return getOrders(orders)
        except Exception as e:
            return error_status(0,str(e))

    @rest_api_order.expect(add_order_api_model)
    #@rest_api_order.marshal_with(get_one_orders_api_model)
    @rest_api_order.doc(security='apikey')
    @token_required_user
    def post(self, current_user):
        """
        Given Order attributes , adds orders
        """
        data = request.get_json()
        current_restaurant = Restaurant_Profile_Model.query.filter_by(public_id=data['restaurant_id']).first()
        if not current_restaurant:
            return error_status(0, "No Restaurant Found")

        customer = Customer_Profile_Model.query.filter_by(user=current_user).first()
        payment_method = Customer_Payment_Model.query.filter_by(public_id=data['customer_payment_id']).first()
        if not payment_method:
            return error_status(0, "No Payment Method Found")

        customer_address = Addresses_Model.query.filter_by(public_id=data['customer_address_id']).first()
        if not customer_address:
            return error_status(0, "No Customer Address Found")

        order = Order_Model(public_id=str(uuid.uuid4()), restaurant=current_restaurant,
                            customer=customer, customer_address=customer_address, receipt_id=str(uuid.uuid4().hex[:8]),price = data['price'],
                            customer_payment_id=payment_method.id)

        order,dishes = AddOrder(order, data)

        if not order:
            return error_status(0, "order not found")

        if not dishes:
            return error_status(0, "dishes not found")

        if len(dishes) == 0:
            return error_status(0, "dishes empty")

        if charge_payment_method(order.price,payment_method.public_id):

            db.session.add(order)
            for o in dishes:
                db.session.add(o)
            db.session.commit()

            return getOrder(order)

        else:
            return error_status(0,"Order not placed")



@order_namespace.route('/<string:id>')
@order_namespace.doc(params={'id': 'Order ID'})
class Order_Updates(Resource):
    """
       Operations related to Restaurants Orders
       """
    def delete(self, id):
        """
        Deletes order
        """
        obj=Order_Model.query.filter_by(public_id=id).first()
        return DeleteObject(obj)

    @rest_api_order.expect(add_order_api_model)
    def put(self, id):
        """
        Updates Order and Order Dishes
        """
        order = Order_Model.query.filter_by(public_id=id).first()
        Order_Dishes_Model.query.filter_by(order=order).delete()
        dishes = UpdateOrder(order)
        for o in dishes:
            db.session.add(o)
        db.session.commit()

        return success_status()

    @rest_api_order.marshal_with(get_one_orders_api_model)
    def get(self, id):
        """
        get  Order based on order id
        """
        orders =Order_Model.query.filter_by(public_id=id).first()
        return getOrder(orders)

@order_namespace.route('/status/<string:id>')
class Order_Status_Updates(Resource):
    @rest_api_order.expect(update_order_status_api_model)
    @order_namespace.doc(params={'id': 'Order ID'})
    def put(self, id):
        """
        Updates Order Status
        """
        data = request.get_json()
        order = Order_Model.query.filter_by(public_id=id).first()
        order.status = data['status']
        db.session.commit()

        return success_status()


@order_namespace.route('/driver_orders/')
class Driver_Orders(Resource):


    @rest_api_order.marshal_with(get_orders_api_model)
    def get(self):
        """
        Returns all the ready orders for drivers
        """
        orders = Order_Model.query.filter_by(status=0).all()

        return getOrderIds(orders)


@order_namespace.route('/customer_orders/')
class Customer_Orders(Resource):


    @rest_api_order.marshal_with(get_all_orders_api_model)
    @token_required_user
    def get(self,current_user):
        """
        Returns all the  orders of a customer
        """
        orders = Order_Model.query.filter_by(customer = current_user.customer).all()

        return getOrders(orders)



