from sqlalchemy.orm import backref

from app import db,user
from app.models import Created_TimeStamp, Updated_TimeStamp
from app.order.trip_details.models import Trips_Model
from app.restaurant.restaurant_food_categories.models import Restaurant_Dish_Model, Restaurant_Dish_Style_Model, Restaurant_Dish_Topping_Model



class Order_Model(db.Model,Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "order"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String())
    #instruction = db.Column("instruction", db.String())
    price = db.Column("price", db.Integer)
    status = db.Column("status",db.Integer, default=0)
    restaurant_id=db.Column("restaurant_id", db.Integer, db.ForeignKey("restaurant_profile.id"))
    customer_id = db.Column("customer_id", db.Integer, db.ForeignKey("customer_profile.id"))
    customer_address_id = db.Column("customer_address_id",db.Integer,db.ForeignKey("addresses.id"))
    customer_payment_id = db.Column(db.Integer, db.ForeignKey("customer_payment.id"))

    receipt_id = db.Column(db.String())
    #restaurant_address_id = db.Column("restaurant_address_id", db.Integer, db.ForeignKey("addresses.id"))
    restaurant = db.relationship(user.restaurant_profile.models.Restaurant_Profile_Model,backref=backref("restaurant_order_children", cascade="all,delete"))
    customer = db.relationship(user.customer_profile.models.Customer_Profile_Model,backref=backref("customer_order_children", cascade="all,delete"))
    customer_address = db.relationship(user.addresses.models.Addresses_Model)

    trips = db.relationship(Trips_Model, backref="trip_order", cascade="all,delete")




class Order_Dishes_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "order_dishes"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String())
    quantity = db.Column(db.Integer)
    instruction = db.Column(db.String)

    order_restaurant_dish_id=db.Column(db.Integer, db.ForeignKey("restaurant_dish.id"))
    order_dish_topping_id = db.Column(db.Integer, db.ForeignKey("restaurant_dish_topping.id"))
    order_dish_style_id = db.Column(db.Integer, db.ForeignKey("restaurant_dish_style.id"))
    order_id = db.Column(db.Integer, db.ForeignKey("order.id"))

    restaurant_dish = db.relationship(Restaurant_Dish_Model,backref=backref("restaurant_dishes_children", cascade="all,delete"))
    order = db.relationship(Order_Model,backref=backref("order_children", cascade="all,delete"))
    topping = db.relationship(Restaurant_Dish_Topping_Model,backref=backref("topping_children", cascade="all,delete"))
    style = db.relationship(Restaurant_Dish_Style_Model,backref=backref("style_children", cascade="all,delete"))
















