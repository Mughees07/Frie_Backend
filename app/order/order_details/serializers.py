from flask_restplus import fields
from app.order import rest_api_order


##########################################################################3
###################################################################3

get_customer_payments_api_model = rest_api_order.model('get customer payment method', {
    'public_id': fields.String(),
    'last_4_digits': fields.String(),
    'type': fields.Integer()
})

restaurant_profile_api_model = rest_api_order.model('restaurant_profile', {
    'public_id': fields.String(),
    'name': fields.String(),
    'rating' : fields.Float,
    'opening_time': fields.String,
    'closing_time': fields.String,
    'phone_no' : fields.String(),
    'image_url' : fields.String(),
    'capacity' : fields.Integer,
})

get_customer = rest_api_order.model('get customer', {
    'public_id': fields.String(),
    'name': fields.String(),
    'username': fields.String(),
    'address': fields.String(),
    'image_url': fields.String(),
    'about': fields.String()
})


get_addresses = rest_api_order.model('get addresses', {
    'public_id': fields.String(),
    'street': fields.String(),
    'area': fields.String(),
    'city': fields.String(),
    'state': fields.String(),
    'zipcode': fields.String(),
    'country': fields.String(),
    'long': fields.Float(),
    'lat': fields.Float()
})

dish_style_ids_api_model = rest_api_order.model('dishes_style', {
    'public_id': fields.String(required=True),
    'style_id': fields.String(required=True),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True)
})
restaurant_dish_api_model = rest_api_order.model('restaurant_dishes', {
    'dish_id': fields.String(required=True),
    'dish_name': fields.String(required=True),
    'collection_id': fields.String(required=True),
    'collection_name': fields.String(required=True),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True)
})

dish_style_ids_api_model = rest_api_order.model('dishes_style', {
    'public_id': fields.String(required=True),
    'style_name': fields.String(required=True),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True)
})

dish_topping_ids_api_model = rest_api_order.model('dishes_toppings', {
    'public_id': fields.String(required=True),
    'topping_name': fields.String(required=True),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True)
})
##################################order dishes##################################


orders_dish_api_model = rest_api_order.model('order_dish', {

    'order_dishes_id': fields.String(required=True),
    'restaurant_dish': fields.Nested(restaurant_dish_api_model),
    'quantity': fields.Integer(required=True),
    'style' : fields.Nested(dish_style_ids_api_model),
    'topping': fields.Nested(dish_topping_ids_api_model)

})

##############################Order##########################################


add_order_dish_api_model= rest_api_order.model('add_order_dishes', {

    'restaurant_dish_id': fields.String(required=True),
    'instruction': fields.String(),
    'quantity': fields.Integer(required=True),
    'restaurant_dish_topping_id': fields.List(fields.String()),
    'restaurant_dish_style_id': fields.String(required=True)

})

add_order_api_model = rest_api_order.model('add_order', {

    #'instruction': fields.String(required=True),
    'price' : fields.Float(required= True),
    'restaurant_id': fields.String(required=True),
    'customer_address_id': fields.String(required=True),
    'customer_payment_id': fields.String(required=True),
    'order_dishes': fields.List(fields.Nested(add_order_dish_api_model))
})



orders_api_model = rest_api_order.model('order', {
    'public_id': fields.String(),
    #'instruction': fields.String(),
    'status': fields.Integer,
    'receipt_id': fields.String(),
    'customer_payment_method': fields.Nested(get_customer_payments_api_model),
    'price': fields.Integer,
    'restaurant': fields.Nested(restaurant_profile_api_model),
    'restaurant_address': fields.Nested(get_addresses),
    'customer': fields.Nested(get_customer),
    'customer_address': fields.Nested(get_addresses),

    'order_dishes':  fields.List(fields.Nested(orders_dish_api_model))
})



get_all_orders_api_model = rest_api_order.model('List of all orders dishes', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'orders': fields.List(fields.Nested(orders_api_model))
})

get_one_orders_api_model = rest_api_order.model('order details', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'order': fields.Nested(orders_api_model)
})


#################### Driver Orders ##########################33

get_order_ids = rest_api_order.model('orders_id', {
    'public_id' : fields.String()
})

get_orders_api_model = rest_api_order.model('Get an order', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'orders':   fields.List(fields.Nested(get_order_ids))
})

update_order_status_api_model = rest_api_order.model('update order status', {
    'status': fields.Integer(required=True)
})





