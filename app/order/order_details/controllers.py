import uuid

from flask import request
from sqlalchemy import func

from app import db

#################Cuisines#####################
from app.order.order_details.models import Order_Dishes_Model, Order_Model
from app.payment.customer_payment.controllers import get_payment_method
from app.payment.customer_payment.models import Customer_Payment_Model
from app.restaurant.restaurant_food_categories.models import Restaurant_Dish_Model, Restaurant_Dish_Topping_Model, \
    Restaurant_Dish_Style_Model
from app.user.addresses.controllers import getUserAddress, getOneUserAddress
from app.user.addresses.models import Addresses_Model
from app.user.customer_profile.controllers import get_customer_data
from app.user.customer_profile.models import Customer_Profile_Model
from app.user.restaurant_profile.controllers import getRestaurant
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.utils import success_status, error_status


def getOrderDishes(o):
    orders_dishes = Order_Dishes_Model.query.filter_by(order=o)

    output=[]

    for r in orders_dishes:
        dish_data={}
        dish_data['order_dishes_id'] = r.public_id
        dish_data['restaurant_dish'] = getRestaurantDish(r.restaurant_dish)
        dish_data['quantity'] = r.quantity
        dish_data['topping'] = getOrderDishesTopping(r.topping)
        dish_data['style'] =  getOrderDishesStyle(r.style)
        output.append(dish_data)

    return output


def getOrders(orders):
    output = []
    for o in orders:
        order = {}
        order['public_id'] = o.public_id
        #order['instruction'] = o.instruction
        order['price'] = o.price
        order['status'] = o.status
        order['customer_payment_method'] = get_payment_method(o.payment_method)
        order['restaurant']= getRestaurant(o.restaurant)
        order['restaurant_address'] = getUserAddress(o.restaurant.addresses_child)
        order['customer']=get_customer_data(o.customer)
        order['customer_address'] = getOneUserAddress(o.customer_address)
        order['order_dishes'] = getOrderDishes(o)
        output.append(order)

    return success_status("orders", output)


def getOrder(o):
    order = {}
    order['public_id'] = o.public_id
    #order['instruction'] = o.instruction
    order['price'] = o.price
    order['status'] = o.status
    order['receipt_id'] = o.receipt_id
    order['customer_payment_method'] = get_payment_method(o.payment_method)
    order['restaurant'] = getRestaurant(o.restaurant)
    order['restaurant_address'] = getUserAddress(o.restaurant.addresses_child)
    order['customer'] = get_customer_data(o.customer)
    order['customer_address'] = getOneUserAddress(o.customer_address)
    order['order_dishes'] = getOrderDishes(o)


    return success_status("order", order)


def getOrderIds(orders):
    output = []
    for o in orders:
        order = {}
        order['public_id'] = o.public_id
        output.append(order)

    return success_status("orders", output)


######################Dishes###################################

def getRestaurantDish(r):

        dish_data = {}
        dish_data['dish_id'] = r.dish.public_id
        dish_data['dish_name'] = r.dish.name
        if(r.collection):
            dish_data['collection_id'] = r.collection.public_id
            dish_data['collection_name'] = r.collection.name
        dish_data['description']= r.description
        dish_data['price']=r.price

        return dish_data


######################Restaurant Dishes Toppings###################################

def getOrderDishesTopping(r):
        dish_data = {}
        dish_data['public_id'] = r.topping.public_id
        dish_data['topping_name'] = r.topping.name
        dish_data['description']= r.description
        dish_data['price']=r.price


        return dish_data


######################Restaurant Dishes Styles###################################

def getOrderDishesStyle(r):

        dish_data = {}
        dish_data['public_id'] = r.style.public_id
        dish_data['style_name'] = r.style.name
        dish_data['description']= r.description
        dish_data['price']=r.price

        return dish_data

def AddOrder(order,data):

    dishes = []
    for d in data['order_dishes']:
       dishes.append(AddOrderDish(order,d))

    #order.price = CalculateOrderPrice(dishes)

    return order, dishes


def UpdateOrder(order):
    data = request.get_json()
    current_restaurant = Restaurant_Profile_Model.query.filter_by(public_id=data['restaurant_id']).first()
    if not current_restaurant:
        return error_status(0, "No Restaurant Found")

    customer = Customer_Profile_Model.query.filter_by(public_id=data['customer_id']).first()

    customer_address = Addresses_Model.query.filter_by(public_id=data['customer_address_id']).first()
    if not customer_address:
        return error_status(0, "No Customer Address Found")

    payment_method = Customer_Payment_Model.query.filter_by(public_id=data['customer_payment_method']).first()
    if not payment_method:
        return error_status(0, "No Payment Method Found")

    order.instruction=data['instruction']
    order.restaurant=current_restaurant
    order.customer=customer
    order.customer_address=customer_address
    order.receipt_id = data['receipt_id']

    dishes = []
    for d in data['order_dishes']:
        dishes.append(AddOrderDish(order,d))

    #order.price = CalculateOrderPrice(dishes) + order.restaurant.delivery_price
    #order.price += ((order.tax_percentage/100) * order.price)

    return dishes


def AddOrderDish(order , d):
    current_restaurant_dish = Restaurant_Dish_Model.query.filter_by(public_id=d['restaurant_dish_id']).first()
    current_restaurant_topping = Restaurant_Dish_Topping_Model.query.filter_by(public_id=d['restaurant_dish_topping_id'][0]).first()
    current_restaurant_style = Restaurant_Dish_Style_Model.query.filter_by(public_id=d['restaurant_dish_style_id']).first()
    obj = Order_Dishes_Model(public_id=str(uuid.uuid4()), restaurant_dish=current_restaurant_dish, order=order,
                             topping=current_restaurant_topping, style=current_restaurant_style,instruction = d['instruction'],
                             quantity=d['quantity'])
    return obj




def CalculateOrderPrice(dishes):
    sum = 0
    for od in dishes:
        sum += od.restaurant_dish.price
        sum += od.topping.price
        sum += od.style.price
        sum *= (od.restaurant_dish.quantity)


    return sum