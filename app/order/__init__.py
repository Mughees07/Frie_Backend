from flask import Flask, Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'
    }
}

order_blueprint = Blueprint('order', __name__, url_prefix="/api/v1/order")

rest_api_order = Api(order_blueprint,
                    title='Orders',
                    version='1.0',
                    description='Endpoints related to Orders and Trips',
                    authorizations=authorizations
                    # All API metadatas
                    )


from app.order.order_details.views import order_namespace
from app.order.trip_details.views import trips_namespace, location_namespace