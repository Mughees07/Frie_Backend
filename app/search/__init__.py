from flask import Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'

    }
}

search_blueprint = Blueprint('search', __name__, url_prefix="/api/v1/search")

rest_api_search = Api(search_blueprint,
                    title='Search',
                    version='1.0',
                    description='Endpoints related to Search',
                    authorizations=authorizations,
                    validate=True
                    # All API metadatas
                    )

from app.search.customer.views import customer_namespace