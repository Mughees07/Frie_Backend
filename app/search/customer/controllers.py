import datetime
import uuid

from flask import jsonify, request
from sqlalchemy import func, and_

from app import db
from app.search.customer.models import Location_Searches_Model
from app.user.addresses.models import Addresses_Model
from app.user.driver_profile.models import Driver_Profile_Model, Vehicle_Type_Model, Areas_Model
from app.user.restaurant_profile.controllers import getRestaurant
from app.user.restaurant_profile.models import Restaurant_Profile_Model

from app.utils import success_status, error_status

def GetNearbyRestaurants(current_long,current_lat,min_radius, max_radius):

    point = 'POINT({} {})'.format(current_long, current_lat)

    addresses_within_radius = Addresses_Model.query.filter(Addresses_Model.restaurant_id.isnot(None)).filter(
        func.ST_Distance(Addresses_Model.geo_location, point) <= max_radius).\
        filter(func.ST_Distance(Addresses_Model.geo_location, point) >= min_radius).\
        order_by(func.ST_Distance(Addresses_Model.geo_location, point)).all()

    output = []
    for ad in addresses_within_radius:
        output.append(ad.restaurant_id)
    return Restaurant_Profile_Model.query.filter(Restaurant_Profile_Model.id.in_(output)), output



def GetSortedRestaurants(sort_order, restaurants,ordered_restaurant):

    if(sort_order==1):
        return restaurants.order_by(Restaurant_Profile_Model.id.asc()).all()
    elif(sort_order == 2):
        return restaurants.order_by(Restaurant_Profile_Model.id.desc()).all()
    elif (sort_order == 3):
        return restaurants.order_by(Restaurant_Profile_Model.price_group.asc()).all()
    elif (sort_order == 4):
        return restaurants.order_by(Restaurant_Profile_Model.price_group.desc()).all()
    elif (sort_order == 5):
        return restaurants.order_by(Restaurant_Profile_Model.rating.asc()).all()
    elif (sort_order == 6):
        return restaurants.order_by(Restaurant_Profile_Model.rating.desc()).all()
    elif (sort_order == 7):
        return restaurants.order_by(Restaurant_Profile_Model.popularity.asc()).all()
    elif (sort_order == 8):
        return restaurants.order_by(Restaurant_Profile_Model.popularity.desc()).all()
    elif (sort_order == 9):
        restaurants = restaurants.all()
        output = list()
        for i in ordered_restaurant:
            for res in restaurants:
                if res.id == i:
                    output.append(res)
        return output
    elif (sort_order == 10):
        restaurants = restaurants.all()
        ordered_restaurant.reverse()
        output = list()
        # for i in ordered_restaurant:
        #     for res in restaurants:
        #         if res.id == i:
        #             output.append(res)
        output = [res for i in ordered_restaurant for res in restaurants if res.id == i]
        return output
    #rating , popularity , distance

def GetFilteredRestaurants(all_restaurants,location_title,location_latitude,location_longitude,sort_order,book_table_available,open_now,min_distance,max_distance,dietary_restrictions,current_customer):

    location_search = Location_Searches_Model(title= location_title,long=location_longitude,lat=location_latitude,customer=current_customer)

    try:
       db.session.add(location_search)
       db.session.commit()
    except Exception as e:
       return error_status(0, str(e))


    restaurants, order = GetNearbyRestaurants(location_longitude,location_latitude, min_distance,max_distance)
    restaurant_ids = [res.id for res in all_restaurants]
    ordered_restaurant = [id_ for id_ in order if id_ in restaurant_ids]
    restaurants = restaurants.filter(Restaurant_Profile_Model.id.in_(restaurant_ids))
    # if(sort_order==0):
    #     restaurants = restaurants.filter_by()

    if (book_table_available == 'true') :
        restaurants = restaurants.filter(Restaurant_Profile_Model.table_booking_url != None)

    if (open_now == 'true'):
       current_time = datetime.datetime.now().time()
       restaurants = restaurants.filter(and_(current_time > Restaurant_Profile_Model.opening_time,current_time < Restaurant_Profile_Model.closing_time))

    if (len(dietary_restrictions) > 0 and len(dietary_restrictions[0]) == 36 ):
        restaurants = restaurants.filter(Restaurant_Profile_Model.id.in_(dietary_restrictions))

    if(int(sort_order) > 0):
       restaurants= GetSortedRestaurants(int(sort_order),restaurants,ordered_restaurant)

    # final_restaurants = restaurants.all()
    output=[]
    for r in restaurants:
        data=getRestaurant(r)
        output.append(data)

    return success_status("restaurants",output)








