from sqlalchemy.orm import relationship

from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp
from app.user.customer_profile.models import Customer_Profile_Model


class Location_Searches_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "location_searches"
    id = db.Column(db.Integer, primary_key=True)

    title = db.Column(db.String)
    long = db.Column(db.Float)
    lat = db.Column(db.Float)

    customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    customer = relationship(Customer_Profile_Model)