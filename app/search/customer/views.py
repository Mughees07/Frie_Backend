from flask import request
from flask_restplus import Resource, reqparse

from app.search import rest_api_search
from app.search.customer.controllers import GetFilteredRestaurants
from app.search.customer.models import Location_Searches_Model
from app.search.customer.serializers import get_location_searches
from app.user.customer_profile.controllers import get_all_customer_profiles, getAllFavRestaurants, getAllAlbums
from app.user.customer_profile.models import Customer_Profile_Model, Customer_Food_Album_Model, User_Followings_Model
from app.user.restaurant_profile.controllers import getAllRestaurants
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.users_auth.decorators import token_required_user
from app.utils import success_status

customer_namespace = rest_api_search.namespace('customer_search', description='Operations related to Search')

@customer_namespace.route('/users/<string:users>')
#@customer_namespace.doc(params={'users ': 'Users Search Text'})
class SearchUsers(Resource):

        def get(self,users,current_user):
                customer = current_user.customer
                users = Customer_Profile_Model.query.filter(Customer_Profile_Model.name.contains(users)).all()
                customer_followings = User_Followings_Model.query.filter_by(follower=customer).all()
                followings = [c_f.following.public_id for c_f in customer_followings]
                users_data = get_all_customer_profiles(users)
                for user in users_data['customers']:
                        if user['public_id'] in followings:
                                user['is_following'] = True
                        else:
                                user['is_following'] = False

                return users_data

@customer_namespace.route('/restaurants/<string:restaurants>')
#@customer_namespace.doc(params={'Search Text ': 'restaurants'})
class Search_Restaurants(Resource):

        def get(self, restaurants):
                users = Restaurant_Profile_Model.query.filter(Restaurant_Profile_Model.name.contains(restaurants)).all()
                return getAllRestaurants(users)


@customer_namespace.route('/albums/<string:albums>')
#@customer_namespace.doc(params={'Search Text ': 'Food Albums'})
class SearchAlbums(Resource):

        def get(self,albums):
                albums = Customer_Food_Album_Model.query.filter(Customer_Food_Album_Model.title.contains(albums)).all()

                return getAllAlbums(albums)




parser = rest_api_search.parser()
parser.add_argument('location_title', required=True, help="Location Title")
parser.add_argument('lat', required=True, help="Latitude" ,type=float)
parser.add_argument('long', required=True, help="Longitude" , type=float)
parser.add_argument('sort_order',  help="Sort Order" , type = int)
parser.add_argument('book_table_available',  help="Is Book Table Available", type= bool)
parser.add_argument('open_now', help="Check if restaurant is open" , type= bool)
parser.add_argument('min_distance',  help="Minimum Radius for distance" , type = float)
parser.add_argument('max_distance',  help="Max Radius for distance" , type= float)
parser.add_argument('dietary_restrictions[]', help="Dietary Restriction", action='append')

@rest_api_search.doc(security='apikey')
@customer_namespace.route('/location/')
@customer_namespace.doc(parser=parser)

class Search_Location(Resource):

        @token_required_user
        def get(self, current_user):
                location_title=request.args['location_title']
                long = request.args['long']
                lat = request.args['lat']
                sort_order = request.args['sort_order']
                book_table_available=request.args['book_table_available']
                open_now = request.args['open_now']
                max_distance = request.args['max_distance']
                min_distance = request.args['min_distance']
                dietary_restrictions=request.args.getlist('dietary_restrictions[]')
                restaurants = Restaurant_Profile_Model.query.all()
                return GetFilteredRestaurants(restaurants,location_title,lat,long,sort_order,book_table_available,open_now,min_distance,max_distance,dietary_restrictions,current_user.customer)



@rest_api_search.doc(security='apikey')
@customer_namespace.route('/searches/')

class Search_Location_Searches(Resource):

        @rest_api_search.marshal_with(get_location_searches)
        @token_required_user
        def get(self, current_user):

                locations=Location_Searches_Model.query.filter_by(customer= current_user.customer).order_by(Location_Searches_Model.created_at).limit(10)

                output=[]
                for loc in locations:
                        data = dict()
                        data['title'] = loc.title
                        data['longitude'] = loc.long
                        data['latitude'] = loc.lat
                        output.append(data)

                return success_status('recent_searches' , output)


