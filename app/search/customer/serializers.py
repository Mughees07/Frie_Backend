from flask_restplus import fields

from app.search import rest_api_search


location_search = rest_api_search.model('location search', {
    'title': fields.String,
    'longitude': fields.Float,
    'latitude': fields.Float,
})

get_location_searches = rest_api_search.model('List of all Location Searches', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'recent_searches': fields.List(fields.Nested(location_search))
})








