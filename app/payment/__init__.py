from flask import Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'

    }
}

payment_blueprint = Blueprint('payment', __name__, url_prefix="/api/v1/payment")

rest_api_payment = Api(payment_blueprint,
                    title='Payment',
                    version='1.0',
                    description='Endpoints related to Payment',
                    authorizations=authorizations,
                    validate=True
                    # All API metadatas
                    )

from app.payment.customer_payment.views import payment_namespace