from flask_restplus import fields
from app.payment import rest_api_payment


pagination = rest_api_payment.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

get_customer_payments_api_model = rest_api_payment.model('get customer payment method', {
    'public_id': fields.String(),
    'last_4_digits': fields.String(default="1234"),
    'type': fields.Integer(default=0)
})

get_all_customer_payment_methods_api_model = rest_api_payment.model('get all customer payment method', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'payment_methods': fields.List(fields.Nested(get_customer_payments_api_model),default=[])
})

add_customer_payment_api_model = rest_api_payment.model('add customer payment', {
    'stripe_token': fields.String(),
    'cardholder_name': fields.String(),
    'last_4_digits': fields.String(),
    'type': fields.Integer(),
})

charge_customer_api_model = rest_api_payment.model('charge customer', {
    'amount': fields.Float()
})