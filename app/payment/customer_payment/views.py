import json
import uuid

import requests
import stripe
from flask import request, make_response, render_template
from flask_restplus import Resource
from app import constants, db
from app.payment import rest_api_payment
from app.payment.customer_payment.controllers import get_payment_method, add_payment_method
from app.payment.customer_payment.serializers import *
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.users_auth.decorators import token_required_user
from app.utils import error_status, success_status, AddObject
from app.payment.customer_payment.models import Customer_Payment_Model

payment_namespace = rest_api_payment.namespace("stripe", description='Operations related to Payments')
stripe.api_key = constants.STRIPE_SECRET_KEY


@payment_namespace.route('/')
class CustomerPayments(Resource):
    @payment_namespace.marshal_with(get_all_customer_payment_methods_api_model)
    @payment_namespace.doc(security='apikey')
    @token_required_user
    def get(self,current_user):
        """
        Return all the payment methods of the current customer
        """
        customer = current_user.customer
        if not customer:
            return error_status(0, "No Customer Found")

        payment_methods = customer.payments_child
        if len(payment_methods) == 0:
            return error_status(0, "No Payment Method Found")

        output = list()
        for method in payment_methods:

            output.append(get_payment_method(method))

        return success_status("payment_methods", payment_methods)

    @payment_namespace.expect(add_customer_payment_api_model)
    @payment_namespace.doc(security='apikey')
    @token_required_user
    def post(self,current_user):
        """
        Given stripe_token, last_4_digits and type this method add a new payment method for current customer
        """
        customer = current_user.customer
        if not customer:
            return error_status(0, "No Customer Found")

        data = request.get_json()
        try:
            stripe_customer = stripe.Customer.create(description="Payment Method for Customer",
                                                     source=data['stripe_token'])
        except stripe.error.StripeError as e:
            return error_status(0, str(e))

        new_payment = Customer_Payment_Model(public_id=str(uuid.uuid4()), last_4_digits=data['last_4_digits'],
                                             type=data['type'], cardholder_name=data['cardholder_name'],
                                             customer_payment_id=stripe_customer.id,
                                             customer_id=customer.id)

        return add_payment_method(new_payment)

@payment_namespace.route('/register/')
class RegisterMerchant(Resource):
    def get(self):

        code = request.args['code']
        state = request.args['state']

        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=state).first()
        if not restaurant:
            return error_status(0, "No, Restaurant Found. The Stripe onboarding process has not succeeded.")

        data = {
            "client_secret": constants.STRIPE_SECRET_KEY,
            "code": code,
            "grant_type": "authorization_code"
        }

        r = requests.post(url="https://connect.stripe.com/oauth/token", data=data)
        response_data = json.loads(r.content.decode('utf-8'))
        if r.status_code == 200:
            restaurant.merchant_payment_id = response_data['stripe_user_id']
            db.session.commit()
        else:
            return error_status(0, "The Stripe onboarding process has not succeeded.")

        headers = {'Content-Type': 'text/html'}
        return make_response(render_template('register.html'), 200, headers)

@payment_namespace.route('/transfer/')
class TransferToConnectAccounts(Resource):
    def post(self):
        data = request.get_json()

        id = "1ebcf763-33c4-4ed0-896c-f314657b8038"

        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
        try:
            stripe.Transfer.create(
                amount=1,
                currency="usd",
                destination=restaurant.merchant_payment_id
            )
        except stripe.error.StripeError as e:
            return error_status(0,str(e))

        return success_status()


# @payment_namespace.route('/charge/<string:id>')
# class ChargeCustomer(Resource):
#     @payment_namespace.expect(charge_customer_api_model)
#     @payment_namespace.doc(security='apikey')
#     @token_required_user
#     def post(self,current_user):
#         """
#         Given payment method id and amount to charge, this method charge the payment method
#         """
#         customer = current_user.customer
#         if not customer:
#             return error_status(0, "No Customer Found")
#
#         data = request.get_json()
#
#         stripe.Charge.create(
#             amount=data['amount'],
#             currency="usd",
#             customer=current_user.customer.payments_child[0].customer_payment_id,  # obtained with Stripe.js
#             description="Charge for jenny.rosen@example.com"
#         )
#
#         return success_status()
