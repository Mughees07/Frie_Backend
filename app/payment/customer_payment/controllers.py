import stripe
from geoalchemy2 import exc

from app import db, constants
from app.constants import STRIPE_SECRET_KEY
from app.payment.customer_payment.models import Customer_Payment_Model
from app.utils import error_status, success_status

stripe.api_key = STRIPE_SECRET_KEY


def get_payment_method(payment_method):

    payment_method_data = dict()
    payment_method_data['public_id'] = payment_method.public_id
    payment_method_data['last_4_digits'] = payment_method.last_4_digits
    payment_method_data['type'] = payment_method.type

    return payment_method_data


def charge_payment_method(amount,payment_id):

    payment_method = Customer_Payment_Model.query.filter_by(public_id=payment_id).first()
    if not payment_method:
        return error_status(0, "No Payment Method Found")

    try:
        stripe.Charge.create(
            amount=int(amount),
            currency="usd",
            customer=payment_method.customer_payment_id,
            description="Charge for customer"
        )

    except stripe.error.StripeError as e:
        print(str(e))
        return False

    return True


def add_payment_method(payment_method):
    try:
        db.session.add(payment_method)
        db.session.commit()
    except exc.SQLAlchemyError as e:

        print(str(e))
        return error_status(constants.ERROR.DB_ERROR, str(e))

    return success_status("payment_method",get_payment_method(payment_method))
