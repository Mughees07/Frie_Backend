from app import db


class Customer_Payment_Model(db.Model):
    __tablename__ = "customer_payment"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String,unique=True)
    cardholder_name = db.Column(db.String)
    last_4_digits = db.Column(db.String)
    type = db.Column(db.Integer)
    customer_payment_id = db.Column(db.String())
    customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    order = db.relationship('Order_Model', backref='payment_method', cascade="all,delete")








