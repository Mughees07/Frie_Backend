from flask import Flask
from flask_sqlalchemy import SQLAlchemy


from config import SECRET_KEY, SQLALCHEMY_DATABASE_URI

app = Flask(__name__)
app.config.from_object('config')
app.config['SECRET_KEY'] = SECRET_KEY

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'
    }
}

#rest_api = Api(app, authorizations=authorizations,title="Frie API")
app.config['SQLALCHEMY_DATABASE_URI'] = SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False # silence the deprecation warning

db = SQLAlchemy(app)




# rest_blueprint = Blueprint('Restaurants', __name__, url_prefix="/api/v1/restaurant")
# rest_api_restaurants= Api(rest_blueprint,
#                     title='Restaurants',
#                     version='1.0',
#                     description='A description',
#                     # All API metadatas
#                     )



#from app.image_upload import image_upload_namespace
from app.restaurant import rest_blueprint
from app.user import user_blueprint
from app.order import order_blueprint
from app.notification import notification_blueprint
from app.payment import payment_blueprint
from app.search import search_blueprint

app.register_blueprint(rest_blueprint)
app.register_blueprint(user_blueprint)
app.register_blueprint(order_blueprint)
app.register_blueprint(notification_blueprint)
app.register_blueprint(payment_blueprint)
app.register_blueprint(search_blueprint)



