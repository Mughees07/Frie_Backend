from flask_restplus import fields
from app.restaurant import rest_api_restaurants


pagination = rest_api_restaurants.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

review_api_model = rest_api_restaurants.model('get review', {
    'public_id': fields.String(),
    'image_url': fields.String(),
    'comment': fields.String(),
    'rating': fields.Float(),
    'time_stamp': fields.DateTime(),
    'user_public_id': fields.String(),
    'user_full_name': fields.String(),
    'user_image_url': fields.String()

})

image_api_model = rest_api_restaurants.model('get image', {
    'image_url': fields.String()
})

get_events_api_model = rest_api_restaurants.model('get events', {
    'public_id': fields.String(),
    'title': fields.String(),
    'description': fields.String(),
    'ticket_price': fields.String(),
    'date_time': fields.DateTime(),
    'image_url': fields.String(),
    'is_favorite': fields.Boolean(),
    'address': fields.String(),
    'long': fields.Float(),
    'lat': fields.Float()
})

event_details_api_model = rest_api_restaurants.model('get event details', {
    'event': fields.Nested(get_events_api_model),
    'images': fields.List(fields.Nested(image_api_model)),
    'total_reviews_count': fields.Integer,
    'reviews': fields.List(fields.Nested(review_api_model))

})

get_event_details = rest_api_restaurants.model('get event details', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'details': fields.Nested(event_details_api_model)
})

get_all_events_api_model = rest_api_restaurants.model('List of all events', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'events': fields.List(fields.Nested(get_events_api_model))
})

add_event_api_model = rest_api_restaurants.model('add event', {
    'title': fields.String(required=True),
    'description': fields.String(required=True),
    'image_url': fields.String(required=True),
    'address': fields.String(required=True),
    'date_time': fields.DateTime(required=True),
    'ticket_price': fields.Float(required=True),
    'long': fields.Float(required=True),
    'lat': fields.Float(required=True)
})

update_event_api_model = rest_api_restaurants.model('update event', {
    'title': fields.String(required=True),
    'description': fields.String(required=True),
    'image_url': fields.String(required=True),
    'address': fields.String(required=True),
    'date_time': fields.DateTime(required=True),
    'long': fields.Float(required=True),
    'lat': fields.Float(required=True)
})
