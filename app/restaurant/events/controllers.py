from flask import jsonify
from sqlalchemy import func

from app import db

# from app.user.utils.models import Images_Model
from app.restaurant.events.models import Event_Model

from app.utils import success_status


def get_all_events(events):
    output = []
    for event in events:
        events_data = dict()
        events_data['public_id'] = event.public_id
        events_data['title'] = event.title
        events_data['description'] = event.description
        events_data['ticket_price'] = event.ticket_price
        events_data['address'] = event.address
        events_data['date_time'] = event.date_time
        events_data['long'] = event.long
        events_data['lat'] = event.lat
        output.append(events_data)

    return success_status("events", output)

def get_one_event(event,favorite=None):

    is_favorite = False
    if favorite:
        is_favorite = True
    events_data = dict()
    events_data['public_id'] = event.public_id
    events_data['title'] = event.title
    events_data['description'] = event.description
    events_data['ticket_price'] = event.ticket_price
    events_data['date_time'] = event.date_time
    events_data['is_favorite'] = is_favorite
    events_data['address'] = event.address
    events_data['long'] = event.long
    events_data['lat'] = event.lat

    return events_data


def updateCuisine(data, cuisine):
    cuisine.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()


def getUserAddress(addresses):
    for address in addresses:
        addresses_data = {}
        addresses_data['public_id'] = address.public_id
        addresses_data['street'] = address.street
        addresses_data['area'] = address.area
        addresses_data['city'] = address.city
        addresses_data['state'] = address.state
        addresses_data['zipcode'] = address.zipcode
        addresses_data['country'] = address.country
        addresses_data['long'] = address.long
        addresses_data['lat'] = address.lat
        return addresses_data

def getOneUserAddress(address):

    addresses_data = {}
    addresses_data['public_id'] = address.public_id
    addresses_data['street'] = address.street
    addresses_data['area'] = address.area
    addresses_data['city'] = address.city
    addresses_data['state'] = address.state
    addresses_data['zipcode'] = address.zipcode
    addresses_data['country'] = address.country
    addresses_data['long'] = address.long
    addresses_data['lat'] = address.lat
    return addresses_data


def get_nearby_events(current_long,current_lat,radius):

    point = 'POINT({} {})'.format(current_long, current_lat)

    events_within_radius = Event_Model.query.filter(func.ST_Distance(Event_Model.geo_location, point) <= radius).all()

    return get_all_events(events_within_radius)



