from geoalchemy2 import Geometry
from sqlalchemy.orm import relationship

from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp


class Event_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "events"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    title = db.Column(db.String(200))
    address = db.Column(db.String(200))
    description = db.Column(db.String(200))
    image_url = db.Column(db.String(200))
    ticket_price = db.Column(db.Float)
    date_time = db.Column(db.DateTime)
    lat = db.Column(db.Float)
    long = db.Column(db.Float)
    geo_location = db.Column(Geometry(geometry_type="POINT"))
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant_profile.id"))
    reviews = db.relationship('Reviews_Model', backref='event')
    # event_photos = db.relationship('Event_Photos_Model', backref='event')

# class Event_Photos_Model(Resource):
#
#     __tablename__ = "event_photos"
#     id = db.Column(db.Integer, primary_key=True)
#     public_id = db.Column(db.String(200))
#     image_url = db.Column(db.String(200))
#     event_id = db.Column(db.Integer, db.ForeignKey("events.id"))