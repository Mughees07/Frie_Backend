import uuid
import app
from flask import request
from flask_restplus import Resource

from app.restaurant.events.models import Event_Model
from app.restaurant.events.serializers import *
from app.restaurant.events.controllers import *
from app.restaurant import rest_api_restaurants
from app.user.customer_profile.models import Customer_Favourites_Model
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.reviews.controllers import get_all_reviews
from app.user.users_auth.decorators import token_required, token_required_user
from app.user.utils.models import Images_Model
from app.utils import AddObject, DeleteObject, error_status
from app.constants import NEARBY_RADIUS
events_namespace = rest_api_restaurants.namespace('events', description='Operations related to Events')

@events_namespace.route('/all')
class AllEvents(Resource):
    @events_namespace.doc(security='apikey')
    @rest_api_restaurants.marshal_with(get_all_events_api_model)
    def get(self):
        """
        return the Events associated with the restaurant
        """
        events = Event_Model.query.all()

        return get_all_events(events)

@events_namespace.route('/<string:id>')
class Events(Resource):
    """"""
    @events_namespace.doc(security='apikey',params={'id': 'Event ID'})
    @token_required_user
    #@rest_api_restaurants.marshal_with(get_event_details)
    def get(self,id,current_user):
        """
        return the Events associated with the restaurant
        """
        event = Event_Model.query.filter_by(public_id=id).first()

        if not event:
            return error_status(0, "No Event found!")

        customer = current_user.customer
        favorite = Customer_Favourites_Model.query.filter_by(customer=customer, type=1, reference_type=1,
                                                             reference_id=event.public_id).first()
        images = Images_Model.query.filter_by(type=2).filter_by(type_id=event.public_id).all()
        images_urls = [{"image_url":i.image_url} for i in images]
        event_reviews = event.reviews
        total_reviews_count = len(event_reviews)
        reviews_data = get_all_reviews(event_reviews)
        events_data = get_one_event(event,favorite)
        details = {
            "event":events_data, "images_urls":images_urls, "reviews":reviews_data['reviews'],
            "total_reviews_count":total_reviews_count
        }
        return success_status("details", details)


    @events_namespace.doc(security='apikey',params={'id': 'Restaurant ID'})
    @token_required
    @rest_api_restaurants.expect(add_event_api_model)
    def post(self,id):
        """
        Given Title, Description, Ticket Price, Address, Long and Lat, this method creates a new event for given restaurant
        """
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()

        if not restaurant:
            return error_status(0, "No Restaurant found!")

        data = request.get_json()
        point = 'POINT({} {})'.format(data['long'], data['lat'])
        new_event = Event_Model(public_id=str(uuid.uuid4()), title=data['title'], description=data['description'],
                                      ticket_price=data['ticket_price'], image_url=data['image_url'],
                                      address=data['address'], date_time=data['date_time'],
                                      long=data['long'],
                                      lat=data['lat'],restaurant_id=restaurant.id , geo_location= point)

        return AddObject(new_event)

# @events_namespace.route('/<string:id>')
# class Event_Item(Resource):
    @rest_api_restaurants.expect(update_event_api_model)
    @events_namespace.doc(security='apikey',params={'id': 'Event ID'})
    @token_required
    def put(self,id):
        """
        Given Title, Description, Ticket Price, Address, Long and Lat, this method updates the event
        """
        event = Event_Model.query.filter_by(public_id=id).first()

        if not event:
            return error_status(0, "No Event found!")

        data = request.get_json()

        event.title = data['title']
        event.description = data['description']
        event.ticket_price = data['ticket_price']
        event.image_url = data['image_url']
        event.address = data['address']
        event.date_time = data['date_time']
        event.long = data['long']
        event.lat = data['lat']
        point = 'POINT({} {})'.format(data['long'], data['lat'])
        event.geo_location = point

        db.session.commit()
        return success_status()

    @events_namespace.doc(security='apikey',params={'id': 'Event ID'})
    @token_required
    def delete(self,id):
        """
        Given public id of an event, this method deletes the address
        """
        event = Event_Model.query.filter_by(public_id=id).first()

        if not event:
            return error_status(0, "No Event found!")

        return DeleteObject(event)

parser = rest_api_restaurants.parser()
parser.add_argument('lat', required=True, help="Latitude",type=float)
parser.add_argument('long', required=True, help="Longitude", type=float)
class Nearby_Events(Resource):
    @events_namespace.doc(parser=parser)
    @token_required
    @rest_api_restaurants.marshal_with(get_all_events_api_model)
    def get(self):
        """
        return the Events associated with the restaurant
        """
        long = request.args['long']
        lat = request.args['lat']

        return get_nearby_events(long,lat,NEARBY_RADIUS)