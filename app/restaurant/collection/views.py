import uuid

from flask import request
from flask_restplus import Resource

from app import db
from app.restaurant import rest_api_restaurants
from app.restaurant.collection.controllers import *
from app.restaurant.collection.serializers import get_all_banners_api_model, add_banner_api_model, \
    update_banner_api_model
from app.search.customer.controllers import GetFilteredRestaurants
from app.user.customer_profile.models import User_Followings_Model, Customer_Favourites_Model
from app.user.users_auth.decorators import token_required_skip_login, token_required_user
from app.utils import error_status, AddObject, DeleteObject

homescreen_collection_namespace = rest_api_restaurants.namespace('home_collections', description='Operations related to collection')
deliveryLists_collection_namespace = rest_api_restaurants.namespace('delivery_lists', description='Operations related to collection')
banners_namespace = rest_api_restaurants.namespace('banners', description='Operations related to Banners')
feeds_namespace = rest_api_restaurants.namespace('user feeds', description='Operations related to Feeds')
##########################################Home Screen ##################################################################

location_parser = rest_api_restaurants.parser()
location_parser.add_argument('long', required=True, help="Longitude" , type=float)
location_parser.add_argument('lat', required=True, help="Latitude" ,type=float)

@homescreen_collection_namespace.route('')
#@rest_api_restaurants.doc(parser=location_parser)
class Homescreen_Collection(Resource):

    @feeds_namespace.doc(security='apikey')
    @token_required_skip_login
    def get(self, current_user):
        """
        Returns all the Lists of Home page
        """
        #long = request.args['long']
        #lat = request.args['lat']

        return GetHomeList(current_user , 12, 12)

@homescreen_collection_namespace.route('/good_spots_near_you')
@rest_api_restaurants.doc(parser=location_parser)
class Homescreen_Collection_Good_Spots(Resource):

    @token_required_skip_login
    def get(self,current_user):
        """
        Returns all the good spots(restaurants) near you
        """

        long = request.args['long']
        lat = request.args['lat']

        return GetGoodSpotsNear(current_user,long,lat)

@homescreen_collection_namespace.route('/trending')
class Homescreen_Collection_Trending(Resource):
    def get(self):
        """
        Returns all the trending restaurants
        """
        return GetTrending()

@homescreen_collection_namespace.route('/recently_visited_places')
class Homescreen_Collection_Recently_Visited(Resource):

    @token_required_skip_login
    def get(self,current_user):
        """
        Returns all the recently visited restaurants
        """
        return GetRecentlyVisited(current_user)


@homescreen_collection_namespace.route('/events_near_you')
class Homescreen_Collection_Events_Near(Resource):
    def get(self):
        """
        Returns all the events near you
        """
        long = request.args['long']
        lat = request.args['lat']

        return GetEventsNear(long, lat)

###################################### Other Lists ######################################################################




@deliveryLists_collection_namespace.route('')
#@rest_api_restaurants.doc(parser=location_parser)
class DeliveryList_Collection(Resource):

    def get(self):
        """
        Returns all the cuisines
        """

        # long = request.args['long']
        # lat = request.args['lat']


        return delivery_lists(12,12)


@deliveryLists_collection_namespace.route('/banners')
class DeliveryList_Collection_Banners(Resource):
    def get(self):
        """
        Returns all the good spots(restaurants) near you
        """
        return GetHomeBanners()

@deliveryLists_collection_namespace.route('/new_on_frie')
class DeliveryList_Collection_New_On_Frie(Resource):
    def get(self):
        """
        Returns all the trending restaurants
        """
        return GetNewOnFrie()


@deliveryLists_collection_namespace.route('/nearby')
@rest_api_restaurants.doc(parser=location_parser)
class DeliveryList_Collection_Nearby(Resource):
    def get(self):
        """
        Returns all the recently visited restaurants
        """
        long = request.args['long']
        lat = request.args['lat']

        return GetNearBy(long, lat)


@deliveryLists_collection_namespace.route('/cuisines')

class DeliveryList_Collection_Cuisines(Resource):
    def get(self):
        """
        Returns all the events near you
        """
        return GetAllCuisines()



@deliveryLists_collection_namespace.route('/health_is_wealth')
class DeliveryList_Collection_Health(Resource):
    def get(self):
        """
        Returns all the recently visited restaurants
        """
        return GetHealthIsWealth()

@deliveryLists_collection_namespace.route('/popular_today')
class DeliveryList_Collection_Popular_Today(Resource):
    def get(self):
        """
        Returns all the recently visited restaurants
        """
        return GetPopularToday(None, True)

@deliveryLists_collection_namespace.route('/our_favourites/')
class DeliveryList_Collection_Our_Favourites(Resource):
    def get(self):
        """
        Returns all the recently visited restaurants
        """
        return GetOurFavourites(None,True)

@deliveryLists_collection_namespace.route('/free_deliveries/')
class DeliveryList_Collection_Free(Resource):
    def get(self):
        """
        Returns all the recently visited restaurants
        """
        return GetFreeDeliveries()


###############################BANNERS###########################


@banners_namespace.route('')
class BannersCollection(Resource):
    @banners_namespace.marshal_with(get_all_banners_api_model)
    def get(self):
        """
        Returns all the Banners of Home page
        """
        banners = Homescreen_Banner_Model.query.all()
        output = list()
        for banner in banners:
            banner_data = dict()
            banner_data['public_id'] = banner.public_id
            banner_data['title'] = banner.title
            banner_data['sub_title'] = banner.sub_title
            banner_data['image_url'] = banner.image_url
            output.append(banner_data)

        return success_status('banners',output)

    @banners_namespace.expect(add_banner_api_model)
    def post(self):
        """
        Returns all the Banners of Home page
        """
        data = request.get_json()

        new_banner = Homescreen_Banner_Model(public_id=str(uuid.uuid4()),title=data['title'],sub_title=data['sub_title'],
                                             image_url=data['image_url'])

        return AddObject(new_banner)


@banners_namespace.route('/<string:id>')
class BannersCollectionOneItem(Resource):
    @banners_namespace.expect(update_banner_api_model)
    @banners_namespace.doc(params={'id': 'Banner Id'})
    def put(self,id):
        banner = Homescreen_Banner_Model.query.filter_by(public_id=id).first()
        if not banner:
            return error_status(0, "No Banner Found")

        data = request.get_json()

        banner.title = data['title']
        banner.sub_title = data['sub_title']
        banner.image_url = data['image_url']

        db.session.commit()
        return success_status()

    @banners_namespace.doc(params={'id': 'Banner Id'})
    def delete(self, id):

        banner = Homescreen_Banner_Model.query.filter_by(public_id=id).first()

        if not banner:
            return error_status(0, "No Banner Found")

        return DeleteObject(banner)

@deliveryLists_collection_namespace.route('/dietary_restricted_restaurants/<string:id>')
@deliveryLists_collection_namespace.doc(params={'id': 'Dietary Restriction ID'})
class DeliveryList_Collection_Dietary_Restriction(Resource):
    '''
    akjs

    '''

    def get(self,id):
        return GetDietartRestrictedRestaurants(id)


@deliveryLists_collection_namespace.route('/cuisine_restaurants/<string:id>')
@deliveryLists_collection_namespace.doc(params={'id': 'Cuisine ID'})
class DeliveryList_Collection_Cusine_Restaurants(Resource):
    '''
    akjs
    '''

    def get(self, id):
        return GetCuisineRestaurants(id)

parser = rest_api_restaurants.parser()
parser.add_argument('location_title', required=True, help="Location Title")
parser.add_argument('lat', required=True, help="Latitude" ,type=float)
parser.add_argument('long', required=True, help="Longitude" , type=float)
parser.add_argument('sort_order',  help="Sort Order" , type = int)
parser.add_argument('book_table_available',  help="Is Book Table Available", type= bool)
parser.add_argument('open_now', help="Check if restaurant is open" , type= bool)
parser.add_argument('min_distance',  help="Minimum Radius for distance" , type = float)
parser.add_argument('max_distance',  help="Max Radius for distance" , type= float)
parser.add_argument('dietary_restrictions[]', help="Dietary Restriction", action='append')

@feeds_namespace.route('/')
class UserNewFeeds(Resource):
    @feeds_namespace.doc(security='apikey',parser=parser)
    @token_required_user
    def get(self, current_user):
        customer = current_user.customer
        if not customer:
            return error_status(0,"No Customer Found")

        location_title = request.args['location_title']
        long = request.args['long']
        lat = request.args['lat']
        sort_order = request.args['sort_order']
        book_table_available = request.args['book_table_available']
        open_now = request.args['open_now']
        max_distance = request.args['max_distance']
        min_distance = request.args['min_distance']
        dietary_restrictions = request.args.getlist('dietary_restrictions[]')

        followings = User_Followings_Model.query.filter_by(follower_id=customer.id).all()
        following_ids = [following.following.public_id for following in followings]
        fav_restaurants = Customer_Favourites_Model.query.filter(Customer_Favourites_Model.reference_type==0,
                                                                 Customer_Favourites_Model.reference_id.in_(following_ids)).all()
        return GetFilteredRestaurants(fav_restaurants, location_title, lat, long, sort_order, book_table_available,
                        open_now, min_distance, max_distance, dietary_restrictions, current_user.customer)