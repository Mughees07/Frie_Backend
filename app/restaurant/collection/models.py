from sqlalchemy.orm import relationship, backref

from app import db
from app.user.restaurant_profile.models import Restaurant_Profile_Model


class Homescreen_Banner_Model(db.Model):
    __tablename__ = "homescreen_banner"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String,unique=True)
    title = db.Column(db.String(200))
    sub_title = db.Column(db.String(200))
    image_url = db.Column(db.String(100), unique=True)








