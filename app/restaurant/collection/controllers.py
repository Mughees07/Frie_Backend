import requests
from sqlalchemy import func

from app import constants
from app.restaurant.collection.models import Homescreen_Banner_Model
from app.restaurant.dietary_restrictions.controllers import getAllCollections, getAllRestrictions
from app.restaurant.dietary_restrictions.models import Restaurants_Dietary_Restriction_Model, Dietary_Restriction_Model
from app.restaurant.events.controllers import get_all_events, get_one_event
from app.restaurant.events.models import Event_Model
from app.restaurant.food_categories.controllers import getAllCuisinesObjects
from app.restaurant.food_categories.models import Cuisine_Model
from app.restaurant.restaurant_food_categories.models import Restaurant_Dish_Model
from app.user.addresses.models import Addresses_Model
from app.user.restaurant_profile.controllers import getRestaurant
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.utils import success_status


def GetRestaurantFromID(id):
    r = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
    return r


def FetchData(URL , pay_load= None):
    #URL = "http://104.248.207.194:8887/ml/home/13123213"

    # sending get request and saving the response as response object
    if(pay_load):
        r = requests.get(url=URL,params=pay_load)
        print(r.url)
    else:
        r = requests.get(url=URL)
        print(r.url)


    # extracting data in json format
    data = r.json()
    print(data)

    return data
    # extracting latitude, longitude and formatted address
    # of the first matching location

def GetTrending():

    data= FetchData(constants.ML_TRENDING_LIST_URL)
    output = []

    for d in data['trending']:
        r = GetRestaurantFromID(d)
        if(r):
            output.append(getRestaurant(r))

    return success_status("restaurants", output)

def GetGoodSpotsNear(user_id, long, lat):

    payload = {'long': long , 'lat': lat}
    data = FetchData(constants.ML_RECOMMENDATION_LIST_URL+str(user_id),payload)
    output = {}
    output['Good_Spots_near_you'] = []
    for d in data['recommendations']:
        r = GetRestaurantFromID(d)
        if (r):
            output['Good_Spots_near_you'].append(getRestaurant(r))

    return success_status("restaurants", output)

def GetRecentlyVisited(user_id):

    data = FetchData(constants.ML_RECENTLY_VISITED_LIST_URL+str(user_id))
    output = {}
    output['Recently_Visited_Places'] = []
    for d in data['recently_visited']:
        r = GetRestaurantFromID(d)
        if (r):
            output['Recently_Visited_Places'].append(getRestaurant(r))
    return success_status("restaurants", output)

def GetEventsNear(current_long, current_lat):

    point = 'POINT({} {})'.format(current_long, current_lat)

    events_within_radius = Event_Model.query.filter(
        func.ST_Distance(Event_Model.geo_location, point) < constants.NEARBY_RADIUS).all()

    output = list()
    for e in events_within_radius:
        output.append(get_one_event(e))

    return success_status("events", output)



def GetHomeList(current_user, long , lat):
    payload = {'long': long, 'lat': lat}
    output = {}
    output['status'] = 1
    output['message'] = "success"
    output['good_spots'] = []
    output['trendings'] = []
    output['events_near_by'] = []
    output['recent_places'] = []
    output['dietary_restrictions'] = []
    if(current_user):
        data = FetchData(constants.ML_HOME_LIST_URL+ str(current_user.public_id), payload)
        for d in data['recently_visited']:
            r = GetRestaurantFromID(d)
            if (r):
                output['recent_places'].append(getRestaurant(r))

        for d in data['recommendations']:
            r = GetRestaurantFromID(d)
            if (r):
                output['good_spots'].append(getRestaurant(r))
    else:
        data= FetchData(constants.ML_TRENDING_LIST_URL)

        for d in data['trending']:
            r = GetRestaurantFromID(d)
            if (r):
                output['trendings'].append(getRestaurant(r))

    events = GetEventsNear(long, lat)
    output['events_near_by'] = events['events']

    restrictions = Dietary_Restriction_Model.query.all()
    output['dietary_restrictions'] = getAllRestrictions(restrictions)

    #output['dietary_restrictions'] =
    #output[''] =

    return output


####################################Other Lists ###########################################
def delivery_lists(current_long, current_lat):

    output={}
    output['status'] = 1
    output['message'] = "success"
    output['free_delivery'] = []

    restaurants = Restaurant_Profile_Model.query.filter_by(delivery_price=0).limit(10)
    for r in restaurants:
        output['free_delivery'].append(getRestaurant(r))

    output["banners"] = []
    banners = Homescreen_Banner_Model.query.all()
    for b in banners:
        output["banners"].append(getHomeScreenBanner(b))

    output['new_on_frie'] = []
    restaurants = Restaurant_Profile_Model.query.order_by(Restaurant_Profile_Model.created_at).limit(10)
    for r in restaurants:
        output['new_on_frie'].append(getRestaurant(r))

    output['nearby'] = []
    ten_miles_in_meters = 16093.4
    point = 'POINT({} {})'.format(current_long,current_lat)

    addresses_within_radius = Addresses_Model.query.filter(Addresses_Model.restaurant_id.isnot(None)).filter(func.ST_Distance(Addresses_Model.geo_location,point) < ten_miles_in_meters).all()

    output['Nearby Restaurants'] = list()
    for ad in addresses_within_radius:
        output['Nearby Restaurants'].append(getRestaurant(ad.restaurant_addresses))

    output['cuisines'] = []
    cuisines = Cuisine_Model.query.limit(10)
    output['cuisines'] = getAllCuisinesObjects(cuisines)
    data = FetchData(constants.ML_DELIVERY_LIST_URL)
    output['popular_today'] = GetPopularToday(data)
    output['our_favourites'] = GetOurFavourites(data)
    output['health_is_wealth'] = []


    # for c in data["Recently visited places"]:
    #     r = Restaurant_Profile_Model.query.filter_by(public_id=c['public_id']).first()
    #     output['Recently visited places'].append(getRestaurant(r))

    return output

def GetFreeDeliveries():
    restaurants = Restaurant_Profile_Model.query.filter_by(delivery_price=0).all()
    output = []
    for r in restaurants:
        output.append(getRestaurant(r))
    return success_status("restaurants", output)

def GetHomeBanners():
    banners = Homescreen_Banner_Model.query.all()
    output = []
    for r in banners:
        output.append(getRestaurant(r))
    return success_status("banners", output)

def GetNewOnFrie():
    restaurants = Restaurant_Profile_Model.query.order_by(Restaurant_Profile_Model.created_at).all()
    output = []
    for r in restaurants:
        output.append(getRestaurant(r))
    return success_status("restaurants", output)

def GetNearBy(current_long,current_lat):
    radius = constants.NEARBY_RADIUS
    point = 'POINT({} {})'.format(current_long, current_lat)

    addresses_within_radius = Addresses_Model.query.filter(Addresses_Model.restaurant_id.isnot(None)).filter(
        func.ST_Distance(Addresses_Model.geo_location, point) < radius).all()
    output =[]
    for ad in addresses_within_radius:
        output.append(getRestaurant(ad.restaurant_addresses))

    return success_status("restaurants", output)

def GetAllCuisines():

    cuisines = Cuisine_Model.query.all()
    return success_status("cuisines", getAllCuisinesObjects(cuisines))

def GetHealthIsWealth():
    restaurants = Restaurant_Profile_Model.query.all()
    output = []
    for r in restaurants:
        output.append(getRestaurant(r))
    return success_status("restaurants", output)


def GetPopularToday(data , has_url=False):
    if(has_url):
        data= FetchData(constants.ML_POPULAR_TODAY_LIST_URL)

    output = {}
    output['popular_today'] = []
    for d in data['popular_today']:
        r = GetRestaurantFromID(d)
        if (r):
            output['popular_today'].append(getRestaurant(r))
    if(data):
        return output['popular_today']

    return success_status("restaurants", output)




def GetOurFavourites(data,has_url=False):
    if (has_url):
        data = FetchData(constants.ML_OUR_FAVOURITES_LIST_URL)
    output = {}
    output['our_favourites'] = []
    for d in data['our_favorites']:
        r = GetRestaurantFromID(d)
        if (r):
            output['our_favourites'].append(getRestaurant(r))

    if (data):
        return output['our_favourites']

    return success_status("restaurants", output)


#############################################################################################
def GetDietartRestrictedRestaurants(d_id):
    dietary = Dietary_Restriction_Model.query.filter_by(public_id = d_id).first()
    restaurant_restrictions = Restaurants_Dietary_Restriction_Model.query.filter_by(dietary_restriction=dietary).all()
    output={}
    for r in restaurant_restrictions:
        data = getRestaurant(r.restaurant)
        output.append(data)

    return success_status("restaurants",output)


def GetCuisineRestaurants(c_id):

    output=[]
    dishes = Restaurant_Dish_Model.query.all()
    for d in dishes:
       if d.dish.cuisine_id==c_id:
           data = getRestaurant(d.restaurant)
           output.append(data)

    return success_status("restaurants", output)
##################################Banner List################################################

def getHomeScreenBanner(obj):
   data = {}
   data['public_id']=obj.public_id
   data['title'] = obj.title
   data['sub_title'] = obj.sub_title
   data['image_url'] = obj.image_url

   return data




