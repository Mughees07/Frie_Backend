from flask import Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'
    }
}

rest_blueprint = Blueprint('Restaurants', __name__, url_prefix="/api/v1/restaurant")
rest_api_restaurants= Api(rest_blueprint,
                    title='Restaurants',
                    version='1.0',
                    description='A description',
                    authorizations=authorizations
                    # All API metadatas
                    )



from app.restaurant.food_categories.views import cuisine_namespace,dish_namespace,topping_namespace,style_namespace,dishes_collection_namespace
from app.restaurant.restaurant_food_categories.views import restaurant_dish_namespace,restaurant_dish_topping_namespace,restaurant_dish_style_namespace
from app.restaurant.events.views import events_namespace
from app.restaurant.dietary_restrictions.views import dietary_restriction_namespace,restaurant_dietary_restriction_namespace
from app.restaurant.collection.views import homescreen_collection_namespace
from app.restaurant.collection.views import banners_namespace
from app.restaurant.collection.views import feeds_namespace








