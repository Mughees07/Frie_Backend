from sqlalchemy.orm import backref

from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp

class Cuisine_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "cuisine"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String,unique=True)
    name = db.Column(db.String(100), unique=True)
    image_url = db.Column(db.String(100), unique=True)
    dishes = db.relationship('Dish_Model', cascade="all, delete-orphan" ,backref='dish_cuisine')

class Dish_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "dish"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)
    name = db.Column(db.String(100), unique=True)
    image_url = db.Column(db.String(100), unique=True)
    cuisine_id = db.Column(db.Integer, db.ForeignKey("cuisine.id"))

class Topping_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "topping"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)
    name = db.Column(db.String(100), unique=True)


class Style_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "style"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)
    name = db.Column(db.String(100), unique=True)

class Dishes_Collection_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "dish_collection"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)
    name = db.Column(db.String(100), unique=True)
    #main_collection_id = db.Column(db.Integer, db.ForeignKey("dish_main_collection.id"))
    #collections = db.relationship('Dishes_Main_Collection_Model', backref=backref("main_collection_children", cascade="all,delete"))

class Dishes_Main_Collection_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "dish_main_collection"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)
    name = db.Column(db.String(100), unique=True)


