import uuid

from flask import jsonify

from app import db

#################Cuisines#####################
from app.restaurant.food_categories.models import Dish_Model, Dishes_Collection_Model
from app.utils import success_status


def getAllCuisines(cuisines):
    output = []
    for c in cuisines:
        cuisine_data = {}
        cuisine_data['public_id'] = c.public_id
        cuisine_data['name'] = c.name
        output.append(cuisine_data)

    return  success_status("cuisines", output)

def getAllCuisinesObjects(cuisines):
    output = []
    for c in cuisines:
        cuisine_data = {}
        cuisine_data['public_id'] = c.public_id
        cuisine_data['name'] = c.name
        output.append(cuisine_data)

    return output

def updateCuisine(data, cuisine):
    cuisine.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()



######################Dishes###################################

def getAllDishes(dishes):
    output = []
    for c in dishes:
        dish_data = {}
        dish_data['public_id'] = c.public_id
        dish_data['name'] = c.name
        dish_data['image_url'] = c.image_url

        output.append(dish_data)

    return  success_status("dishes", output)


def updateDishes(data, dishes):
    dishes.name = data['name']
    dishes.image_url = data['image_url']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()


def AddCuisineDishes(data , current_cuisine):

    for d in data["dishes"]:
        current_cuisine.dishes.append(Dish_Model(public_id=str(uuid.uuid4()),name=d["name"],image_url=d['image_url']))
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})

    return success_status()

#########################Toppings#################################

def getAllToppings(toppings):
    output = []
    for c in toppings:
        toppings_data = {}
        toppings_data['public_id'] = c.public_id
        toppings_data['name'] = c.name
        output.append(toppings_data)

    return  success_status("toppings", output)


def updateToppings(data, toppings):
    toppings.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()


############################Styles#####################################

def getAllStyles(styles):
    output = []
    for c in styles:
        styles_data = {}
        styles_data['public_id'] = c.public_id
        styles_data['name'] = c.name
        output.append(styles_data)

    return  success_status("styles", output)


def updateStyles(data, styles):
    styles.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()

############################Dishes Collection####################################
def getAllDishesCollections(dishes_collection):
    output = []
    for c in dishes_collection:
        dishes_collection_data = {}
        dishes_collection_data['public_id'] = c.public_id
        dishes_collection_data['name'] = c.name
        output.append(dishes_collection_data)

    return  success_status("dishes_collections", output)


def updateDishesCollections(data, dishes_collection):
    dishes_collection.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()


#################################################################

def AddSubCollections(data):

        for c in data['collections']:
            obj=Dishes_Collection_Model(public_id=str(uuid.uuid4()), name=c["name"])
            db.session.add(obj)
        try:
            db.session.commit()
        except Exception as e:
            return jsonify({"status": 0, 'message': str(e)})

        return success_status()


def getAllCollections(collections):
    output = []
    for c in collections:
        collection_data = {}
        collection_data['public_id'] = c.public_id
        collection_data['name'] = c.name

        output.append(collection_data)

    return  success_status("dishes_collections", output)