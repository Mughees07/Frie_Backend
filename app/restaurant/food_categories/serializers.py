from flask_restplus import fields
from app.restaurant import rest_api_restaurants



##############Cuisines



cuisine_api_model = rest_api_restaurants.model('cuisine', {
    'public_id': fields.String(),
    'name': fields.String()
})

add_cuisine_api_model = rest_api_restaurants.model('add cuisine', {
    'name': fields.String(required=True)
})

get_all_cuisine_api_model = rest_api_restaurants.model('List of all cuisines', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'cuisines': fields.List(fields.Nested(cuisine_api_model))
})

delete_cuisine_api_model = rest_api_restaurants.model('delete cuisine', {
    'public_id': fields.String(required=True)
})



#################Dishes##############################

dish_api_model = rest_api_restaurants.model('dish', {
    'public_id': fields.String(),
    'name': fields.String(),
    'image_url' : fields.String(),
})

add_dish_api_model = rest_api_restaurants.model('add dish', {
    'name': fields.String(required=True),
    'image_url' : fields.String(required=True),
})



get_all_dish_api_model = rest_api_restaurants.model('List of all dishes', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dishes': fields.List(fields.Nested(dish_api_model))
})

delete_dish_api_model = rest_api_restaurants.model('delete dish', {
    'public_id': fields.String(required=True)
})
##################### Cuisine Dishes ###########################
add_cuisine_dishes_api_model= rest_api_restaurants.model('add cuisine_dishes', {
    'dishes': fields.List(fields.Nested(add_dish_api_model))
})
##############################Toppings ##################

topping_api_model = rest_api_restaurants.model('topping', {
    'public_id': fields.String(),
    'name': fields.String()
})

add_topping_api_model = rest_api_restaurants.model('add topping', {
    'name': fields.String(required=True)
})

get_all_topping_api_model = rest_api_restaurants.model('List of all toppings', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'toppings': fields.List(fields.Nested(topping_api_model))
})

delete_topping_api_model = rest_api_restaurants.model('delete topping', {
    'public_id': fields.String(required=True)
})

############################Style##############################

style_api_model = rest_api_restaurants.model('style', {
    'public_id': fields.String(),
    'name': fields.String()
})

add_style_api_model = rest_api_restaurants.model('add style', {
    'name': fields.String(required=True)
})

get_all_style_api_model = rest_api_restaurants.model('List of all styles', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'styles': fields.List(fields.Nested(style_api_model))
})

delete_style_api_model = rest_api_restaurants.model('delete style', {
    'public_id': fields.String(required=True)
})


#############################Dishes Collection #############################


dishes_collection_api_model = rest_api_restaurants.model('dishes collection', {
    'public_id': fields.String(),
    'name': fields.String(),
})

add_dishes_collection_api_model = rest_api_restaurants.model('add dishes collection', {
    'name': fields.String(required=True)
})

add_sub_collections_api_model= rest_api_restaurants.model('add sub collections', {
    'collections': fields.List(fields.Nested(add_dishes_collection_api_model))
})

get_dishes_collection_api_model = rest_api_restaurants.model('List of all dishes collection', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dishes_collections': fields.List(fields.Nested(dishes_collection_api_model))
})

delete_dishes_collection_api_model = rest_api_restaurants.model('delete dishes collection', {
    'public_id': fields.String(required=True)
})

#######################################Dishes Main Collection################################

dishes_main_collection_api_model = rest_api_restaurants.model('dishes main collection', {
    'public_id': fields.String(),
    'name': fields.String()
})

add_dishes_main_collection_api_model = rest_api_restaurants.model('add dishes main collection', {
    'name': fields.String(required=True)
})

get_main_dishes_collection_api_model = rest_api_restaurants.model('List of all main dishes collection', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dishes_collections': fields.List(fields.Nested(dishes_main_collection_api_model))
})

delete_main_dishes_collection_api_model = rest_api_restaurants.model('delete main dishes collection', {
    'public_id': fields.String(required=True)
})