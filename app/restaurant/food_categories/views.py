import uuid

from flask import request
from flask_restplus import Resource

from app.restaurant.food_categories.serializers import *
from app.restaurant.food_categories.models import *
from app.utils import AddObject, DeleteObject, error_status, AddObjects
from app.restaurant.food_categories.controllers import *

cuisine_namespace = rest_api_restaurants.namespace('cuisine', description='Operations related to cuisines')
dish_namespace = rest_api_restaurants.namespace('dish', description='Operations related to dishes')
topping_namespace = rest_api_restaurants.namespace('topping', description='Operations related to toppings')
style_namespace = rest_api_restaurants.namespace('style', description='Operations related to styles')
dishes_collection_namespace = rest_api_restaurants.namespace('dishes_collection', description='Operations related to dishes dietary_restrictions')
main_dishes_collection_namespace = rest_api_restaurants.namespace('main_dishes_collection', description='Operations related to main dishes collections')


@cuisine_namespace.route('/')
class Cuisine(Resource):
    @rest_api_restaurants.marshal_with(get_all_cuisine_api_model)
    def get(self):
        """
        Returns all the cuisines
        """
        cuisines = Cuisine_Model.query.all()
        return getAllCuisines(cuisines)

    @rest_api_restaurants.expect(add_cuisine_api_model)
    def post(self):
        """
        Given Cuisine name this method creates a new cuisine
        """
        data = request.get_json()
        return AddObject(Cuisine_Model(public_id=str(uuid.uuid4()),name=data["name"]))


    @rest_api_restaurants.expect(dish_api_model)
    def put(self):
        """
         Given Cuisine object  updates given cuisine
        """
        data = request.get_json()
        current_cuisine = Cuisine_Model.query.filter_by(public_id=data['public_id']).first()
        return updateCuisine(data, current_cuisine)

    @rest_api_restaurants.expect(delete_cuisine_api_model)
    def delete(self):
        """
        Given Cuisine id this delete cuisine
        """
        data = request.get_json()
        cuisine = Cuisine_Model.query.filter_by(public_id=data['public_id']).first()
        return DeleteObject(cuisine)


@dish_namespace.route('/')
class Dish(Resource):
    @rest_api_restaurants.marshal_with(get_all_dish_api_model)
    def get(self):
        """
        Returns all the dishes stored in DB
        """
        dishes = Dish_Model.query.all()
        return getAllDishes(dishes)


    @rest_api_restaurants.expect(dish_api_model)
    def put(self):
        """
         Given Dish name and its features this method updates dish
        """
        data = request.get_json()
        current_dish = Dish_Model.query.filter_by(public_id=data['public_id']).first()
        return updateDishes(data, current_dish)

    @rest_api_restaurants.expect(delete_dish_api_model)
    def delete(self):
        """
        Given Dish id this delete cuisine
        """
        data = request.get_json()
        dish = Dish_Model.query.filter_by(public_id=data['public_id']).first()
        return DeleteObject(dish)

@dish_namespace.route('/<string:id>')
@dish_namespace.doc(params={'id' : 'Cuisine ID'})
class Cuisine_Dish(Resource):
    @rest_api_restaurants.expect(add_cuisine_dishes_api_model)
    def post(self,id):
        """
         Given cuisine id adds dishes provided
        """
        data = request.get_json()
        current_cuisine = Cuisine_Model.query.filter_by(public_id=id).first()
        return AddCuisineDishes(data, current_cuisine)

    @rest_api_restaurants.marshal_with(get_all_dish_api_model)
    def get(self,id):
        """
        Returns all the dishes for a specific cuisine
        """
        cuisine = Cuisine_Model.query.filter_by(public_id=id).first()

        if cuisine:
            return getAllDishes(cuisine.dishes)
        else:
            return error_status(0,"Cuisine not found")

@topping_namespace.route('/')
class Topping(Resource):
    @rest_api_restaurants.marshal_with(get_all_topping_api_model)
    def get(self):
        """
        returns all the toppings
        """
        toppings = Topping_Model.query.all()
        return getAllToppings(toppings)

    @rest_api_restaurants.expect(add_topping_api_model)
    def post(self):
        """
        Given topping name this method creates a new topping
        """
        data = request.get_json()
        return AddObject(Topping_Model(public_id=str(uuid.uuid4()),name=data["name"]))


    @rest_api_restaurants.expect(topping_api_model)
    def put(self):
        """
         given topping name and its features , updates topping
        """
        data = request.get_json()
        current_topping = Topping_Model.query.filter_by(public_id=data['public_id']).first()
        return updateToppings(data, current_topping)

    @rest_api_restaurants.expect(delete_topping_api_model)
    def delete(self):
        """
        Given topping id, delete topping
        """
        data = request.get_json()
        topping = Topping_Model.query.filter_by(public_id=data['public_id']).first()
        return DeleteObject(topping)

@style_namespace.route('/')
class Style(Resource):
    @rest_api_restaurants.marshal_with(get_all_style_api_model)
    def get(self):
        """
        Returns all the styles
        """
        style = Style_Model.query.all()
        return getAllStyles(style)

    @rest_api_restaurants.expect(add_style_api_model)
    def post(self):
        """
        Given style name  creates a new style
        """
        data = request.get_json()
        return AddObject(Style_Model(public_id=str(uuid.uuid4()),name=data["name"]))


    @rest_api_restaurants.expect(style_api_model)
    def put(self):
        """
         Given style name and its features this method updates style
        """
        data = request.get_json()
        current_style = Style_Model.query.filter_by(public_id=data['public_id']).first()
        return updateStyles(data, current_style)

    @rest_api_restaurants.expect(delete_style_api_model)
    def delete(self):
        """
        Given style id this method delete style
        """
        data = request.get_json()
        style = Style_Model.query.filter_by(public_id=data['public_id']).first()
        return DeleteObject(style)


@dishes_collection_namespace.route((''))
class Dishes_Collection(Resource):

    @rest_api_restaurants.marshal_with(get_dishes_collection_api_model)
    def get(self):
        current_collection = Dishes_Collection_Model.query.all()

        if current_collection:
            return getAllCollections(current_collection)
        else:
            return error_status(0, "Collection not found")

    @rest_api_restaurants.expect(add_sub_collections_api_model)
    def post(self):
        """
        Given dishes collection name this method creates a new dish collection
        """
        data = request.get_json()

        return AddSubCollections(data)


    @rest_api_restaurants.expect(dishes_collection_api_model)
    def put(self):
        """
        Given dish collection and its features updates collection
        """
        data = request.get_json()
        dishes_collection = Dishes_Collection_Model.query.filter_by(public_id=data['public_id']).first()
        return updateDishesCollections(data, dishes_collection)

    @rest_api_restaurants.expect(delete_dishes_collection_api_model)
    def delete(self):
        """
        Given dishes  collection id  delete collection
        """
        data = request.get_json()
        dishes_collection = Dishes_Collection_Model.query.filter_by(public_id=data['public_id']).first()
        return DeleteObject(dishes_collection)

# @dishes_collection_namespace.route('/<string:id>')
# @dishes_collection_namespace.doc(params={'id' : 'Main Collection ID'})
# class Sub_Collections(Resource):
#     @rest_api_restaurants.expect(add_sub_collections_api_model)
#     def post(self, id):
#         """
#         Given dishes collection name this method creates a new dish collection
#         """
#         data = request.get_json()
#         current_collection = Dishes_Main_Collection_Model.query.filter_by(public_id=id).first()
#
#         return AddSubCollections(data,current_collection)
#
#
#     def get(self, id):
#         current_collection = Dishes_Main_Collection_Model.query.filter_by(public_id=id).first()
#
#         if current_collection:
#             return getAllCollections(current_collection.main_collection_children)
#         else:
#             return error_status(0, "Collection not found")

########################################################################33
@main_dishes_collection_namespace.route(('/'))
class Main_Dishes_Collection(Resource):
    @rest_api_restaurants.marshal_with(get_main_dishes_collection_api_model)
    def get(self):
        """
        Returns all the dishes collection
        """
        dishes_collection = Dishes_Main_Collection_Model.query.all()
        return getAllDishesCollections(dishes_collection)

    @rest_api_restaurants.expect(add_dishes_main_collection_api_model)
    def post(self):
        """
        Given dishes collection name this method creates a new dish collection
        """
        data = request.get_json()
        return AddObject(Dishes_Main_Collection_Model(public_id=str(uuid.uuid4()),name=data["name"]))


    @rest_api_restaurants.expect(dishes_main_collection_api_model)
    def put(self):
        """
        Given dish collection and its features updates collection
        """
        data = request.get_json()
        dishes_collection = Dishes_Main_Collection_Model.query.filter_by(public_id=data['public_id']).first()
        return updateDishesCollections(data, dishes_collection)

    @rest_api_restaurants.expect(delete_main_dishes_collection_api_model)
    def delete(self):
        """
        Given dishes  collection id  delete collection
        """
        data = request.get_json()
        dishes_collection = Dishes_Main_Collection_Model.query.filter_by(public_id=data['public_id']).first()
        return DeleteObject(dishes_collection)





