import uuid

from flask import jsonify
from sqlalchemy import func

from app import db

#################Cuisines#####################
from app.restaurant.food_categories.models import Dishes_Main_Collection_Model, Dishes_Collection_Model, Dish_Model
from app.restaurant.restaurant_food_categories.models import Restaurant_Dish_Model, Restaurant_Dish_Topping_Model, \
    Restaurant_Dish_Style_Model
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.utils import success_status, error_status


def getAllRestaurantCuisine(cuisines):
    output = []
    for c in cuisines:
        cuisine_data = {}
        cuisine_data['public_id'] = c.public_id
        cuisine_data['name'] = c.name
        output.append(cuisine_data)

    return  success_status("cuisines", output)


def updateCuisine(data, cuisine):

    for d in data["dishes"]:
        cuisine.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()



######################Dishes###################################

def getAllRestaurantDishes(rest):
        restaurant_dishes = Restaurant_Dish_Model.query.filter_by(restaurant=rest).all()

        output = []
        for r in restaurant_dishes:
            if not r:
                return error_status(0,"No Restaurant Dish Found")
            dish_data = {}
            dish_data['public_id'] = r.public_id
            dish_data['dish_id'] = r.dish.public_id
            dish_data['dish_name'] = r.dish.name

            dish_data['collection_id'] = r.collection.public_id
            dish_data['collection_name'] = r.collection.name
            dish_data['main_collection_id'] = r.main_collection.public_id
            dish_data['main_collection_name'] = r.main_collection.name

            dish_data['description']= r.description
            dish_data['price']=r.price

            toppings = Restaurant_Dish_Topping_Model.query.filter_by(restaurant_dish=r).all()
            styles = Restaurant_Dish_Style_Model.query.filter_by(restaurant_dish=r).all()


            dish_data['toppings']=[]
            for t in toppings:
                data={}
                data['public_id'] = t.public_id
                data['topping_id']= t.topping.public_id
                data['topping_name']=t.topping.name
                data['description']= t.description
                data['price']= t.price
                dish_data['toppings'].append(data)

            dish_data['styles'] = []
            for t in styles:
                data = {}
                data['public_id'] = t.public_id
                data['style_id'] = t.style.public_id
                data['style_name'] = t.style.name
                data['description'] = t.description
                data['price'] = t.price
                dish_data['styles'].append(data)

            output.append(dish_data)



        return success_status("restaurant_dishes", output)


def getRestaurantDish(id):
    r = Restaurant_Dish_Model.query.filter_by(public_id=id).first()



    dish_data = {}
    dish_data['dish_id'] = r.dish.public_id
    dish_data['public_id'] = r.public_id
    dish_data['dish_name'] = r.dish.name
    if (r.collection):
        dish_data['collection_id'] = r.collection.public_id
        dish_data['collection_name'] = r.collection.name
    dish_data['description'] = r.description
    dish_data['price'] = r.price

    toppings = Restaurant_Dish_Topping_Model.query.filter_by(restaurant_dish=r).all()
    styles = Restaurant_Dish_Style_Model.query.filter_by(restaurant_dish=r).all()

    dish_data['toppings'] = []
    for t in toppings:
        data = {}
        data['public_id'] = t.public_id
        data['topping_id'] = t.topping.public_id
        data['topping_name'] = t.topping.name
        data['description'] = t.description
        data['price'] = t.price
        dish_data['toppings'].append(data)

    dish_data['styles'] = []
    for t in styles:
        data = {}
        data['public_id'] = t.public_id
        data['style_id'] = t.style.public_id
        data['style_name'] = t.style.name
        data['description'] = t.description
        data['price'] = t.price
        dish_data['styles'].append(data)


    dish_data = { 'dish' : dish_data}

    return success_status("dish_details", dish_data)


def addRestaurantDish(id,data):
    current_restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
    if not current_restaurant:
        return error_status(0, "Restaurant not found")
    for d in data['restaurant_dishes']:
        current = Dish_Model.query.filter_by(public_id=d['dish_id']).first()
        if not current:
            return error_status(0, "Dish not found")
        col = Dishes_Collection_Model.query.filter_by(public_id=d['collection_id']).first()
        if not col:
            return error_status(0, "Sub Collection not found")
        mcol = Dishes_Main_Collection_Model.query.filter_by(public_id=d['main_collection_id']).first()
        if not mcol:
            return error_status(0, "Main Collection not found")

        obj = Restaurant_Dish_Model(restaurant=current_restaurant, dish=current, collection=col, main_collection=mcol,
                                    description=d['description'], price=d['price'], public_id=str(uuid.uuid4()))

        db.session.add(obj)
    db.session.commit()
    return success_status()
######################Restaurant Dishes Toppings###################################

def getAllRestaurantDishesToppings(restaurant_dish):

        toppings = Restaurant_Dish_Topping_Model.query.filter_by(restaurant_dish=restaurant_dish)
        output = []
        for r in toppings:
            dish_data = {}
            dish_data['dish_name'] = r.restaurant_dish.dish.name
            dish_data['public_id'] = r.public_id
            dish_data['topping_id'] = r.topping.public_id
            dish_data['topping_name'] = r.topping.name
            dish_data['description']= r.description
            dish_data['price']=r.price

            output.append(dish_data)

        return success_status("dishes_toppings", output)


######################Restaurant Dishes Toppings###################################

def getAllRestaurantDishesStyles(restaurant_dish):
        styles = Restaurant_Dish_Style_Model.query.filter_by(restaurant_dish=restaurant_dish)
        output = []
        for r in styles:
            dish_data = {}
            dish_data['public_id'] = r.public_id
            dish_data['dish_name'] = r.restaurant_dish.dish.name

            dish_data['style_id'] = r.style.public_id
            dish_data['style_name'] = r.style.name
            dish_data['description']= r.description
            dish_data['price']=r.price

            output.append(dish_data)

        return success_status("dishes_styles", output)



###############################Restaurant Details ######################################33333
def getAllRestaurantDishes_collection(restaurant_dish):


    output = []
    for r in restaurant_dish:
        dish_data = {}
        dish_data['dish_id'] = r.dish.public_id
        dish_data['image_url'] = r.dish.image_url
        dish_data['public_id'] = r.public_id
        dish_data['dish_name'] = r.dish.name
        if (r.collection):
            dish_data['collection_id'] = r.collection.public_id
            dish_data['collection_name'] = r.collection.name

        if (r.main_collection):
            dish_data['main_collection_id'] = r.main_collection.public_id
            dish_data['main_collection_name'] = r.main_collection.name
        dish_data['description'] = r.description
        dish_data['price'] = r.price

        toppings = Restaurant_Dish_Topping_Model.query.filter_by(restaurant_dish=r).all()
        styles = Restaurant_Dish_Style_Model.query.filter_by(restaurant_dish=r).all()

        dish_data['toppings'] = []
        for t in toppings:
            data = {}
            data['public_id'] = t.public_id
            data['topping_id'] = t.topping.public_id
            data['topping_name'] = t.topping.name
            data['description'] = t.description
            data['price'] = t.price
            dish_data['toppings'].append(data)

        dish_data['styles'] = []
        for t in styles:
            data = {}
            data['public_id'] = t.public_id
            data['style_id'] = t.style.public_id
            data['style_name'] = t.style.name
            data['description'] = t.description
            data['price'] = t.price
            dish_data['styles'].append(data)

        output.append(dish_data)

    return output

def getCollections(r):

    restaurant_main_collections=Restaurant_Dish_Model.query.filter_by(restaurant=r).distinct(Restaurant_Dish_Model.dish_main_collection_id).all()
    # main_collections = Restaurant_Dish_Model.query.with_entities(Restaurant_Dish_Model.main_collection,
    #                                                              func.count(Restaurant_Dish_Model.id)).\
    #                                                              filter_by(restaurant=r).\
    #                                                              group_by(Restaurant_Dish_Model.main_collection).all()
    output=[]
    for mcr in restaurant_main_collections:
        m= mcr.main_collection
        if not m:
            return error_status(0,"No Main Collection Found")
        data = {}
        data['name'] =  m.name
        data['public_id'] =  m.public_id
        data['sub_collections']=[]
        restaurant_collections=Restaurant_Dish_Model.query.filter_by(restaurant=r).filter_by(main_collection=m).distinct(Restaurant_Dish_Model.dish_collection_id).all()

        for c in restaurant_collections:
            cm=c.collection
            if not cm:
                return error_status(0, "No Sub Collection Found")
            collections_data = {}
            collections_data['name'] = cm.name
            collections_data['public_id'] = cm.public_id
            collections_data['dishes_details']=[]
            restaurant_dishes  = Restaurant_Dish_Model.query.filter_by(restaurant=r).filter_by(main_collection=m).filter_by(collection = cm).all()
            collections_data['dishes_details']=getAllRestaurantDishes_collection(restaurant_dishes)
            data['sub_collections'].append(collections_data)

        output.append(data)

    return output




def getPopularDishes(rest):
    restaurant_dishes = Restaurant_Dish_Model.query.filter_by(restaurant=rest).all()

    return getAllRestaurantDishes_collection(restaurant_dishes)




