from flask_restplus import fields
from app.restaurant import rest_api_restaurants
from app.restaurant.food_categories.serializers import cuisine_api_model, dish_api_model, dishes_collection_api_model

########################Cuisines##################################


cuisine_ids_api_model = rest_api_restaurants.model('cuisine_ids', {
    'public_id': fields.String(required=True)
})

add_cuisine_ids_api_model= rest_api_restaurants.model('add cuisine_ids', {
    'cuisines': fields.List(fields.Nested(cuisine_ids_api_model))
})


get_all_restaurant_cuisine_api_model = rest_api_restaurants.model('List of all restaurant cuisine', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'cuisines': fields.List(fields.Nested(cuisine_api_model))
})

delete_cuisine_api_model = rest_api_restaurants.model('delete cuisine', {
    'public_id': fields.String(required=True)
})


##############################Restaurant Dishes##########################################
restaurant_dish_api_topping_api_model = rest_api_restaurants.model('dishes_toppings', {
    'public_id': fields.String(),
    'topping_id': fields.String(),
    'topping_name': fields.String(default="Topping Name"),
    'description': fields.String(default="Description"),
    'price': fields.Integer(default=0)
})

restaurant_dish_api_style_api_model = rest_api_restaurants.model('dishes_toppings', {
    'public_id': fields.String(),
    'style_id': fields.String(),
    'style_name': fields.String(default="Style Name"),
    'description': fields.String(default="Description"),
    'price': fields.Integer(default=0)
})

restaurant_dish_api_model = rest_api_restaurants.model('restaurant_dish', {
    'public_id': fields.String(),
    'dish_id': fields.String(),
    'image_url' : fields.String(default="/images/restaurant_dish/dish.png"),
    'dish_name': fields.String(default="Dish Name"),
    'collection_id': fields.String(),
    'collection_name': fields.String(default="Collection Name"),
    'main_collection_id': fields.String(),
    'main_collection_name': fields.String(default="Main Collection Name"),
    'description': fields.String(default="Description"),
    'price': fields.Integer(default=0),
    'toppings': fields.List(fields.Nested(restaurant_dish_api_topping_api_model),default=[]),
    'styles': fields.List(fields.Nested(restaurant_dish_api_style_api_model),default=[])
})


dish_ids_api_model = rest_api_restaurants.model('restaurant_dishes', {
    'dish_id': fields.String(required=True),
    'collection_id': fields.String(required =False),
    'main_collection_id' :fields.String(required=False),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True),
})

add_dish_ids_api_model= rest_api_restaurants.model('add restaurant dish_ids', {
    'restaurant_dishes': fields.List(fields.Nested(dish_ids_api_model))
})


get_all_restaurant_dishes_api_model = rest_api_restaurants.model('List of all restaurant dishes', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'restaurant_dishes': fields.List(fields.Nested(restaurant_dish_api_model),default=[])
})

get_one_restaurant_dish_api_model = rest_api_restaurants.model('Get one dish details based on ID', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dish_details': fields.Nested(restaurant_dish_api_model)
})


##########################Restaurant Dishes Toppings###################################3

restaurant_dish_topping_api_model = rest_api_restaurants.model('dishes_toppings', {
    'public_id': fields.String(),
    'dish_name': fields.String(),
    'topping_id': fields.String(),
    'topping_name': fields.String(),
    'description': fields.String(),
    'price': fields.Integer()
})

dish_topping_ids_api_model = rest_api_restaurants.model('add dishes_toppings', {
    'topping_id': fields.String(required=True),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True)
})

add_dish_topping_ids_api_model= rest_api_restaurants.model('add dish topping', {
    'dishes_toppings': fields.List(fields.Nested(dish_topping_ids_api_model))
})


get_all_restaurant_dishes_topping_api_model = rest_api_restaurants.model('List of all restaurant dishes toppings', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dishes_toppings': fields.List(fields.Nested(restaurant_dish_topping_api_model))
})


restaurant_dish_api_topping_api_model = rest_api_restaurants.model('dishes_toppings', {
    'public_id': fields.String(),
    'topping_id': fields.String(),
    'topping_name': fields.String(),
    'description': fields.String(),
    'price': fields.Integer()
})


##########################Restaurant Dishes Style###################################3
restaurant_dish_style_api_model = rest_api_restaurants.model('dishes_style', {
    'public_id': fields.String(),
    'dish_name': fields.String(),
    'style_id': fields.String(),
    'style_name': fields.String(),
    'description': fields.String(),
    'price': fields.Integer()
})

dish_style_ids_api_model = rest_api_restaurants.model('dishes_style', {
    'style_id': fields.String(required=True),
    'description': fields.String(required=True),
    'price': fields.Integer(required=True)
})

add_dish_style_ids_api_model= rest_api_restaurants.model('add dish styles', {
    'dishes_styles': fields.List(fields.Nested(dish_style_ids_api_model))
})


get_all_restaurant_dishes_style_api_model = rest_api_restaurants.model('List of all restaurant dishes styles', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dishes_styles': fields.List(fields.Nested(restaurant_dish_style_api_model))
})

delete_api_model = rest_api_restaurants.model('delete', {
    'public_id': fields.String(required=True)
})




#############################Restaurant Details ##########################################
sub_collection_api_model = rest_api_restaurants.model('get dish sub collections', {
    'public_id': fields.String(),
    'name' : fields.String(),
    'dishes_details': fields.List(fields.Nested(restaurant_dish_api_model),default=[]),
})


main_collection_api_model= rest_api_restaurants.model('get dish main collections', {
    'public_id': fields.String(),
    'name' : fields.String(default="Main Dish Collection"),
    'sub_collections': fields.List(fields.Nested(sub_collection_api_model),default=[]),
})



get_addresses_api_model = rest_api_restaurants.model('get addresses', {
    'public_id': fields.String(),
    'street': fields.String(),
    'area': fields.String(),
    'city': fields.String(),
    'state': fields.String(),
    'zipcode': fields.String(),
    'country': fields.String(),
    'long': fields.Float(),
    'lat': fields.Float()
})
add_address_api_model = rest_api_restaurants.model('add address', {
    'street': fields.String(required=True),
    'area': fields.String(required=True),
    'city': fields.String(required=True),
    'state': fields.String(required=True),
    'zipcode': fields.String(required=True),
    'country': fields.String(required=True),
    'long': fields.Float(required=True),
    'lat': fields.Float(required=True)
})

####################################################MR Waqas #################################################3
review_api_model = rest_api_restaurants.model('get review', {
    'public_id': fields.String(),
    'image_url': fields.String(),
    'comment': fields.String(),
    'rating': fields.Float(),
    'time_stamp': fields.DateTime(),
    'user_public_id': fields.String(),
    'user_full_name': fields.String(),
    'user_image_url': fields.String()

})



location_api_model = rest_api_restaurants.model('Location', {
    "latitude": fields.Float,
    "longitude": fields.Float
})
restaurant_api_model = rest_api_restaurants.model('Restaurant', {
    'public_id' : fields.String(),
    'name': fields.String(required=True,default="Restaurant Name"),
    'rating': fields.Float(required=True,default=0),
    'opening_time': fields.String(required=True,default="00:00"),
    'closing_time': fields.String(required=True,default="00:00"),
    'phone_no': fields.String(required=True,default="1234567"),
    'image_url': fields.String(required=True,default="/images/restaurant_profile/restaurant.png"),
    'capacity': fields.Integer(required=True,default=0),
    'delivery_price': fields.Integer(required=True,default=0),
    'tax_percentage': fields.Integer(required=True,default=0),
    'table_booking_url': fields.String(required=True,default="www.google.com"),
    'price_group': fields.String(required=True,default="S"),
    'popularity': fields.Integer(required=True,default=0),
    'description': fields.String(required=True,default="Description"),
    'tags' : fields.List(fields.String,default=[]),
    'address': fields.String(default="Lahore"),
    'location': fields.Nested(location_api_model,default=[])
})

restaurants_extra_api_model= rest_api_restaurants.model('Exta Restaurant' , {
    'restaurant': fields.Nested(restaurant_api_model,default={}),
    'opening_time': fields.String(default="00:00"),
    'closing_time': fields.String(default="00:00"),
    'delivery_price': fields.Integer(default=0),
    'tax_percentage': fields.Integer(default=0),
    'is_bookmarked' : fields.Boolean(default=False),
    #'addresses': fields.List(fields.Nested(get_addresses_api_model)),
    'popular_dishes': fields.List(fields.Nested(restaurant_dish_api_model),default=[]),
    'dishes_collections': fields.List(fields.Nested(main_collection_api_model),default=[]),
    'reviews' : fields.List(fields.Nested(review_api_model),default=[]),
    'review_count' : fields.Integer(default=0)
})

get_one_restaurant_api_model = rest_api_restaurants.model('Get one restaurant details based on ID', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'restaurant_details': fields.Nested(restaurants_extra_api_model,default={})
})

########################## DISH CUSTOMER APP ################################
restaurants_extra_api_model= rest_api_restaurants.model('Exta Dish Restaurant Model' , {
      'dish' : fields.Nested(restaurant_dish_api_model,default={})
})
get_one_restaurant_dish_api_model = rest_api_restaurants.model('Get dish details based on Restaurant ID', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dish_details': fields.Nested(restaurants_extra_api_model)
})

