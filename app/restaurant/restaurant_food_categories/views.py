import uuid

from flask import request
from flask_restplus import Resource

from app import constants
from app.restaurant.events.controllers import getUserAddress
from app.restaurant.restaurant_food_categories.controllers import getAllRestaurantDishes, \
    getAllRestaurantDishesToppings, getAllRestaurantDishesStyles, getRestaurantDish, getPopularDishes, getCollections, \
    addRestaurantDish
from app.user.customer_profile.controllers import check_restaurant_bookmarked
from app.user.restaurant_profile.controllers import getRestaurant

from app.user.restaurant_profile.models import Restaurant_Profile_Model


from app.restaurant.restaurant_food_categories.models import *
from app.restaurant.restaurant_food_categories.serializers import *
from app.user.reviews.controllers import get_restaurant_all_reviews
from app.utils import success_status, DeleteObject, error_status

restaurant_dish_namespace = rest_api_restaurants.namespace('restaurant_dishes', description='Operations related to Restaurants dishes')
restaurant_dish_topping_namespace = rest_api_restaurants.namespace('restaurant_dish_toppings', description='Operations related to restaurants dish toppings')
restaurant_dish_style_namespace = rest_api_restaurants.namespace('restaurant_dish_styles', description='Operations related to restaurants dish styles')


@restaurant_dish_namespace.route('/<string:id>')
@restaurant_dish_namespace.doc(params={'id': 'Restaurant ID'})
class Restaurant_Dish(Resource):
    """
    Operations related to Restaurants dishes
    """
    @rest_api_restaurants.marshal_with(get_all_restaurant_dishes_api_model)
    def get(self,id):
        """
        Returns all the dishes of the restaurant
        """

        current_restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()

        return getAllRestaurantDishes(current_restaurant)

    @rest_api_restaurants.expect(add_dish_ids_api_model)
    def post(self, id):
        """
        Given Restaurant_ID and Dishes , add Dishes for that Restaurant
        """
        data = request.get_json()
        return addRestaurantDish(id,data)


@restaurant_dish_namespace.route('/dish/<string:id>')
@restaurant_dish_namespace.doc(params={'id': 'Restaurant Dish ID'})
class Restaurant_Dish_Updates(Resource):

    def delete(self, id):
        """
        Given Restaurant_Dish_ID deletes the restaurant dish
        """

        obj = Restaurant_Dish_Model.query.filter_by(public_id= id).first()


        return DeleteObject(obj)

    @rest_api_restaurants.marshal_with(get_one_restaurant_dish_api_model)
    def get(self, id):
        """
        Given Restaurant_Dish_ID fetches dish information
        """

        return getRestaurantDish(id)



@rest_api_restaurants.route('/<string:id>')
@rest_api_restaurants.doc(params={'id': 'Restaurant ID'})
class Restaurant_Details(Resource):

    @rest_api_restaurants.marshal_with(get_one_restaurant_api_model)
    def get(self, id):
        """
        return the Restaurants associated with the current user
        """
        r = Restaurant_Profile_Model.query.filter_by(public_id=id).first()

        if not r:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0], constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        o_time = str(r.opening_time)
        c_time = str(r.closing_time)
        reviews = get_restaurant_all_reviews(r)
        restaurant_data = { 'restaurant': getRestaurant(r),
                           'opening_time': o_time, 'closing_time': c_time,
                           'delivery_price': r.delivery_price, 'tax_percentage': r.tax_percentage,
                           'is_bookmarked' : check_restaurant_bookmarked(),
                            'popular_dishes' : getPopularDishes(r),
                           'dishes_collections' : getCollections(r),
                            'reviews' : reviews,
                            'review_count' : len(reviews)
                           }

        return success_status("restaurant_details", restaurant_data)


#############################################################################################

@restaurant_dish_topping_namespace.route('/<string:id>')
@restaurant_dish_topping_namespace.doc(params={'id': 'Restaurant Dish ID'})
class Restaurant_Dish_Topping(Resource):
    """
    Operations related to restaurants dish toppings
    """
    @rest_api_restaurants.marshal_with(get_all_restaurant_dishes_topping_api_model)
    def get(self,id):
        """
        Returns all the toppings of a restaurant dish
        """

        restaurant_dishes = Restaurant_Dish_Model.query.filter_by(public_id=id).first()

        return getAllRestaurantDishesToppings(restaurant_dishes)

    @rest_api_restaurants.expect(add_dish_topping_ids_api_model)
    def post(self, id):
        """
        Adds toppings of restaurant dish
        """
        data = request.get_json()
        current_dish = Restaurant_Dish_Model.query.filter_by(public_id=id).first()
        if not current_dish:
            return  error_status(0,"Dish not found")
        for d in data['dishes_toppings']:

            current_topping = Topping_Model.query.filter_by(public_id=d['topping_id']).first()

            obj = Restaurant_Dish_Topping_Model(public_id=str(uuid.uuid4()),restaurant_dish=current_dish,topping=current_topping, description=d['description'], price=d['price'])

            db.session.add(obj)
        db.session.commit()
        return success_status()

    @rest_api_restaurants.expect(delete_api_model)
    def delete(self, id):
        """
        Deletes topping of the restaurant dish
        """
        data = request.get_json()
        current_dish = Restaurant_Dish_Model.query.filter_by(public_id=id).first()
        if not current_dish:
            return error_status(0, "Dish not found")
        current_topping = Topping_Model.query.filter_by(public_id=data['public_id']).first()
        if not current_topping:
            return error_status(0, "Topping not found")
        obj=Restaurant_Dish_Topping_Model.query.filter_by(restaurant_dish=current_dish).filter_by(topping=current_topping)
        obj.delete()
        db.session.commit()
        return success_status()

@restaurant_dish_topping_namespace.route('/delete/<string:id>')
@restaurant_dish_topping_namespace.doc(params={'id': 'Restaurant Dish Topping ID'})
class Restaurant_Dish_Topping_Delete(Resource):


    def delete(self, id):
        """
        Deletes topping of the restaurant dish
        """
        obj=Restaurant_Dish_Topping_Model.query.filter_by(public_id=id).first()
        return DeleteObject(obj)

###################################################################################################

@restaurant_dish_style_namespace.route('/<string:id>')
@restaurant_dish_style_namespace.doc(params={'id': 'Restaurant Dish ID'})
class Restaurant_Dish_Style(Resource):
    """
        Operations related to restaurants dish toppings
    """
    @rest_api_restaurants.marshal_with(get_all_restaurant_dishes_style_api_model)
    def get(self,id):
        """
        Returns all the styles of a restaurant dish
        """

        restaurant_dishes = Restaurant_Dish_Model.query.filter_by(public_id=id).first()

        return getAllRestaurantDishesStyles(restaurant_dishes)

    @rest_api_restaurants.expect(add_dish_style_ids_api_model)
    def post(self, id):
        """
        Adds  styles of a restaurant dish
        """
        data = request.get_json()
        current_dish = Restaurant_Dish_Model.query.filter_by(public_id=id).first()
        if not current_dish:
            return  error_status(0,"Dish not found")

        for d in data['dishes_styles']:
            current_style = Style_Model.query.filter_by(public_id=d['style_id']).first()
            obj = Restaurant_Dish_Style_Model(public_id=str(uuid.uuid4()),restaurant_dish=current_dish,style=current_style, description=d['description'], price=d['price'])
            db.session.add(obj)

        db.session.commit()
        return success_status()


@restaurant_dish_style_namespace.route('/delete/<string:id>')
@restaurant_dish_style_namespace.doc(params={'id': 'Restaurant Dish Style ID'})
class Restaurant_Dish_Style_Delete(Resource):

    def delete(self, id):
        """
        Deletes style of the restaurant dish
        """
        obj=Restaurant_Dish_Style_Model.query.filter_by(public_id=id).first()
        return DeleteObject(obj)

