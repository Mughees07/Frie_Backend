from sqlalchemy.orm import relationship, backref

from app import db,user
from app.models import Created_TimeStamp, Updated_TimeStamp
from app.restaurant.food_categories.models import Dish_Model, Dishes_Collection_Model, Topping_Model, Style_Model, \
    Dishes_Main_Collection_Model


class Restaurant_Dish_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "restaurant_dish"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String())
    description = db.Column("description", db.String())
    price = db.Column("price", db.Integer)

    restaurant_id=db.Column("restaurant_id", db.Integer, db.ForeignKey("restaurant_profile.id"))
    dishes_id = db.Column("dish_id", db.Integer, db.ForeignKey("dish.id"))
    dish_collection_id = db.Column("dish_collection_id", db.Integer, db.ForeignKey("dish_collection.id") , nullable=True)
    dish_main_collection_id = db.Column("dish_main_collection_id", db.Integer, db.ForeignKey("dish_main_collection.id"), nullable=True)

    restaurant = relationship(user.restaurant_profile.models.Restaurant_Profile_Model,backref=backref("restaurant_children", cascade="all,delete"))
    dish = relationship(Dish_Model,backref=backref("dish_children", cascade="all,delete"))
    collection= relationship(Dishes_Collection_Model)
    main_collection = relationship(Dishes_Main_Collection_Model)



class Restaurant_Dish_Topping_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "restaurant_dish_topping"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String())
    description = db.Column("description", db.String())
    price = db.Column("price", db.Integer)


    restaurant_dishes_id = db.Column("restaurant_dish_id", db.Integer, db.ForeignKey("restaurant_dish.id"))
    restaurant_dish_topping_id = db.Column("restaurant_dish_topping_id", db.Integer, db.ForeignKey("topping.id"))


    restaurant_dish = relationship(Restaurant_Dish_Model ,backref=backref("restaurant_dish_children_1", cascade="all,delete"))
    topping= relationship(Topping_Model,backref=backref("topping_children", cascade="all,delete"))


class Restaurant_Dish_Style_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "restaurant_dish_style"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String())


    description = db.Column("description", db.String())
    price = db.Column("price", db.Integer)

    restaurant_dishes_id = db.Column("restaurant_dish_id", db.Integer, db.ForeignKey("restaurant_dish.id"))
    restaurant_dish_style_id = db.Column("restaurant_dish_style_id", db.Integer, db.ForeignKey("style.id"))

    restaurant_dish = relationship(Restaurant_Dish_Model,backref=backref("restaurant_dish_children", cascade="all,delete"))
    style= relationship(Style_Model,backref=backref("style_children", cascade="all,delete"))











