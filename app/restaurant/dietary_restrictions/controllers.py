from app.utils import success_status


def getAllCollections(collection):
    output = []
    for c in collection:
        collection_data = {}
        collection_data['public_id'] = c.public_id
        collection_data['name'] = c.name
        collection_data['image_url'] = c.image_url
        output.append(collection_data)

    return success_status("dietary_restrictions", output)


def getAllRestaurantCollection(restaurant_colllection):
    output = []
    for c in restaurant_colllection:
        collection_data = {}
        collection_data['public_id'] = c.public_id
        collection_data['dietary_restriction_id'] = c.dietary_restriction.public_id
        collection_data['dietary_restriction_name'] = c.dietary_restriction.name
        collection_data['restaurant_id'] = c.restaurant.public_id
        collection_data['restaurant_name'] = c.restaurant.name
        output.append(collection_data)

    return success_status("restaurant_dietary_restrictions", output)


def getAllRestrictions(collection):
    output = []
    for c in collection:
        collection_data = {}
        collection_data['public_id'] = c.public_id
        collection_data['name'] = c.name
        collection_data['image_url'] = c.image_url
        output.append(collection_data)

    return output