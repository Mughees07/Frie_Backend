from flask_restplus import fields
from app.restaurant import rest_api_restaurants

#############################Dietery Restrictions #############################


dietary_restrictions_api_model = rest_api_restaurants.model('dietary restrictions', {
    'public_id': fields.String(),
    'name': fields.String(),
    'image_url' : fields.String(),
})

add_dietary_restrictions_api_model = rest_api_restaurants.model('add dietary restriction', {
    'name': fields.String(required=True)
})

get_dietary_restrictions_api_model = rest_api_restaurants.model('List of all dietary restrictions', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'dietary_restrictions': fields.List(fields.Nested(dietary_restrictions_api_model))
})


###############################Restaurant Dietary Restrictions ###################################

restaurant_dietary_restrictions_api_model = rest_api_restaurants.model('restaurant dietary restrictions', {
    'public_id' : fields.String(),
    'dietary_restriction_id': fields.String(),
    'dietary_restriction_name': fields.String(),
    'restaurant_id': fields.String(),
    'restaurant_name': fields.String()
})

add_restaurant_dietary_restrictions_api_model = rest_api_restaurants.model('add restaurant dietary restrictions', {
    'restaurant_id': fields.String(required=True),
    'dietary_restriction_id': fields.String(required=True)
})

get_restaurant_dietary_restrictions_api_model = rest_api_restaurants.model('List of all restaurant dietary restrictions', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'restaurant_dietary_restrictions': fields.List(fields.Nested(restaurant_dietary_restrictions_api_model))
})


######################################################################################################

