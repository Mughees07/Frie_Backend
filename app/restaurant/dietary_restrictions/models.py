from sqlalchemy.orm import relationship, backref

from app import db
from app.user.restaurant_profile.models import Restaurant_Profile_Model


class Dietary_Restriction_Model(db.Model):
    __tablename__ = "dietary_restriction"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String,unique=True)
    name = db.Column(db.String(100), unique=True)
    image_url = db.Column(db.String(100), unique=True)


class Restaurants_Dietary_Restriction_Model(db.Model):
    __tablename__ = "restaurants_dietary_restriction"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String, unique=True)
    dietary_restriction_id = db.Column("collection_id", db.Integer, db.ForeignKey("dietary_restriction.id"))
    restaurant_id=db.Column("restaurant_id", db.Integer, db.ForeignKey("restaurant_profile.id"))

    restaurant = relationship(Restaurant_Profile_Model,backref=backref("dietry_restaurant_children", cascade="all,delete"))
    dietary_restriction = relationship(Dietary_Restriction_Model,backref=backref("dietary_restriction_children", cascade="all,delete"))





