import uuid

from flask import request
from flask_restplus import Resource

from app.restaurant.dietary_restrictions.controllers import getAllRestaurantCollection, getAllCollections
from app.restaurant.dietary_restrictions.models import Dietary_Restriction_Model, Restaurants_Dietary_Restriction_Model
from app.restaurant.dietary_restrictions.serializers import get_dietary_restrictions_api_model, \
    add_dietary_restrictions_api_model, get_restaurant_dietary_restrictions_api_model, \
    add_restaurant_dietary_restrictions_api_model
from app.restaurant.food_categories.serializers import *
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.utils import AddObject, DeleteObject, error_status
from app.restaurant.food_categories.controllers import *

dietary_restriction_namespace = rest_api_restaurants.namespace('dietary_restriction', description='Operations related to collection')
restaurant_dietary_restriction_namespace = rest_api_restaurants.namespace('restaurant_dietary_restriction', description='Operations related to restaurant dietary_restrictions')





@dietary_restriction_namespace.route('/')
class Dietary_Restriction(Resource):
    @rest_api_restaurants.marshal_with(get_dietary_restrictions_api_model)
    def get(self):
        """
        Returns all the cuisines
        """
        collection = Dietary_Restriction_Model.query.all()
        return getAllCollections(collection)

    @rest_api_restaurants.expect(add_dietary_restrictions_api_model)
    def post(self):
        """
        Given Cuisine name this method creates a new cuisine
        """
        data = request.get_json()
        return AddObject(Dietary_Restriction_Model(public_id=str(uuid.uuid4()),name=data["name"]))

@dietary_restriction_namespace.route('/<string:id>')
@dietary_restriction_namespace.doc(params={'id' : 'dietary restriction ID'})
class Dietary_Restriction_Update(Resource):
    @rest_api_restaurants.expect(add_dietary_restrictions_api_model)
    def put(self,id):
        """
         Given Cuisine object  updates given cuisine
        """
        data = request.get_json()
        current = Dietary_Restriction_Model.query.filter_by(public_id=id).first()
        current.name= data['name']
        db.session.commit()

        return success_status()

    def delete(self,id):
        """
        Given Cuisine id this delete cuisine
        """
        current = Dietary_Restriction_Model.query.filter_by(public_id=id).first()
        return DeleteObject(current)


######################################### Restaurant Collections ################################################3

@restaurant_dietary_restriction_namespace.route('/')
class Restaurant_Dietary_Restriction(Resource):
    @rest_api_restaurants.marshal_with(get_restaurant_dietary_restrictions_api_model)
    def get(self):
        """
        Returns all the cuisines
        """
        collection = Restaurants_Dietary_Restriction_Model.query.all()
        return getAllRestaurantCollection(collection)

    @rest_api_restaurants.expect(add_restaurant_dietary_restrictions_api_model)
    def post(self):
        """
        Given Cuisine name this method creates a new cuisine
        """
        data = request.get_json()
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=data['restaurant_id']).first()
        restriction = Dietary_Restriction_Model.query.filter_by(public_id=data['dietary_restriction_id']).first()
        return AddObject(Restaurants_Dietary_Restriction_Model(public_id=str(uuid.uuid4()),restaurant=restaurant, dietary_restriction=restriction))

@restaurant_dietary_restriction_namespace.route('/<string:id>')
@restaurant_dietary_restriction_namespace.doc(params={'id' : 'Restaurant dietary restriction ID'})
class Restaurant_Dietary_Restriction_Update(Resource):


    @rest_api_restaurants.expect(delete_cuisine_api_model)
    def delete(self,id):
        """
        Given Cuisine id this delete cuisine
        """
        current = Restaurants_Dietary_Restriction_Model.query.filter_by(public_id=id).first()
        return DeleteObject(current)




