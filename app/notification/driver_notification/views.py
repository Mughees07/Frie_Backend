import uuid

from flask import request
from flask_restplus import Resource

from app import constants
from app.notification import rest_api_notification
from app.notification.driver_notification.serializers import get_notification_api_model
from app.user.driver_profile.serializers import *

from app.user.users_auth.decorators import token_required_user
from app.utils import success_status
from pyfcm import FCMNotification


driver_notifications_namespace = rest_api_notification.namespace('driver_notifications', description='Operations related to Driver Notification')


@driver_notifications_namespace.route('/')
class Driver_Notification(Resource):
    @driver_notifications_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_notification.expect(get_notification_api_model)
    def post(self,current_user):
        """
        return the driver associated with the current user
        """
        data = request.get_json()

        push_service = FCMNotification(api_key=constants.FCM_API_KEY)#, proxy_dict=proxy_dict)

        # Your api-key can be gotten from:  https://console.firebase.google.com/project/<project-name>/settings/cloudmessaging

        registration_id = current_user.fcm_token
        message_title = data["message_title"]
        message_body = data["message_body"]
        result = push_service.notify_single_device(registration_id=registration_id, message_title=message_title,
                                                   message_body=message_body)



        # # Send to multiple devices by passing a list of ids.
        # registration_ids = ["<device registration_id 1>", "<device registration_id 2>", ...]
        # message_title = "Uber update"
        # message_body = "Hope you're having fun this weekend, don't forget to check today's news"
        # result = push_service.notify_multiple_devices(registration_ids=registration_ids, message_title=message_title,
        #                                               message_body=message_body)

        return success_status()
