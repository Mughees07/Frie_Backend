import uuid

from flask import jsonify, request

from app import db
from app.user.driver_profile.models import Driver_Profile_Model, Vehicle_Type_Model, Areas_Model

from app.utils import success_status


def get_driver_profile(driver, user):
    output = []
    driver_data = {}
    driver_data['public_id'] = driver.public_id
    driver_data['first_name'] = driver.first_name
    driver_data['last_name'] = driver.last_name
    driver_data['email'] = user.email
    driver_data['ref_code'] = driver.ref_code
    driver_data['image_url'] = driver.image_url
    driver_data['image_verified'] = driver.image_verified
    driver_data['background_check'] = driver.background_check
    driver_data['delivery_areas'] = get_delivery_areas(driver.delivery_areas_child)
    driver_data['vehicles'] = get_vehicles(driver.vehicle_types_child)
    output.append(driver_data)

    return success_status("drivers", output)


def get_delivery_areas(areas):
    output = []
    for area in areas:
        area_data = {}
        area_data['public_id'] = area.public_id
        area_data['name'] = area.name
        output.append(area_data)

    return output


def get_vehicles(vehicles):
    output = []
    for vehicle in vehicles:

        vehicles_data = {}
        vehicles_data['public_id'] = vehicle.public_id
        vehicles_data['name'] = vehicle.name
        output.append(vehicles_data)

    return output


def add_driver(user):

    data = request.get_json()
    new_driver = Driver_Profile_Model(public_id=str(uuid.uuid4()), first_name=data['first_name'],
                                      last_name=data['last_name'], ref_code='Random Number',
                                      image_url=data['image_url'], user_auth_id=user.id)

    for vehicle in data['vehicles']:
        new_vehicle = Vehicle_Type_Model.query.filter_by(public_id=vehicle['public_id']).first()
        new_driver.vehicle_types_child.append(new_vehicle)

    for area in data['delivery_areas']:
        new_area = Areas_Model.query.filter_by(public_id=area['public_id']).first()
        new_driver.delivery_areas_child.append(new_area)

    db.session.add(new_driver)
    db.session.commit()

    return success_status()

def update_driver(driver):

    driver.vehicle_types_child = []
    driver.delivery_areas_child = []

    data = request.get_json()
    driver.first_name = data['first_name']
    driver.last_name = data['last_name']
    driver.image_url = data['image_url']

    for vehicle in data['vehicles']:
        new_vehicle = Vehicle_Type_Model.query.filter_by(public_id=vehicle['public_id']).first()
        driver.vehicle_types_child.append(new_vehicle)

    for area in data['delivery_areas']:
        new_area = Areas_Model.query.filter_by(public_id=area['public_id']).first()
        driver.delivery_areas_child.append(new_area)

    db.session.commit()
    return success_status()