from flask import Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'

    }
}

notification_blueprint = Blueprint('notification', __name__, url_prefix="/api/v1/notification")

rest_api_notification = Api(notification_blueprint,
                    title='Notifications',
                    version='1.0',
                    description='Endpoints related to Notifications',
                    authorizations=authorizations,
                    validate=True
                    # All API metadatas
                    )

from app.notification.driver_notification.views import driver_notifications_namespace