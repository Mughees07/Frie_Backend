import config

if config.PRODUCTION:
    UPLOAD_FOLDER = "/var/www/apk/images"
else:
    UPLOAD_FOLDER = 'images'

ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])
############################################3EMAIL ###############33
EMAIL_LOGIN = "wp@datics.ai"
EMAIL_PASSWORD = "datics123"

####################################################################3
SUCCESS = "success"
NEARBY_RADIUS = 16093.4
########################STRIPE KEYS #############################

STRIPE_PUB_KEY = "pk_test_ruV5HnCRK60euQ3rXa4hvCtK00CntLDLX5"
STRIPE_SECRET_KEY = "sk_test_DguhbS1MuR0G4U0EmSRZk8rN00GsFXmp6P"
STRIPE_CLIENT_ID = "ca_FfvT9m1Ya6bwkRl7SYVxRq5ui3N5e3wL"

authorize_url = "https://connect.stripe.com/express/oauth/authorize?redirect_uri=https://stripe.com/connect/default/oauth/test&client_id={}&state={}".format(STRIPE_CLIENT_ID,"1234")

##############################GOOGLE KEYS #######################333
GOOGLE_DIRECTIONS_API_KEY = "AIzaSyCn5FfBwwLojkVhk7Rm96Cq1FlqNUt2XQI"

##########################NOTIFICATION ##########################
FCM_API_KEY = "AAAAdSXH9eQ:APA91bH9NyW5X35asyXSuOUylWYHlLayJ35almX42SpxapRYqz77GvrjTzzBtfjrLf-nELtnMHb-m1RP_9VhobaIPoXTCCsjE_xZwdX3naQNihYHozb6Ls1GzDjiSsZl3qjid6m0BpjF"


##################################ML END POINTS ###########################333

ML_HOME_LIST_URL = "http://104.248.207.194:8887/ml/home/" # user_id required

ML_DELIVERY_LIST_URL = "http://104.248.207.194:8887/ml/delivery/"

ML_RECOMMENDATION_LIST_URL = "http://104.248.207.194:8887/ml/recommendations/"  # user id required

ML_TRENDING_LIST_URL = "http://104.248.207.194:8887/ml/trending/"

ML_RECENTLY_VISITED_LIST_URL = "http://104.248.207.194:8887/ml/recently_visited/"  # user_id required

ML_POPULAR_TODAY_LIST_URL = "http://104.248.207.194:8887/ml/popular_today/"

ML_OUR_FAVOURITES_LIST_URL = "http://104.248.207.194:8887/ml/our_favorites/"

################################### ERROR CODES ###############################

class ERROR:
    FILE_UPLOAD_ERROR = [1000, 'error in uploading file !']
    USER_NOT_EXISTS = [1001, 'user does not exists !']
    CUSTOMER_NOT_EXISTS = [1002, 'user does not exists !']
    DRIVER_NOT_EXISTS = [1003, 'user does not exists !']
    RESTAURANT_NOT_EXISTS = [1004, 'restaurant does not exists !']
    DISH_NOT_EXISTS = [1005, 'dish does not exists !']
    CUISINE_NOT_EXISTS = [1006, 'cuisine does not exists !']
    ORDER_NOT_EXISTS = [1007, 'order does not exists !']
    EVENT_NOT_EXISTS = [1008, 'event does not exists !']
    REVIEW_NOT_EXISTS = [1009, 'review does not exists !']
    ADDRESS_NOT_EXISTS = [1010, 'address does not exists !']
    DB_ERROR = [1050]

    #########################DRIVER #####################
    DRIVER_AREAS_NOT_DEFINED = [1051, 'driver does not have any areas selected!']
    DRIVER_VEHICLES_NOT_DEFINED = [1052, 'driver does not have any vehicles selected !']
    AREA_NOT_EXIST = [1053, 'delivery area(s) does not exist!']
    VEHICLE_NOT_EXIST= [1054, 'vehicle(s) does not exist !']

IMAGE_FOLDERS = { 0 : 'driver_profile' ,
                  1 : 'restaurant_profile',
                  2 : 'customer_profile',
                  3 : 'restaurant',
                  4 : 'restaurant_dish',
                  5 : 'homescreen_banners'}


RESTAURANT_PRICE_GROUPS = {
                None : '$',
                  0 : '$' ,
                  1 : '$$',
                  2 : '$$$'}


USER_TYPES = {
    0 : 'Customer',
    1 :  'Driver',
    2 :  'Restaurant'

}

IMAGES_TYPE = {
    0 : 'Restaurant',
    1 :  'Dish',
    2 :  'Event'
}




