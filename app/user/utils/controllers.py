from flask import jsonify

from app import db, constants

#################Cuisines#####################
from app.utils import success_status


def get_all_users(users):
    output = []
    for user in users:
        user_data = {}
        user_data['public_id'] = user.public_id
        user_data['email'] = user.email
        user_data['password'] = user.password
        user_data['type'] = user.type
        user_data['verified'] = user.verified
        user_data['fcm_token'] = user.fcm_token
        output.append(user_data)

    return success_status("users", output)


def updateCuisine(data, cuisine):
    cuisine.name = data['name']
    try:
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})
    return success_status()


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in constants.ALLOWED_EXTENSIONS