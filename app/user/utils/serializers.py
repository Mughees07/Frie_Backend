import werkzeug
from flask_restplus import reqparse, fields

from app.user import rest_api_user

file_upload = reqparse.RequestParser()
file_upload.add_argument('image',
                         type=werkzeug.datastructures.FileStorage,
                         location='files',
                         required=True,
                         help='Image')
file_upload.add_argument('folder_id',
                         type=int,
                         required=True,
                         help='')


add_images_api_model = rest_api_user.model('add  type ( 0- Restaurant ,1- Dish ) with their id and url ' , {

    'type': fields.Integer(required=True),
    'type_id' : fields.String(required=True),
    'image_url' : fields.String(required=True),
})

images_api_model = rest_api_user.model('Images ' , {

    'image_url' : fields.String(required=True),
})

get_all_images_api_model = rest_api_user.model('All Images ' , {

    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'images': fields.List(fields.Nested(images_api_model))
})