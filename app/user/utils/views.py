import os

from flask import request
from werkzeug.utils import secure_filename

import config
from app import app,constants

from app.user import rest_api_user
from app.user.utils.controllers import allowed_file
from app.user.utils.models import Images_Model
from app.user.utils.serializers import file_upload, add_images_api_model, images_api_model, get_all_images_api_model
from app.utils import success_status, error_status, AddObject
from flask_restplus import Resource



utils_upload_namespace = rest_api_user.namespace('utils', description='utils')



@utils_upload_namespace.route('/image_upload')
class File_Upload(Resource):
    """
    File Upload
    """
    @rest_api_user.expect(file_upload)
    def post(self):
        """
        Upload image to the given folder name
        """
        args = file_upload.parse_args()
        index = int( args['folder_id'])
        destination = os.path.join(constants.UPLOAD_FOLDER, constants.IMAGE_FOLDERS[index])

        if not os.path.exists(destination):
            os.makedirs(destination)

        filename= args['image'].filename

        if filename == '':
            return error_status(constants.ERROR.ORDER_NOT_EXISTS[0],'No selected file !')


        elif allowed_file(filename):
            filename = secure_filename(filename)
            image_file = os.path.join(destination, filename)
            args['image'].save(image_file)
            if (config.PRODUCTION):
                return success_status("image_url",image_file[12:])
            else :
                return success_status("image_url", image_file)


        else:
            return error_status(constants.ERROR.ORDER_NOT_EXISTS[0],'path not allowed')

##################################Images###############################################
@utils_upload_namespace.route('/add_images')
class Images_Upload(Resource):

    @rest_api_user.expect(add_images_api_model)
    def post(self ):
        """
        Add multiple images to according to type (0- Restaurant , 1- Dish , 2- Events ) and there id type_id
        """
        data = request.get_json()

        obj = Images_Model(type_id=data['type_id'], image_url=data['image_url'],type=data['type'])

        return AddObject(obj)

################################## Restaurant Images ########################################

@utils_upload_namespace.route('/restaurant_images/<string:id>')
@utils_upload_namespace.doc(params={"id": " Restaurants ID "})
class Restaurant_Images(Resource):

    @rest_api_user.marshal_with(get_all_images_api_model)
    def get(self, id):


        images = Images_Model.query.filter_by(type = 0).filter_by(type_id = id).all()
        output= []

        for i in images:
            data={}
            data['image_url'] = i.image_url
            output.append(data)


        return success_status('images',output)

##################################Dish Images ##################################################

@utils_upload_namespace.route('/dish_images/<string:id>')
@utils_upload_namespace.doc(params={"id": " Dish ID "})
class Dish_Images(Resource):
    @rest_api_user.marshal_with(get_all_images_api_model)
    def get(self, id):
        images = Images_Model.query.filter_by(type=1).filter_by(type_id=id).all()
        output = []

        for i in images:
            data = {}
            data['image_url'] = i.image_url
            output.append(data)

        return success_status('images', output)


################################ EVENT IMAGES #############################

@utils_upload_namespace.route('/event_images/<string:id>')
@utils_upload_namespace.doc(params={"id": " Event ID "})
class Event_Images(Resource):
    @rest_api_user.marshal_with(get_all_images_api_model)
    def get(self, id):
        images = Images_Model.query.filter_by(type=2).filter_by(type_id=id).all()
        output = []

        for i in images:
            data = {}
            data['image_url'] = i.image_url
            output.append(data)

        return success_status('images', output)












