from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp

class Images_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "images"
    id = db.Column(db.Integer, primary_key=True)
    type = db.Column(db.Integer)
    type_id = db.Column(db.String(100))
    image_url = db.Column(db.String())