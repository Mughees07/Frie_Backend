from flask import Blueprint
from flask_restplus import Api

authorizations = {
    'apikey' : {
        'type' : 'apiKey',
        'in' : 'header',
        'name' : 'Authorization'
    }
}

user_blueprint = Blueprint('user', __name__, url_prefix="/api/v1/user")

rest_api_user = Api(user_blueprint,
                    title='Users',
                    version='1.0',
                    description='Endpoints related to User Authentication, Customer Profile, Driver Profile, Restaurant'
                                ' Profile, Addresses and Delivery Areas',
                    authorizations=authorizations,
                    validate=True
                    # All API metadatas
                    )

from app.user.users_auth.views import user_auth_namespace
from app.user.customer_profile.views import customer_profile_namespace
from app.user.driver_profile.views import driver_profile_namespace, delivery_areas_namespace, vehicle_types_namespace
from app.user.addresses.views import addresses_namespace
from app.user.restaurant_profile.views import restaurant_profile_namespace
from app.user.restaurant_profile.views import filters_namespace
from app.user.utils.views import utils_upload_namespace
from app.user.reviews.views import reviews_namespace
from app.user.customer_profile.views import customer_followings_namespace
from app.user.customer_profile.views import rewards_namespace