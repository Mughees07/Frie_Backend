from app import db
from sqlalchemy.orm import backref, relationship
from app.models import Created_TimeStamp, Updated_TimeStamp

class Restaurant_Profile_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "restaurant_profile"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(),unique=True)
    description = db.Column(db.String)
    merchant_payment_id = db.Column(db.String(),nullable=True)
    name = db.Column(db.String(100))
    rating = db.Column(db.Float)
    opening_time = db.Column(db.TIME())
    closing_time = db.Column(db.TIME())
    phone_no = db.Column(db.String(20))
    image_url = db.Column(db.String(200))
    capacity = db.Column(db.Integer)
    delivery_price = db.Column(db.Integer)
    tax_percentage = db.Column(db.Float)
    table_booking_url = db.Column(db.String(),nullable=True)
    price_group = db.Column(db.Integer)
    popularity  =  db.Column(db.Integer)


    user_auth_id = db.Column(db.Integer, db.ForeignKey("user_auth.id"), unique=True)
    #restaurant_dishes = db.relationship('Restaurant_Dish_Model',secondary = "restaurant_dish",backref=db.backref('dish_restaurants'),lazy = 'dynamic' , cascade="all,delete")
    #restaurant_dish = db.relationship('Dish_Collection_Model', secondary="restaurant_dish",backref=db.backref('dish_restaurants'), lazy='dynamic')
    restaurant_events = db.relationship('Event_Model', backref='events')
    addresses_child = db.relationship('Addresses_Model', backref='restaurant_addresses')
    reviews = db.relationship('Reviews_Model', backref='restaurant')


class Filters_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "filters"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(),unique=True)
    name = db.Column(db.String(100))


class Restaurant_Filters_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "restaurant_filters"
    id = db.Column(db.Integer, primary_key=True)
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant_profile.id"))
    filter_id = db.Column(db.Integer, db.ForeignKey("filters.id"))
    restaurant = relationship(Restaurant_Profile_Model, backref=backref("filters", cascade="all,delete"))
    filter = relationship(Filters_Model, backref=backref("restaurant_filter", cascade="all,delete"))