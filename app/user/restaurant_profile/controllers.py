from flask import request
from app import db, constants
from app.user.addresses.controllers import getAddressToString, getAddressLocation

from app.user.reviews.controllers import getRestaurantReviewCount
from app.utils import success_status, commit_changes

def getMainCollection(r):
    from app.restaurant.restaurant_food_categories.models import Restaurant_Dish_Model
    restaurant_main_collections=Restaurant_Dish_Model.query.filter_by(restaurant=r).distinct(Restaurant_Dish_Model.dish_main_collection_id).all()
    # main_collections = Restaurant_Dish_Model.query.with_entities(Restaurant_Dish_Model.main_collection,
    #                                                              func.count(Restaurant_Dish_Model.id)).\
    #                                                              filter_by(restaurant=r).\
    #                                                              group_by(Restaurant_Dish_Model.main_collection).all()
    output=[]
    for mcr in restaurant_main_collections:
        m= mcr.main_collection
        if m:
            output.append( m.name)


    return output



def getRestaurant(r):
    o_time = str(r.opening_time)
    c_time = str(r.closing_time)
    price_group = constants.RESTAURANT_PRICE_GROUPS[r.price_group]

    restaurant_data = {'public_id': r.public_id, 'name': r.name,
                       'rating': r.rating, 'opening_time': o_time, 'closing_time': c_time,
                       'phone_no': r.phone_no, 'image_url': r.image_url, 'capacity': r.capacity ,
                       'delivery_price': r.delivery_price,
                       'tax_percentage': r.tax_percentage,
                       'table_booking_url': r.table_booking_url,
                       'price_group' : price_group,'description': r.description,
                       'tags' : getMainCollection(r) ,'review_count' : getRestaurantReviewCount(r),
                       'location' : getAddressLocation(r.addresses_child),
                       'address' :  getAddressToString(r.addresses_child)

                       }

    return restaurant_data


def getAllRestaurants(restaurants):
    output=[]

    for r in restaurants:
        output.append(getRestaurant(r))

    return success_status("restaurants",output)



def update_restaurant(restaurant):
    data = request.get_json()

    restaurant.name = data['name']
    restaurant.rating = data['rating']
    restaurant.opening_time = data['opening_time']
    restaurant.closing_time = data['closing_time']
    restaurant.phone_no = data['phone_no']
    restaurant.image_url = data['image_url']
    restaurant.capacity = data['capacity']
    restaurant.delivery_price = data['delivery_price']
    restaurant.tax_percentage = data['tax_percentage']
    restaurant.table_booking_url = data['table_booking_url']
    restaurant.price_group = data['price_group']
    restaurant.description = data['description']

    return commit_changes()