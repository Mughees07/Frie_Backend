import uuid
from flask import request
from flask_restplus import Resource

from app import constants
from app.user.addresses.models import Addresses_Model
from app.user.restaurant_profile.controllers import update_restaurant, getRestaurant
from app.user.addresses.controllers import getUserAddress
from app.user.restaurant_profile.models import *
from app.user.restaurant_profile.serializers import *
from app.user.reviews.controllers import get_restaurant_all_reviews
from app.user.users_auth.decorators import token_required_user, token_required
from app.utils import success_status, AddObject, DeleteObject, error_status, commit_changes

restaurant_profile_namespace = rest_api_user.namespace('restaurant_profile', description='Operations related to Restaurant Profile')
filters_namespace = rest_api_user.namespace('filters', description='Operations related to Restaurant Filters')


@restaurant_profile_namespace.route('/<string:id>')
class Restaurant_Profile(Resource):
    @rest_api_user.expect(add_restaurant_api_model)
    @restaurant_profile_namespace.doc(security='apikey',params={'id': 'Restaurant ID'})
    @token_required
    def put(self,id):
        """
        Given Name, Address, Long, Lat, Rating, Opening_Time, Closing_Time, Phone_no, Image_url and Capacity,
        this method updates restaurant profile, authentication token is required
        """
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
        if not restaurant:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0], constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        return update_restaurant(restaurant)

    @restaurant_profile_namespace.doc(security='apikey',params={'id': 'Restaurant ID'})
    @token_required
    def delete(self,id):
        """
        Given public_id, this method deletes restaurant profile,
        authentication token is required
        """
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
        if not restaurant:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0], constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        return DeleteObject(restaurant)

    @restaurant_profile_namespace.doc(security='apikey')
    @rest_api_user.marshal_with(get_one_restaurants_profile_api_model)
    def get(self, id):
        """
        return the Restaurants associated with the current user
        """
        r = Restaurant_Profile_Model.query.filter_by(public_id=id).first()

        if not r:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0], constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        restaurant_data = getRestaurant(r)

        ###### dishes_popular

        return success_status("restaurant", restaurant_data)


@restaurant_profile_namespace.route('')
class User_Restaurant_Profile(Resource):

    @restaurant_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.marshal_with(get_one_restaurants_profile_api_model)
    def get(self,current_user):
        """
        return the Restaurants associated with the current user
        """
        r = current_user.restaurant

        if not r:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0], constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        restaurant_data = getRestaurant(r)

        return success_status("restaurant", restaurant_data)

    @restaurant_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_restaurant_api_model)
    def post(self,current_user):
        """
        Given Name, Address, Long, Lat, Rating, Opening_Time, Closing_Time, Phone_no, Image_url and Capacity,
        this method creates a new Restaurant
        """
        data = request.get_json()
        o_time = datetime.datetime.strptime(data['opening_time'], '%H:%M').time()
        c_time = datetime.datetime.strptime(data['closing_time'], '%H:%M').time()
        new_restaurant = Restaurant_Profile_Model(public_id=str(uuid.uuid4()),name=data['name'],
                                                  rating=data['rating'],
                                                  opening_time=o_time,closing_time=c_time,
                                                  phone_no=data['phone_no'],image_url=data['image_url'],
                                                  capacity=data['capacity'],delivery_price=data['delivery_price'],
                                                  tax_percentage=data['tax_percentage'],table_booking_url=data['table_booking_url'],
                                                  user_auth_id=current_user.id,price_group=data['price_group'],popularity=data['popularity'],
                                                  description = data['description'])
        point = 'POINT({} {})'.format(data['address']['long'], data['address']['lat'])
        new_address = Addresses_Model(public_id=str(uuid.uuid4()), street=data['address']['street'], area=data['address']['area'],
                                          city=data['address']['city'],
                                          state=data['address']['state'], zipcode=data['address']['zipcode'], country=data['address']['country'],
                                          long=data['address']['long'],
                                          lat=data['address']['lat'],geo_location=point,restaurant=new_restaurant)

        db.session.add(new_restaurant)
        db.session.add(new_address)

        return commit_changes()


@restaurant_profile_namespace.route('/all')
class All_Restaurant_Profiles(Resource):
    @rest_api_user.marshal_with(get_all_restaurants_profile_api_model)
    def get(self):
        """
        return all the Restaurants
        """
        restaurants = Restaurant_Profile_Model.query.all()
        output=[]

        for r in restaurants:

            restaurant_data = getRestaurant(r)

            output.append(restaurant_data)

        return success_status("restaurants", output)


@filters_namespace.route('')
class RestaurantFilters(Resource):
    @filters_namespace.marshal_with(get_all_filter_api_model)
    def get(self):
        """
        return all the Filters
        """
        filters = Filters_Model.query.all()
        output=[]
        for filter in filters:
            filter_data = dict()
            filter_data['public_id'] = filter.public_id
            filter_data['name'] = filter.name
            output.append(filter_data)

        return success_status("filters",output)

    @filters_namespace.expect(add_filter_api_model)
    def post(self):
        """
        Add a new filter
        """
        data = request.get_json()

        new_filter = Filters_Model(public_id=str(uuid.uuid4()),name=data['name'])

        return AddObject(new_filter)


@filters_namespace.route('/<string:id>')
class RestaurantFilterOneItem(Resource):
    @filters_namespace.marshal_with(get_all_restaurants_profile_api_model)
    def get(self,id):
        """
        return all the Restaurants for a given filter
        """
        filter_ = Filters_Model.query.filter_by(public_id=id).first()
        if not filter_:
            return error_status(0,"No Filter Found")
        restaurant_filters = Restaurant_Filters_Model.query.filter_by(filter=filter_).all()

        restaurants = [r_f.restaurant for r_f in restaurant_filters]
        output=[]

        for r in restaurants:
            restaurant_data = getRestaurant(r)
            output.append(restaurant_data)

        return success_status("restaurants", output)

    @filters_namespace.expect(add_restaurant_filter_api_model)
    def post(self,id):
        """
        Add new restaurant Filter
        """
        filter_ = Filters_Model.query.filter_by(public_id=id).first()
        if not filter_:
            return error_status(0,"No Filter Found")

        data = request.get_json()
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=data['public_id']).first()
        if not restaurant:
            return error_status(0,"No Restaurant Found")

        new_restaurant_filter = Restaurant_Filters_Model(restaurant=restaurant,filter=filter_)

        return AddObject(new_restaurant_filter)

    @filters_namespace.expect(update_filter_api_model)
    def put(self,id):
        """
        update the filter
        """
        filter_ = Filters_Model.query.filter_by(public_id=id).first()
        if not filter_:
            return error_status(0,"No Filter Found")

        data = request.get_json()
        filter_.name = data['name']
        db.session.commit()

    def delete(self, id):
        """
        delete filter
        """
        filter_ = Filters_Model.query.filter_by(public_id=id).first()
        if not filter_:
            return error_status(0, "No Filter Found")

        return DeleteObject(filter_)