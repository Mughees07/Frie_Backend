import datetime

from flask_restplus import fields
from app.user import rest_api_user
from app.user.addresses.serializers import add_address_api_model, get_addresses_api_model


class TimeFormat(fields.Raw):
    def format(self, value):
        return datetime.time.strftime(value, "%H:%M")
location_api_model = rest_api_user.model('Location', {
    "latitude": fields.Float,
    "longitude": fields.Float
})
restaurant_profile_api_model = rest_api_user.model('restaurant_profile', {
    'public_id': fields.String(),
    'name': fields.String(),
    'description': fields.String(),
    'rating' : fields.Float,
    'opening_time': fields.String,
    'closing_time': fields.String,
    'phone_no' : fields.String(),
    'image_url' : fields.String(),
    'capacity' : fields.Integer,
    'delivery_price': fields.Integer,
    'tax_percentage': fields.Integer,
    'table_booking_url': fields.String,
    'price_group' : fields.String,
    'popularity': fields.Integer,
    'tags': fields.List(fields.String),
    'review_count' : fields.Integer,
    'address': fields.String,
    'location' : fields.Nested(location_api_model)
})


add_restaurant_api_model = rest_api_user.model('add restaurant', {
    'name': fields.String(required=True),
    'rating' : fields.Float(required=True),
    'opening_time' : fields.String(required=True),
    'closing_time' : fields.String(required=True),
    'phone_no' : fields.String(required=True),
    'image_url' : fields.String(required=True),
    'capacity' : fields.Integer(required=True),
    'delivery_price': fields.Integer(required=True),
    'tax_percentage': fields.Integer(required=True),
    'table_booking_url': fields.String(required=True),
    'price_group' : fields.Integer(required= True),
    'popularity' : fields.Integer(required=True),
    'description' : fields.String(required=True),
    'address': fields.Nested(add_address_api_model)
})

get_all_restaurants_profile_api_model = rest_api_user.model('List of all restaurant', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'restaurants': fields.List(fields.Nested(restaurant_profile_api_model))
})

get_one_restaurants_profile_api_model = rest_api_user.model('One restaurant', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'restaurant': fields.Nested(restaurant_profile_api_model)
})

delete_restaurant_api_model = rest_api_user.model('delete restaurant', {
    'public_id': fields.String()
})




#############################Favourites #################################################

add_fav_api_model = rest_api_user.model('add  Reference ( 0- Restaurant ,1- Events , 2 - Food Album, to Collection type(1-favourites ,0 - bookmark' , {
    'reference_id' : fields.String(required=True),
    'reference_type' : fields.Integer(required=True),
    'type': fields.Integer(required=True)
})


update_fav_api_model = rest_api_user.model('delete/get  Reference ( 0- Restaurant ,1- Events , 2 - Food Album, to Collection type(1-favourites ,0 - bookmark' , {
    'reference_type' : fields.Integer(required=True),
    'type': fields.Integer(required=True)
})

##########################Filters

add_filter_api_model = rest_api_user.model('add filter', {
    'name': fields.String(required=True)
})

get_filter_api_model = rest_api_user.model('get filter', {
    'public_id': fields.String(),
    'name': fields.String()
})

get_all_filter_api_model = rest_api_user.model('List of all filters', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'filters': fields.List(fields.Nested(get_filter_api_model))
})

update_filter_api_model = rest_api_user.model('update filter', {
    'name': fields.String(required=True)
})

add_restaurant_filter_api_model = rest_api_user.model('add restaurant filter', {
    'public_id': fields.String(required=True)
})