from flask import request, jsonify

from app import db

from app.utils import success_status, error_status


def get_all_user_addresses(addresses):

    output = []
    for address in addresses:
        addresses_data = dict()
        addresses_data['public_id'] = address.public_id
        addresses_data['street'] = address.street
        addresses_data['area'] = address.area
        addresses_data['city'] = address.city
        addresses_data['state'] = address.state
        addresses_data['zipcode'] = address.zipcode
        addresses_data['country'] = address.country
        addresses_data['long'] = address.long
        addresses_data['lat'] = address.lat
        output.append(addresses_data)

    return success_status("addresses", output)


def update_address(address):

    data = request.get_json()
    point = 'POINT({} {})'.format(data['long'], data['lat'])
    address.street = data['street']
    address.area = data['area']
    address.city = data['city']
    address.state = data['state']
    address.zipcode = data['zipcode']
    address.country = data['country']
    address.long = data['long']
    address.lat = data['lat']
    address.geo_location = point

    try:
        db.session.commit()
    except Exception as e:
        return error_status(0, str(e))

    return success_status()


def getUserAddress(addresses):

    for address in addresses:
        addresses_data = {}
        addresses_data['public_id'] = address.public_id
        addresses_data['street'] = address.street
        addresses_data['area'] = address.area
        addresses_data['city'] = address.city
        addresses_data['state'] = address.state
        addresses_data['zipcode'] = address.zipcode
        addresses_data['country'] = address.country
        addresses_data['long'] = address.long
        addresses_data['lat'] = address.lat

        return addresses_data

def getOneUserAddress(address):

    addresses_data = {}
    addresses_data['public_id'] = address.public_id
    addresses_data['street'] = address.street
    addresses_data['area'] = address.area
    addresses_data['city'] = address.city
    addresses_data['state'] = address.state
    addresses_data['zipcode'] = address.zipcode
    addresses_data['country'] = address.country
    addresses_data['long'] = address.long
    addresses_data['lat'] = address.lat

    return addresses_data


def getAddressToString(address):
    if(len(address) > 0):
        return  str(address[0].street)+" "+str(address[0].area)+" " + str(address[0].city)

    return ""


def getAddressLocation(address):
     if(len(address) > 0):
        return {"latitude": address[0].lat, "longitude": address[0].long}


def AddObject(obj):
    try:
        db.session.add(obj)
        db.session.commit()
    except Exception as e:
        return jsonify({"status": 0, 'message': str(e)})

    return success_status('address',getOneUserAddress(obj))