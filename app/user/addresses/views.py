import uuid

from sqlalchemy import func

from app import constants
from app.user.addresses.models import Addresses_Model
from flask import request
from flask_restplus import Resource

from app.user import rest_api_user
from app.user.addresses.serializers import get_all_addresses_api_model, add_address_api_model, update_address_api_model, \
    get_one_address_api_model
from app.user.users_auth.decorators import token_required, token_required_user
from app.utils import DeleteObject, error_status
from app.user.addresses.controllers import *

addresses_namespace = rest_api_user.namespace('addresses', description='Operations related to Addresses')

@addresses_namespace.route('')
class Addresses(Resource):
    @addresses_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.marshal_with(get_all_addresses_api_model)
    def get(self,current_user):
        """
        return the Addresses associated with the current user (customer/restaurant)
        """
        if current_user.type == 0:
            customer_addresses = current_user.customer.addresses_child

            if not customer_addresses:
                return error_status(constants.ERROR.ADDRESS_NOT_EXISTS[0], constants.ERROR.ADDRESS_NOT_EXISTS[1])

            return get_all_user_addresses(customer_addresses)

        elif current_user.type == 2:
            restaurant_addresses = current_user.restaurant.addresses_child

            if not restaurant_addresses:
                return error_status(constants.ERROR.ADDRESS_NOT_EXISTS[0], constants.ERROR.ADDRESS_NOT_EXISTS[1])

            return get_all_user_addresses(restaurant_addresses)


    @addresses_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_address_api_model)
    @rest_api_user.marshal_with(get_one_address_api_model)
    def post(self,current_user):
        """
        Given Street, Area, City, State, Zipcode, Country, Long and Lat, this method creates a new address for given user
        """
        data = request.get_json()

        if current_user.type == 0:
            point = 'POINT({} {})'.format(data['long'], data['lat'])
            new_address = Addresses_Model(public_id=str(uuid.uuid4()), street=data['street'], area=data['area'],
                                          city=data['city'],
                                          state=data['state'], zipcode=data['zipcode'], country=data['country'],
                                          long=data['long'],
                                          lat=data['lat'],geo_location=point,customer_id=current_user.customer.id)

            return AddObject(new_address)

        elif current_user.type == 2:
            point = 'POINT({} {})'.format(data['long'], data['lat'])
            new_address = Addresses_Model(public_id=str(uuid.uuid4()), street=data['street'], area=data['area'],
                                          city=data['city'],
                                          state=data['state'], zipcode=data['zipcode'], country=data['country'],
                                          long=data['long'],
                                          lat=data['lat'], geo_location=point, restaurant_id=current_user.restaurant.id)

            return AddObject(new_address)

@addresses_namespace.route('/<string:id>')
class Address_Item(Resource):
    @rest_api_user.expect(update_address_api_model)
    @addresses_namespace.doc(security='apikey',params={'id': 'Address ID'})
    @token_required
    def put(self,id):
        """
        Given Street, Area, City, State, Zipcode, Country, Long and Lat, this method updates the address
        """
        address = Addresses_Model.query.filter_by(public_id=id).first()

        if not address:
            return error_status(constants.ERROR.ADDRESS_NOT_EXISTS[0], constants.ERROR.ADDRESS_NOT_EXISTS[1])

        return update_address(address)

    @addresses_namespace.doc(security='apikey',params={'id': 'Address ID'})
    @token_required
    def delete(self,id):
        """
        Given public id of an address, this method deletes the address
        """
        address = Addresses_Model.query.filter_by(public_id=id).first()

        if not address:
            return error_status(constants.ERROR.ADDRESS_NOT_EXISTS[0], constants.ERROR.ADDRESS_NOT_EXISTS[1])

        return DeleteObject(address)
