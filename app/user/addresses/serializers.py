from flask_restplus import fields
from app.user import rest_api_user


pagination = rest_api_user.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

location_api_model = rest_api_user.model('Location', {
    "latitude": fields.Float,
    "longitude": fields.Float
})

get_addresses_api_model = rest_api_user.model('get addresses', {
    'public_id': fields.String(),
    'street': fields.String(default="street"),
    'area': fields.String(default="area 5"),
    'city': fields.String(default="Lahore"),
    'state': fields.String(default="Punjab"),
    'zipcode': fields.String(default="12345"),
    'country': fields.String(default="Pakistan"),
    'long': fields.Float(default=74.356757),
    'lat': fields.Float(default=31.526776)

})


get_all_addresses_api_model = rest_api_user.model('List of all addresses', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'addresses': fields.List(fields.Nested(get_addresses_api_model),default=[])
})


get_one_address_api_model = rest_api_user.model('Address', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'address': fields.Nested(get_addresses_api_model,default={})
})

add_address_api_model = rest_api_user.model('add address', {
    'street': fields.String(required=True),
    'area': fields.String(required=True),
    'city': fields.String(required=True),
    'state': fields.String(required=True),
    'zipcode': fields.String(required=True),
    'country': fields.String(required=True),
    'long': fields.Float(required=True),
    'lat': fields.Float(required=True)
})

update_address_api_model = rest_api_user.model('update address', {
    'street': fields.String(required=True),
    'area': fields.String(required=True),
    'city': fields.String(required=True),
    'state': fields.String(required=True),
    'zipcode': fields.String(required=True),
    'country': fields.String(required=True),
    'long': fields.Float(required=True),
    'lat': fields.Float(required=True)
})
