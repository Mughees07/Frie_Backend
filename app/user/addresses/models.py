from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry

from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp
from app.user.customer_profile.models import Customer_Profile_Model
from app.user.restaurant_profile.models import Restaurant_Profile_Model


class Addresses_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "addresses"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    street = db.Column(db.String(200))
    area = db.Column(db.String(200))
    city = db.Column(db.String(100))
    state = db.Column(db.String(100))
    zipcode = db.Column(db.String(100))
    country = db.Column(db.String(100))
    long = db.Column(db.Float)
    lat = db.Column(db.Float)
    geo_location = db.Column(Geometry(geometry_type="POINT"))
    customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"), nullable=True)
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant_profile.id"), nullable=True)
    restaurant = relationship(Restaurant_Profile_Model)
    customer = relationship(Customer_Profile_Model)
    # customer = db.relationship('Customer_Profile_Model', backref='customer_address')
    # restaurant = db.relationship('Restaurant_Profile_Model', backref='restaurant_address')

# class Customer_Addresses_Model(db.Model):
#
#     __tablename__ = "customer_addresses"
#     id = db.Column(db.Integer, primary_key=True)
#     public_id = db.Column(db.String(200))
#     addresses_id = db.Column(db.Integer, db.ForeignKey("addresses.id"), nullable=True)
#     customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"), nullable=True)
#
#
# class Restaurant_Addresses_Model(db.Model):
#
#     __tablename__ = "restaurant_addresses"
#     id = db.Column(db.Integer, primary_key=True)
#     public_id = db.Column(db.String(200))
#     addresses_id = db.Column(db.Integer, db.ForeignKey("addresses.id"),nullable=True)
#     restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant_profile.id"),nullable=True)