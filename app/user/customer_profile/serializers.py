from flask_restplus import fields
from app.user import rest_api_user
from app.user.addresses.serializers import add_address_api_model, get_addresses_api_model
from app.user.restaurant_profile.serializers import restaurant_profile_api_model

pagination = rest_api_user.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

get_customer_api_model = rest_api_user.model('get customer', {
    'public_id': fields.String(),
    'name': fields.String(default="Customer Name"),
    'username': fields.String(default="username"),
    'image_url': fields.String(default="/images/driver_profile/customer.png"),
    'about': fields.String(default="about"),
    'addresses': fields.List(fields.Nested(get_addresses_api_model),default=[])
})

get_all_customer_api_model = rest_api_user.model('List of all customers', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'customers': fields.List(fields.Nested(get_customer_api_model),default=[])
})

add_customer_api_model = rest_api_user.model('add customer', {
    'name': fields.String(required=True),
    'username': fields.String(required=True),
    'about': fields.String(required=True),
    'image_url': fields.String(required=True),
    'address': fields.Nested(add_address_api_model),
})

update_customer_api_model = rest_api_user.model('update customer', {
    'name': fields.String(required=True),
    'username': fields.String(required=True),
    'address': fields.String(required=True),
    'about': fields.String(required=True),
    'image_url': fields.String(required=True)
})

login_user = rest_api_user.model('login_model', {
    'email': fields.String(required=True),
    'password': fields.String(required=True)
})

public_id_model = rest_api_user.model('public_id_model', {
    'public_id': fields.String(required=True)
})

forgot_password = rest_api_user.model('forgot_password', {
    'email': fields.String(required=True)
})

get_user_profile_tab = rest_api_user.model('user profile tab', {
    'email': fields.String(required=True)
})

################################ Food Album ##############################

food_album = rest_api_user.model('food album' , {
    'public_id' : fields.String(),
    'title' : fields.String(default="Food Album"),
    'description' : fields.String(default="Description"),
    'type' : fields.Integer(default=0),# 0 - private , 1 - public
    'image_url' : fields.String(default="image_url"),
    'restaurants' : fields.List(fields.Nested(restaurant_profile_api_model),default=[]),
    'owner' : fields.Nested(get_customer_api_model,default={})
})

get_all_food_albums = rest_api_user.model('get all food album' , {
    'status' : fields.Integer,
    'message' : fields.String,
    'food_albums' : fields.List(fields.Nested(food_album),default=[])
})


add_food_album = rest_api_user.model('add food album' , {

    'title' : fields.String(),
    'description' : fields.String(),
    'is_private' : fields.Integer(),# 0 - public , 1 - private
    'image_url' : fields.String()

})

food_album_detail_api_model = rest_api_user.model('get food album details', {
    'album' : fields.Nested(food_album,default={}),
    'restaurants_count' : fields.Integer(default=0),
    'bookmarks_count' : fields.Integer(default=0),
    'owner_followers_count' : fields.Integer(default=0),
    'is_bookmark' : fields.Boolean(default=False),
    'is_following_owner' : fields.Boolean(default=False)
})

get_one_food_albums_api_model = rest_api_user.model('get one food album', {
    'status' : fields.Integer,
    'message' : fields.String,
    'details' : fields.List(fields.Nested(food_album_detail_api_model),default=[])
})
################################ Customer Followings #############################
add_customer_following_api_model = rest_api_user.model('add customer following' , {

    'user_id' : fields.String(),
    'is_following' : fields.Integer()# 0 - no , 1 - yes

})

get_follower_following_api_model = rest_api_user.model('get followers followings', {
    'public_id': fields.String(),
    'name': fields.String(default="Customer Name"),
    'username': fields.String(default="username"),
    'image_url': fields.String(default="/images/driver_profile/customer.png"),
    'about': fields.String(default="about"),
    'is_following': fields.Boolean(default=False),
    'addresses': fields.List(fields.Nested(get_addresses_api_model),default=[])
})

get_all_followers_api_model = rest_api_user.model('List of all followers', {
    'followers': fields.List(fields.Nested(get_follower_following_api_model),default=[])
})

get_all_followings_api_model = rest_api_user.model('List of all followings', {
    'followings': fields.List(fields.Nested(get_follower_following_api_model),default=[])
})


get_customer_followers_followings_api_model = rest_api_user.model('get customer followers and followings' , {

    'status' : fields.Integer,
    'message' : fields.String,
    'followers' : fields.Nested(get_all_followers_api_model,default={}),
    'followings' : fields.Nested(get_all_followings_api_model,default={})

})

add_reward_type_api_model = rest_api_user.model('add reward type', {
    'reward_description': fields.String(),
    'reward_type': fields.Integer(),
    'reward_points': fields.Integer()
})

add_reward_action_api_model = rest_api_user.model('add reward action', {
    'reward_action_description': fields.String(),
    'reward_action': fields.Integer(),
    'reward_points': fields.Integer()
})

add_customer_reward_action_api_model = rest_api_user.model('add customer reward action', {
    'reward_action_id': fields.String()
})

###############################Favorite Event##########################

get_fav_events_api_model = rest_api_user.model('get fav events', {
    'public_id': fields.String(),
    'title': fields.String(default="Fav Event"),
    'ticket_price': fields.String(default=0),
    'image_url': fields.String(default="/images/restaurant_profile/restaurant.png"),
    'date_time': fields.DateTime(default="2019-09-12T08:53:49.832Z"),
    'address': fields.String(default="Lahore"),
    'is_favorite': fields.Boolean(default=False)
})

get_all_fav_events_api_model = rest_api_user.model('get all fav events' , {
    'status' : fields.Integer,
    'message' : fields.String,
    'events' : fields.List(fields.Nested(get_fav_events_api_model),default=[])
})