import json
import uuid

import requests
from attr._make import and_
from flask import request
from flask_restplus import Resource
from sqlalchemy import func

from app import db, constants
from app.restaurant.events.models import Event_Model
from app.user import rest_api_user
from app.user.addresses.models import Addresses_Model
from app.user.customer_profile.models import Customer_Profile_Model, Customer_Favourites_Model, \
    Customer_Food_Album_Model, Food_Album_Restaurant_Model, User_Followings_Model, Reward_Actions_Model, \
    Reward_Type_Model, Customer_Reward_Action_Model
from app.user.customer_profile.serializers import add_customer_api_model, get_all_customer_api_model, \
    update_customer_api_model, get_user_profile_tab, add_food_album, get_all_food_albums, \
    add_customer_following_api_model, get_customer_followers_followings_api_model, add_reward_type_api_model, \
    add_reward_action_api_model, add_customer_reward_action_api_model, food_album, get_one_food_albums_api_model, \
    get_all_fav_events_api_model
from app.user.customer_profile.controllers import get_customer_profile, update_customer, getAllFavRestaurants, \
    updateFoodAlbum, getAllAlbums, getAllFavEvents, getAllFavFoodAlbums, get_all_customer_profiles, \
    get_recent_activities, getAlbumsOnly, get_user_activities_data, get_customer_reward_points, \
    get_customer_reward_details, getFoodAlbum, get_one_food_album_details
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.restaurant_profile.serializers import add_fav_api_model, get_all_restaurants_profile_api_model, \
    update_fav_api_model
from app.user.users_auth.decorators import token_required_user, token_required
from app.utils import success_status, AddObject, DeleteObject, error_status, commit_changes

customer_profile_namespace = rest_api_user.namespace('customer_profile', description='Operations related to Customer Profile')
customer_profile_collections_namespace = rest_api_user.namespace('customer_collections', description='Operations related to Customer Profile Favourites')
customer_food_album_namespace = rest_api_user.namespace('customer_food_albums', description='Operations related to Customer Food Albums')
customer_followings_namespace = rest_api_user.namespace('customer_followings', description='Operations related to Customer Followings')
rewards_namespace = rest_api_user.namespace('rewards', description='Operations related to Rewards')

@customer_profile_namespace.route('')
class Customer_Profile(Resource):
    @customer_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.marshal_with(get_all_customer_api_model)
    def get(self,current_user):
        """
        return the customer associated with current user
        """
        customer = current_user.customer

        if not customer:
            return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0],constants.ERROR.CUSTOMER_NOT_EXISTS[1])

        return get_customer_profile(customer)


    @customer_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_customer_api_model)
    def post(self,current_user):
        """
        Given Name, Username, Address, Image_url and about, this method creates a new customer
        """
        data = request.get_json()

        new_customer = Customer_Profile_Model(public_id=str(uuid.uuid4()),name=data['name'], username=data['username'],
                                              image_url=data['image_url'], about=data['about'],user_auth_id=current_user.id)

        # point = 'POINT({} {})'.format(data['long'], data['lat'])
        # new_address = Addresses_Model(public_id=str(uuid.uuid4()), street=data['address']['street'], area=data['address']['area'],
        #                                   city=data['address']['city'],
        #                                   state=data['address']['state'], zipcode=data['address']['zipcode'], country=data['address']['country'],
        #                                   long=data['address']['long'],
        #                                   lat=data['address']['lat'],geo_location=point,customer=new_customer)
        #
        # db.session.add(new_customer)
        # db.session.add(new_address)

        return AddObject(new_customer)

    @customer_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_customer_api_model)
    def put(self, current_user):
        """
        Given Name, Username, Address, Image_url and about, this method creates a new customer
        """
        data = request.get_json()

        new_customer = Customer_Profile_Model.query.filter_by(user = current_user).all()

        new_customer.name = data['name']
        new_customer.username = data['username']
        new_customer.image_url = data['image_url']
        new_customer.about = data['about']

        # point = 'POINT({} {})'.format(data['long'], data['lat'])
        # new_address = Addresses_Model(public_id=str(uuid.uuid4()), street=data['address']['street'], area=data['address']['area'],
        #                                   city=data['address']['city'],
        #                                   state=data['address']['state'], zipcode=data['address']['zipcode'], country=data['address']['country'],
        #                                   long=data['address']['long'],
        #                                   lat=data['address']['lat'],geo_location=point,customer=new_customer)
        #
        # db.session.add(new_customer)
        # db.session.add(new_address)

        return commit_changes()


@customer_profile_namespace.route('/<string:id>')
class Customer_Profile_User(Resource):
    @rest_api_user.expect(update_customer_api_model)
    @customer_profile_namespace.doc(security='apikey',params={'id': 'Customer ID'})
    @token_required
    def put(self,id):
        """
        Given Name, Username, Address, Image_url and about, this method updates customer profile,
        authentication token is required
        """
        customer = Customer_Profile_Model.query.filter_by(public_id=id).first()
        if not customer:
            return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0],constants.ERROR.CUSTOMER_NOT_EXISTS[1])

        return update_customer(customer)


    @customer_profile_namespace.doc(security='apikey',params={'id': 'Customer ID'})
    @token_required
    def delete(self,id):
        """
        Given public_id, this method deletes customer profile,
        authentication token is required
        """
        customer = Customer_Profile_Model.query.filter_by(public_id=id).first()
        if not customer:
            return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0],constants.ERROR.CUSTOMER_NOT_EXISTS[1])

        return DeleteObject(customer)

@customer_profile_namespace.route('/tab_profile')
class Customer_Tab_Profile(Resource):
    @customer_profile_namespace.doc(security='apikey')
    @token_required_user
    # @rest_api_user.marshal_with(get_user_profile_tab)
    def get(self,current_user):
        """
        return the customer profile tab data associated with the current user
        """
        customer = current_user.customer

        if not customer:
            return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0],constants.ERROR.CUSTOMER_NOT_EXISTS[1])

        albums_count = len(customer.customer_albums)
        bookmarks_count = Customer_Favourites_Model.query.filter_by(customer_id=customer.id, type=0).count()
        followings_count = User_Followings_Model.query.filter_by(follower=customer).count()
        followers_count = User_Followings_Model.query.filter_by(following=customer).count()
        food_albums = getAlbumsOnly(customer.customer_albums)
        reward_points = get_customer_reward_points(customer)
        customer_data = get_customer_profile(customer)
        current_reward_level, reward_next_level, reward_next_level_points = get_customer_reward_details(reward_points)
        # url = ""
        # r = requests.get(url, params={'user_id': [customer.public_id]})
        # response_data = json.loads(r.content.decode('utf-8'))
        response_data = [{
            "activity": 2,
            "public_id": "92000403-f285-43f6-908c-951a93e327e4",
            "type": 0,
            "user_id": "43e85880-2aa7-49da-aeec-2806a46f8320"}]
        recent_activities = get_user_activities_data(response_data)

        profile_tab_data = {"user": customer_data['customers'], "albums_count": albums_count,
                            "bookmarks_count": bookmarks_count, "followings_count": followings_count,"followers_count": followers_count,
                            "reward_current_level":current_reward_level, "reward_next_level":reward_next_level,
                            "reward_next_level_points":reward_next_level_points, "food_albums": food_albums['food_albums'],
                            "recent_activity": recent_activities}

        return success_status("profile_tab_data",profile_tab_data)

@customer_profile_namespace.route('/public_profile/<string:id>')
class CustomerPublicProfile(Resource):
    @customer_profile_namespace.doc(security='apikey', params={'id': 'Customer ID'})
    @token_required_user
    def get(self, id, current_user):
        """
        return the customer profile tab data associated with the current user
        """
        current_customer = current_user.customer
        customer = Customer_Profile_Model.query.filter_by(public_id=id).first()
        if not customer:
            return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0],constants.ERROR.CUSTOMER_NOT_EXISTS[1])

        customer_data = get_customer_profile(customer)
        is_following = False
        following = User_Followings_Model.query.filter_by(following=current_customer).first()
        if following:
            is_following = True
        user_data = customer_data['customers']
        user_data["is_following"] = is_following
        albums_count = len(customer.customer_albums)
        bookmarks_count = Customer_Favourites_Model.query.filter_by(customer_id=customer.id, type=0).count()
        followings_count = User_Followings_Model.query.filter_by(follower=customer).count()
        followers_count = User_Followings_Model.query.filter_by(following=customer).count()
        food_albums = getAlbumsOnly(customer.customer_albums)

        # url = ""
        # r = requests.get(url, params={'user_id': [customer.public_id]})
        # response_data = json.loads(r.content.decode('utf-8'))
        response_data = [{
       "activity": 2,
       "public_id": "92000403-f285-43f6-908c-951a93e327e4",
       "type": 0,
       "user_id": "43e85880-2aa7-49da-aeec-2806a46f8320"}]
        recent_activities = get_user_activities_data(response_data)
        profile_data = {"user": user_data, "albums_count": albums_count, "bookmarks_count": bookmarks_count,
                            "followings_count": followings_count,"followers_count": followers_count,
                            "food_albums": food_albums['food_albums'], "recent_activity": recent_activities}

        return success_status("profile",profile_data)


@customer_profile_collections_namespace.route('/')
class Customer_Favourites(Resource):
    @rest_api_user.expect(add_fav_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def post(self,current_user):
        """
        Adds an itme based on item_id ,  reference type(0- Restaurant ,1- Events , 2 - Food Album) and type type(1-favourites ,0 - bookmark)
        """
        data= request.get_json()

        obj=Customer_Favourites_Model(reference_id=data['reference_id'],reference_type=data['reference_type'],type=data['type'],customer=current_user.customer)

        return AddObject(obj)

@customer_profile_collections_namespace.route('/<string:id>')
@customer_profile_collections_namespace.doc(params={"id": "Restaurant/Events/FoodAlbum ID "})
class Customer_Favourites_Delete(Resource):

    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def delete(self,current_user, id):
        """
        delete  restaurant based on ID and Reference ( 0- Restaurant ,1- Events , 2 - Food Album ) and Collection type(1-favourites ,0 - bookmark)
        """
        obj = Customer_Favourites_Model.query.filter_by(reference_id=id).first()
        return DeleteObject(obj)



###############################################Get Favourite Restaurants ###################################################
@customer_profile_collections_namespace.route('/favourite_restaurants')
class Customer_Favourite_Restaurants(Resource):

    @rest_api_user.marshal_with(get_all_restaurants_profile_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        """
        Returns all the favorite restaurants  stored in DB based on
        """
        restaurants = Customer_Favourites_Model.query.filter_by(customer=current_user.customer).filter_by(
            type=1).filter_by(reference_type=0).all()
        return getAllFavRestaurants(restaurants)

@customer_profile_collections_namespace.route('/bookmarked_restaurants')
class Customer_Bookmarked_Restaurants(Resource):

    @rest_api_user.marshal_with(get_all_restaurants_profile_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        """
        Returns all the favorite restaurants  stored in DB based on
        """
        restaurants = Customer_Favourites_Model.query.filter_by(customer=current_user.customer).filter_by(type=0).filter_by(reference_type=0).all()
        return getAllFavRestaurants(restaurants)

##############################Get Food Events #######################################

@customer_profile_collections_namespace.route('/favourite_events')
class Customer_Favourite_Events(Resource):

    @rest_api_user.marshal_with(get_all_fav_events_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        """
        Returns all the favorite restaurants  stored in DB based on
        """
        events = Customer_Favourites_Model.query.filter_by(customer=current_user.customer).filter_by(
            type=1).filter_by(reference_type=1).all()
        return getAllFavEvents(events)

@customer_profile_collections_namespace.route('/bookmarked_event')
class Customer_Bookmarked_Albums(Resource):

    @rest_api_user.marshal_with(get_all_restaurants_profile_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        """
        Returns all the favorite restaurants  stored in DB based on
        """
        events = Customer_Favourites_Model.query.filter_by(customer=current_user.customer).filter_by(type=0).filter_by(reference_type=1).all()
        return getAllFavEvents(events)

################################### Get Favourite Food ALbums ######################################
@customer_profile_collections_namespace.route('/favourite_albums')
class Customer_Favourite_Albums(Resource):

    @rest_api_user.marshal_with(get_all_restaurants_profile_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        """
        Returns all the favorite restaurants  stored in DB based on
        """
        albums = Customer_Favourites_Model.query.filter_by(customer=current_user.customer).filter_by(
            type=1).filter_by(reference_type=2).all()
        return getAllFavFoodAlbums(albums)


@customer_profile_collections_namespace.route('/bookmarked_Albums')
class Customer_Bookmarked_Albums(Resource):

    @rest_api_user.marshal_with(get_all_restaurants_profile_api_model)
    @customer_profile_collections_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        """
        Returns all the favorite restaurants  stored in DB based on
        """
        albums = Customer_Favourites_Model.query.filter_by(customer=current_user.customer).filter_by(type=0).filter_by(reference_type=2).all()
        return getAllFavFoodAlbums(albums)


################################## Food Albums ##################################


@customer_food_album_namespace.route('/')
class Customer_Food_Albums(Resource):

    @rest_api_user.expect(add_food_album)
    @customer_food_album_namespace.doc(security='apikey')
    @token_required_user
    def post(self,current_user):
        data = request.get_json()
        title = data['title']
        description = data['description']
        is_private = data['is_private']
        image_url = data['image_url']

        food_album = Customer_Food_Album_Model(public_id= str(uuid.uuid4()) , title = title , description = description , type = is_private , image_url = image_url , customer = current_user.customer)

        return AddObject(food_album)

    @rest_api_user.marshal_with(get_all_food_albums)
    @customer_food_album_namespace.doc(security='apikey')
    @token_required
    def get(self):
        food_albums = Customer_Food_Album_Model.query.all()
        return getAllAlbums(food_albums)


@customer_food_album_namespace.route('/<string:id>')
class FoodAlbums(Resource):
    @rest_api_user.marshal_with(get_one_food_albums_api_model)
    @customer_food_album_namespace.doc(security='apikey', params={'id': 'Food Album ID'})
    @token_required_user
    def get(self,id,current_user):
        customer = current_user.customer
        if not customer:
            return error_status(0, "No Customer Found")

        food_album = Customer_Food_Album_Model.query.filter_by(public_id=id).first()
        if not food_album:
            return error_status(0, "No Food Album Found")

        return get_one_food_album_details(food_album,customer)


@customer_food_album_namespace.route('/update/<string:id>')
class Update_Food_Album(Resource):

    @rest_api_user.expect(add_food_album)
    @customer_profile_namespace.doc(security='apikey', params={'id': 'Food Album ID'})
    @token_required
    def put(self,id):
        """
        Given Title, Description, is_private, and Image_url, this method updates food album,
        authentication token is required
        """
        data= request.get_json()
        album_id = id
        title = data['title']
        description = data['description']
        is_private = data['is_private']
        image_url = data['image_url']

        food_album = Customer_Food_Album_Model.query.filter_by(public_id=id).first()
        if not food_album:
            return error_status(0, "No Customer found!")

        return updateFoodAlbum(album_id,title,description,is_private,image_url)

@customer_food_album_namespace.route('/delete/<string:id>')
class Delete_Food_Album(Resource):

    @customer_profile_namespace.doc(security='apikey', params={'id': 'Food Album ID'})
    @token_required
    def delete(self, id):
        """
        Given public_id, this method deletes food album,
        authentication token is required
        """
        food_album = Customer_Food_Album_Model.query.filter_by(public_id=id).first()
        if not food_album:
            return error_status(0, "No Food Album found!")

        return DeleteObject(food_album)



@customer_food_album_namespace.route('/food_Album_restaurant/')
class Food_Album_Restaurants(Resource):

    @customer_food_album_namespace.doc(security='apikey', params={'album_id': 'Album ID' , 'rest_id' : 'Restaurant ID'})
    @token_required
    def post(self):
        """
        Given Title, Description, is_private, and Image_url, this method adds new restaurant food album,
        authentication token is required
        """
        album_id = request.args['album_id']
        rest_id = request.args['rest_id']
        album = Customer_Food_Album_Model.query.filter_by(public_id=album_id).first()
        rest = Restaurant_Profile_Model.query.filter_by(public_id=rest_id).first()

        if not album:
            return error_status(0, "No Album found!")

        if not rest:
            return error_status(0, "No Restaurant found!")

        food_album_restaurant =Food_Album_Restaurant_Model(album = album, restaurant = rest)

        return AddObject(food_album_restaurant)

    @customer_food_album_namespace.doc(security='apikey',params={'album_id': 'Album ID' , 'rest_id' : 'Restaurant ID'})
    @token_required
    def delete(self):
        """
        Given album_id and restaurant_id, this method deletes restaurant food album,
        authentication token is required
        """
        album_id = request.args['album_id']
        rest_id = request.args['rest_id']
        album = Customer_Food_Album_Model.query.filter_by(public_id = album_id).first()
        rest  = Restaurant_Profile_Model.query.filter_by(public_id = rest_id ).first()
        if not album:
            return error_status(0, "No Album found!")

        if not rest:
            return error_status(0, "No Restaurant found!")

        food_album = Food_Album_Restaurant_Model.query.filter_by(album = album).filter_by(restaurant = rest) .first()
        if not food_album:
            return error_status(0, "No Restaurant Album found!")

        return DeleteObject(food_album)



@customer_followings_namespace.route('')
class CustomerFollowings(Resource):
    # @customer_followings_namespace.marshal_with(get_customer_followers_followings_api_model)
    @customer_followings_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):

        customer = current_user.customer
        if not customer:
            return error_status(0, "No Customer Found")

        all_customers = Customer_Profile_Model.query.all()
        customer_followings = User_Followings_Model.query.filter_by(follower=customer).all()
        followings = [c_f.following.public_id for c_f in customer_followings]
        customer_followers = User_Followings_Model.query.filter_by(following=customer).all()
        followers = [c_f.follower.public_id for c_f in customer_followers]

        all_customers_data = get_all_customer_profiles(all_customers)
        followings_data = list()
        followers_data = list()
        for c in all_customers_data['customers']:
            if c['public_id'] == customer.public_id:
                continue
            else:
                if c['public_id'] in followings:
                    c['is_following'] = True
                else:
                    c['is_following'] = False
                followings_data.append(c)
                c_copy = c.copy()
                if c['public_id'] in followers:
                    c_copy['is_following'] = True
                else:
                    c_copy['is_following'] = False
                followers_data.append(c_copy)
        return {"status": 1, "message": "Success", "followers": followers_data, "followings": followings_data}

    @customer_followings_namespace.expect(add_customer_following_api_model)
    @customer_followings_namespace.doc(security='apikey')
    @token_required_user
    def post(self,current_user):

        customer = current_user.customer
        if not customer:
            return error_status(0,"No Customer Found")

        data = request.get_json()

        to_follow = Customer_Profile_Model.query.filter_by(public_id=data['user_id']).first()
        if not to_follow:
            return error_status(0, "No Customer Found to Follow/Unfollow")
        if data['is_following'] == 0:
            following = User_Followings_Model.query.filter_by(follower_id=customer.id,following_id=to_follow.id).first()
            if not following:
                return error_status(0, "No Following Found")

            return DeleteObject(following)
        elif data['is_following'] == 1:
            new_following = User_Followings_Model(public_id=str(uuid.uuid4()),
                                                  follower=customer,following=to_follow)
            return AddObject(new_following)
        else:
            return error_status(0,"is_following value is wrong")

@customer_profile_namespace.route('/recent_activity')
class FollowingsRecentActivity(Resource):
    # @customer_followings_namespace.marshal_with(get_customer_followers_followings_api_model)
    @customer_followings_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        customer = current_user.customer
        if not customer:
            return error_status(0,"No Customer Found")
        customer_followings = User_Followings_Model.query.filter_by(follower=customer).all()
        followings_public_id = [c_f.following.public_id for c_f in customer_followings]
        following_users = [c_f.following for c_f in customer_followings]
        url = ""
        r = requests.get(url, params={'id_list': followings_public_id})
        response_data = json.loads(r.content.decode('utf-8'))

        if r.status_code == 200:
            return get_recent_activities(response_data,following_users)
        else:
            error_status(0,r.status_code)


#########################Reward System############################
@rewards_namespace.route('')
class Rewards(Resource):
    # @customer_followings_namespace.marshal_with(get_customer_followers_followings_api_model)
    def get(self):
        reward_types = Reward_Type_Model.query.all()
        output = list()
        for r_type in reward_types:
            reward = dict()
            reward['public_id'] = r_type.public_id
            reward['reward_type'] = r_type.reward_type
            reward['reward_description'] = r_type.reward_description
            reward['reward_points'] = r_type.reward_points
            output.append(reward)
        return success_status("reward_types",output)

    @rewards_namespace.expect(add_reward_type_api_model)
    def post(self):
        data = request.get_json()
        new_reward_type = Reward_Type_Model(public_id=str(uuid.uuid4()),reward_type=data['reward_type'],
                                            reward_description=data['reward_description'],reward_points=data['reward_points'])

        return AddObject(new_reward_type)


@rewards_namespace.route('/actions')
class RewardActions(Resource):
    # @customer_followings_namespace.marshal_with(get_customer_followers_followings_api_model)
    def get(self):
        reward_actions = Reward_Actions_Model.query.all()
        output = list()
        for r_action in reward_actions:
            reward = dict()
            reward['public_id'] = r_action.public_id
            reward['reward_action'] = r_action.reward_action
            reward['reward_action_description'] = r_action.reward_action_description
            reward['reward_points'] = r_action.reward_points
            output.append(reward)
        return success_status("reward_types", output)

    @rewards_namespace.expect(add_reward_action_api_model)
    def post(self):
        data = request.get_json()
        new_reward_action = Reward_Actions_Model(public_id=str(uuid.uuid4()), reward_action=data['reward_action'],
                                              reward_action_description=data['reward_action_description'],
                                            reward_points=data['reward_points'])

        return AddObject(new_reward_action)

@rewards_namespace.route('/details')
class RewardDetails(Resource):
    # @customer_followings_namespace.marshal_with(get_customer_followers_followings_api_model)
    @rewards_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        customer = current_user.customer
        if not customer:
            return error_status(0,"No Customer Found")

        reward_points = get_customer_reward_points(customer)
        current_reward_level, reward_next_level, reward_next_level_points = get_customer_reward_details(reward_points)
        reward_levels = Reward_Type_Model.query.all()
        reward_levels = {r.reward_points: r.reward_description for r in reward_levels}
        reward_actions = Reward_Actions_Model.query.all()
        reward_action_points = {action.reward_action_description:action.reward_points for action in reward_actions}
        del reward_levels[0]
        status_points = {"status_"+v+"_points": k for k, v in reward_levels.items()}
        action_points = {k+"_points": v for k, v in reward_action_points.items()}
        details = {
            "reward_points":reward_points, "reward_current_level":current_reward_level, "reward_next_level":reward_next_level,
            "reward_next_level_points":reward_next_level_points, "status_points":status_points, "action_points":action_points
        }
        return success_status("details",details)

    @rewards_namespace.expect(add_customer_reward_action_api_model)
    @rewards_namespace.doc(security='apikey')
    @token_required_user
    def post(self, current_user):
        customer = current_user.customer
        if not customer:
            return error_status(0, "No Customer Found")

        data = request.get_json()
        reward_action = Reward_Actions_Model.query.filter_by(public_id=data['reward_action_id']).first()
        new_c_r_action = Customer_Reward_Action_Model(customer_id=customer.id,reward_action_id=reward_action.id)

        return AddObject(new_c_r_action)