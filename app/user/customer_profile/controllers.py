from flask import request

from app import db
from app.restaurant.events.controllers import get_one_event
from app.restaurant.events.models import Event_Model
from app.user.addresses.controllers import getUserAddress, getAddressToString
from app.user.customer_profile.models import Customer_Food_Album_Model, Food_Album_Restaurant_Model, \
    Reward_Actions_Model, Reward_Type_Model, Customer_Profile_Model, Customer_Favourites_Model, User_Followings_Model
from app.user.restaurant_profile.controllers import getRestaurant
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.utils import success_status, error_status


def get_customer_profile(customer):

    customer_data = dict()
    customer_data['public_id'] = customer.public_id
    customer_data['name'] = customer.name
    customer_data['username'] = customer.username
    customer_data['addresses'] = getUserAddress(customer.addresses_child)
    customer_data['image_url'] = customer.image_url
    customer_data['about'] = customer.about

    return success_status("customers", customer_data)

def get_one_customer_profile(customer):

    customer_data = {}
    customer_data['public_id'] = customer.public_id
    customer_data['name'] = customer.name
    customer_data['username'] = customer.username
    customer_data['addresses'] = getUserAddress(customer.addresses_child)
    customer_data['image_url'] = customer.image_url
    customer_data['about'] = customer.about

    return customer_data

def get_one_customer_profile_login(customer):

    customer_data = {}
    customer_data['public_id'] = customer.public_id
    customer_data['name'] = customer.name
    customer_data['username'] = customer.username
    customer_data['addresses'] = getAddressToString(customer.addresses_child)
    customer_data['image_url'] = customer.image_url
    customer_data['about'] = customer.about

    return customer_data


def get_all_customer_profiles(customers):

    output =[]
    for customer in customers:
        customer_data = {}
        customer_data['public_id'] = customer.public_id
        customer_data['name'] = customer.name
        customer_data['username'] = customer.username
        customer_data['addresses'] = getUserAddress(customer.addresses_child)
        customer_data['image_url'] = customer.image_url
        customer_data['about'] = customer.about
        output.append(customer_data)

    return success_status("customers", output)


def get_customer_data(customer):
    
    customer_data = dict()
    customer_data['public_id'] = customer.public_id
    customer_data['name'] = customer.name
    customer_data['username'] = customer.username
    customer_data['image_url'] = customer.image_url
    customer_data['about'] = customer.about

    return customer_data


def update_customer(customer):
    data = request.get_json()
    customer.name = data['name']
    customer.username = data['username']
    customer.address = data['address']
    customer.image_url = data['image_url']
    customer.about = data['about']

    try:
        db.session.commit()
    except Exception as e:
        return error_status(0, str(e))

    return success_status()


#############################################################################################


def getAlbumsOnly(food_albums):
    output = list()
    for album in food_albums:
        data = dict()
        data['public_id'] = album.public_id
        data['title'] = album.title
        data['description'] = album.description
        data['is_private'] = album.type
        data['image_url'] = album.image_url
        data['restaurants']=[]
        albums = Food_Album_Restaurant_Model.query.filter_by(album=album).all()
        for r in albums:
            data['restaurants'].append(getRestaurant(r.restaurant))

        output.append(data)

    return success_status('food_albums', output)

def getAllAlbums(food_albums):
    output=[]
    for album in food_albums:
        data={}
        data['public_id'] = album.public_id
        data['title'] = album.title
        data['description'] = album.description
        data['is_private'] = album.type
        data['image_url'] = album.image_url
        data['restaurants']=[]
        albums = Food_Album_Restaurant_Model.query.filter_by(album=album).all()
        for r in albums:
            data['restaurants'].append(getRestaurant(r.restaurant))

        data['owner'] = get_one_customer_profile(album.customer)

        output.append(data)

    return success_status('food_albums',output)


def updateFoodAlbum(album_id,title,description,is_private,image_url):

    album = Customer_Food_Album_Model.query.filter_by(public_id = album_id).first()
    album.title = title
    album.description = description
    album.type =is_private
    album.image_url = image_url


    try:
        db.session.commit()
    except Exception as e:
        return error_status(0, str(e))

    return success_status()


def getFoodAlbum(album):
    data = dict()
    data['public_id'] = album.public_id
    data['title'] = album.title
    data['description'] = album.description
    data['is_private'] = album.type
    data['image_url'] = album.image_url
    data['restaurants'] = []
    restaurants = Food_Album_Restaurant_Model.query.filter_by(album=album).all()
    for r in restaurants:
        data['restaurants'].append(getRestaurant(r))

    data['owner'] = get_one_customer_profile(album.customer)

    return data


def get_one_food_album_details(food_album,customer):
    food_albums_data = getFoodAlbum(food_album)
    restaurants_count = len(food_albums_data['restaurants'])
    owner_obj = Customer_Profile_Model.query.filter_by(public_id=food_albums_data['owner']['public_id']).first()

    is_bookmark = False
    bookmarked = Customer_Favourites_Model.query.filter_by(type=0, reference_type=2, customer_id=customer.id).first()
    if bookmarked:
        is_bookmark = True
    bookmarks_count = Customer_Favourites_Model.query.filter_by(reference_id=food_album.public_id, type=0,
                                                                reference_type=2).count()
    owner_followers_count = User_Followings_Model.query.filter_by(following=owner_obj).count()
    is_following_owner = False
    following = User_Followings_Model.query.filter_by(following=owner_obj, follower=customer).first()
    if following:
        is_following_owner = True
    details = {
        'album': food_albums_data, 'restaurants_count': restaurants_count, 'bookmarks_count': bookmarks_count,
        'owner_followers_count': owner_followers_count, 'is_bookmark': is_bookmark,
        'is_following_owner': is_following_owner
    }
    return success_status("details", details)
###########################################################################################3

def getAllFavRestaurants(favs):

    output=[]
    for f in favs:
        r=Restaurant_Profile_Model.query.filter_by(public_id= f.reference_id).first()
        output.append(getRestaurant(r))

    return success_status("restaurant", output)


def getAllFavEvents(favs):
    output = []
    for f in favs:
        r = Event_Model.query.filter_by(public_id=f.reference_id).first()
        favorite = True
        output.append(get_one_event(r,favorite))

    return success_status("events", output)

def getAllFavFoodAlbums(favs):
    output = []
    for f in favs:
        r = Customer_Food_Album_Model.query.filter_by(public_id=f.reference_id).first()
        output.append(getFoodAlbum(r))

    return success_status("restaurant", output)

######################################Recent Activity #######################################


def get_activity_data(activity_type, activity_id, activity):
    activity_details = dict()

    if activity_type == 0:
        restaurant = Restaurant_Profile_Model.query.filter_by(
            public_id=activity_id).first()

        activity_details['title'] = restaurant.name
        activity_details['image_url'] = restaurant.image_url
        activity_details['public_id'] = restaurant.public_id
        activity_details['activity_type'] = activity
        activity_details['type'] = activity_type
    elif activity_type == 1:
        event = Event_Model.query.filter_by(
            public_id=activity_id).first()
        activity_details['title'] = event.name
        activity_details['image_url'] = event.image_url
        activity_details['public_id'] = event.public_id
        activity_details['activity_type'] = activity
        activity_details['type'] = activity_type
    elif activity_type == 2:
        food_album = Customer_Food_Album_Model.query.filter_by(
            public_id=activity_id).first()
        activity_details['title'] = food_album.Title
        activity_details['image_url'] = food_album.image_url
        activity_details['public_id'] = food_album.public_id
        activity_details['activity_type'] = activity
        activity_details['type'] = activity_type

    return activity_details


def get_recent_activities(response_data,user_data):

    recent_activities = list()
    for user in response_data:
        recent_activity = dict()
        for f_u in user_data:
            if user['user_id'] == f_u.public_id:
                recent_activity['name'] = f_u.name
                recent_activity['image_url'] = f_u.image_url
                recent_activity['public_id'] = f_u.public_id
                recent_activity['activity'] = get_activity_data(user['type'],user['public_id'],user['activity'])

        recent_activities.append(recent_activity)
    return success_status("recent_activity", recent_activities)


def get_user_activities_data(response_data):

    recent_activities = list()
    for activity in response_data:
        recent_activity = get_activity_data(activity['type'],activity['public_id'],activity['activity'])
        recent_activities.append(recent_activity)

    return recent_activities


def get_customer_reward_points(customer):
    reward_points_ids = [reward.reward_action_id for reward in customer.customer_rewards]
    reward_points_objects = Reward_Actions_Model.query.filter(Reward_Actions_Model.id.in_(reward_points_ids)).all()
    reward_points = sum([points.reward_points for points in reward_points_objects])
    return reward_points


def get_customer_reward_details(reward_points):
    reward_levels = Reward_Type_Model.query.all()
    reward_levels = {r.reward_points: r.reward_description for r in reward_levels}
    reward_level_points = list(reward_levels.keys())
    reward_level_points.sort()
    points = None
    reward_next_level_points = None
    for i in range(len(reward_level_points)):
        if (reward_points > reward_level_points[i]) and (reward_points < reward_level_points[i + 1]):
            points = reward_level_points[i]
            try:
                reward_next_level_points = reward_level_points[i + 1]
            except:
                reward_next_level_points = reward_level_points[i]
            break
    current_reward_level = reward_levels[points]
    reward_next_level = reward_levels[reward_next_level_points]

    return current_reward_level, reward_next_level, reward_next_level_points



def check_restaurant_bookmarked(current_user = None):


    return False
