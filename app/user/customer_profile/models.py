from sqlalchemy.orm import backref, relationship
from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.users_auth.models import User_Model


class Customer_Profile_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "customer_profile"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    public_id = db.Column(db.String(100))
    username = db.Column(db.String(100), unique=True)
    image_url = db.Column(db.String(200))
    about = db.Column(db.String(500))
    user_auth_id = db.Column(db.Integer,db.ForeignKey("user_auth.id"),unique=True)

    addresses_child = db.relationship('Addresses_Model', backref='customer_addresses', cascade="all,delete")
    payments_child = db.relationship('Customer_Payment_Model', backref='customer_payment_methods', cascade="all,delete")
    user = relationship(User_Model, backref=backref("users", cascade="all,delete"))
    reviews_child = db.relationship('Reviews_Model', backref='customer', cascade="all,delete")


class Customer_Favourites_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "customer_favourites"
    id = db.Column(db.Integer, primary_key=True)

    public_id = db.Column(db.String())
    reference_id = db.Column(db.String())
    reference_type = db.Column(db.Integer) # 0 - Restaurant , 1 - Event , 2 - Album
    type = db.Column(db.Integer) #0 - bookmark , 1- favourite

    customer_id = db.Column(db.Integer,db.ForeignKey("customer_profile.id"))
    customer = relationship(Customer_Profile_Model,backref=backref("album_customers", cascade="all,delete"))


############################### Food Album #########################################
class Customer_Food_Album_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "customer_food_album"
    id = db.Column(db.Integer, primary_key=True)

    public_id = db.Column(db.String())
    title = db.Column(db.String())
    description = db.Column(db.String())
    type = db.Column(db.Integer)  # 1 - private , 0 - public
    image_url = db.Column(db.String())

    customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    customer = relationship(Customer_Profile_Model, backref=backref("customer_albums", cascade="all,delete"))


class Food_Album_Restaurant_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "restaurant_food_album"
    id = db.Column(db.Integer, primary_key=True)

    album_id = db.Column(db.Integer, db.ForeignKey("customer_food_album.id"))
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant_profile.id"))

    album = relationship(Customer_Food_Album_Model, backref=backref("customer_food_album", cascade="all,delete"))
    restaurant = relationship(Restaurant_Profile_Model, backref=backref("restaurant_albums", cascade="all,delete"))


################################ Followers/Following ################################
class User_Followings_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "user_followings"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String())
    following_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    follower_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    follower = relationship(Customer_Profile_Model, foreign_keys=[follower_id],
                            backref=backref("followers", cascade="all,delete"))
    following = relationship(Customer_Profile_Model, foreign_keys=[following_id],
                             backref=backref("followings",  cascade="all,delete"))

#########################################################3

class Reward_Type_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "reward_type"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String)
    reward_type = db.Column(db.Integer)
    reward_description = db.Column(db.String)
    reward_points = db.Column(db.Integer)


class Reward_Actions_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "reward_action"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String)
    reward_action = db.Column(db.Integer)
    reward_action_description = db.Column(db.String)
    reward_points = db.Column(db.Integer)


class Customer_Reward_Action_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):
    __tablename__ = "customer_reward_action"
    id = db.Column(db.Integer, primary_key=True)

    customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    reward_action_id = db.Column(db.Integer, db.ForeignKey("reward_action.id"))

    customer = relationship(Customer_Profile_Model, backref=backref("customer_rewards", cascade="all,delete"))
    reward_action = relationship(Reward_Actions_Model, backref=backref("customer_reward_action", cascade="all,delete"))



