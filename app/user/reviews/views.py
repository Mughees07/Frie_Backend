import uuid

from app import constants
from app.user.reviews.models import Reviews_Model
from app.user.reviews.serializers import get_all_reviews_api_model, add_review_api_model, update_review_api_model,\
    get_one_review_api_model
from flask import request
from flask_restplus import Resource

from app.user import rest_api_user
from app.restaurant.events.models import Event_Model
from app.user.restaurant_profile.models import Restaurant_Profile_Model
from app.user.reviews.controllers import *
from app.user.users_auth.decorators import token_required_user, token_required
from app.utils import AddObject, error_status, success_status, DeleteObject, commit_changes

reviews_namespace = rest_api_user.namespace('reviews', description='Operations related to Reviews')


@reviews_namespace.route('/')
class CustomerReviews(Resource):
    @rest_api_user.marshal_with(get_all_reviews_api_model)
    @reviews_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):

        if not current_user.customer:
            return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0],constants.ERROR.CUSTOMER_NOT_EXISTS[1])

        reviews = Reviews_Model.query.filter_by(customer_id=current_user.customer.id).all()

        return get_all_reviews(reviews)


@reviews_namespace.route('/review_images')
class CustomerReviewImages(Resource):
    @reviews_namespace.doc(security='apikey')
    @token_required_user
    def get(self, current_user):
        reviews = Reviews_Model.query.filter_by(customer_id=current_user.customer.id).all()
        if not reviews:
            return error_status(constants.ERROR.REVIEW_NOT_EXISTS[0], constants.ERROR.REVIEW_NOT_EXISTS[1])

        return get_reviews_images(reviews)

@reviews_namespace.route('/events/<string:id>')
class Reviews_Event(Resource):
    @rest_api_user.marshal_with(get_all_reviews_api_model)
    def get(self,id):
        """
        return the Reviews associated with the given public id of an event
        """
        event = Event_Model.query.filter_by(public_id=id).first()
        if not event:
            return error_status(constants.ERROR.EVENT_NOT_EXISTS[0],constants.ERROR.EVENT_NOT_EXISTS[1])

        reviews = Reviews_Model.query.filter_by(event_id=event.id).all()

        return get_all_reviews(reviews)


    @reviews_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_review_api_model)
    def post(self, id, current_user):
        """
        Given image_url, comment and rating, this method creates a new review for given event id
        """
        data = request.get_json()
        event = Event_Model.query.filter_by(public_id=id).first()
        if not event:
            return error_status(constants.ERROR.EVENT_NOT_EXISTS[0], constants.ERROR.EVENT_NOT_EXISTS[1])

        new_review = Reviews_Model(public_id=str(uuid.uuid4()), image_url=data['image_url'], comment=data['comment'],
                                          rating=data['rating'],customer=current_user.customer,event_id=event.id)

        return AddObject(new_review)


@reviews_namespace.route('/restaurants/<string:id>')
class Reviews_Restaurant(Resource):
    @rest_api_user.marshal_with(get_all_reviews_api_model)
    def get(self, id):
        """
        return the Reviews associated with the given public id of a restaurant
        """
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
        if not restaurant:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0],constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        reviews = Reviews_Model.query.filter_by(restaurant_id=restaurant.id).all()

        return get_all_reviews(reviews)

    @reviews_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_review_api_model)
    def post(self, id, current_user):
        """
        Given image_url, comment and rating, this method creates a new review for given restaurant id
        """
        data = request.get_json()
        restaurant = Restaurant_Profile_Model.query.filter_by(public_id=id).first()
        if not restaurant:
            return error_status(constants.ERROR.RESTAURANT_NOT_EXISTS[0], constants.ERROR.RESTAURANT_NOT_EXISTS[1])

        new_review = Reviews_Model(public_id=str(uuid.uuid4()), image_url=data['image_url'],comment=data['comment'],
                                          rating=data['rating'],customer_id=current_user.customer.id,
                                   restaurant_id=restaurant.id)

        return AddObject(new_review)


@reviews_namespace.route('/<string:id>')
class ReviewsOneItem(Resource):
    @rest_api_user.marshal_with(get_one_review_api_model)
    def get(self, id):
        """
        Given public_id of the review this method returns the review
        """
        review = Reviews_Model.query.filter_by(public_id=id).first()
        if not review:
            return error_status(constants.ERROR.REVIEW_NOT_EXISTS[0], constants.ERROR.REVIEW_NOT_EXISTS[1])

        return get_review(review)

    @rest_api_user.expect(update_review_api_model)
    def put(self, id):
        """
        Given image_url, comment and rating, this method updates the review
        """
        review = Reviews_Model.query.filter_by(public_id=id).first()

        if not review:
            return error_status(constants.ERROR.REVIEW_NOT_EXISTS[0], constants.ERROR.REVIEW_NOT_EXISTS[1])

        data = request.get_json()

        review.image_url = data['image_url']
        review.comment = data['comment']
        review.rating = data['rating']

        #db.session.commit()
        return commit_changes()

    def delete(self, id):
        """
        Given public_id of the review this method deletes the review
        """
        review = Reviews_Model.query.filter_by(public_id=id).first()
        if not review:
            return error_status(constants.ERROR.REVIEW_NOT_EXISTS[0], constants.ERROR.REVIEW_NOT_EXISTS[1])

        return DeleteObject(review)

@reviews_namespace.route('/spam/<string:id>')
class MarkSpam(Resource):
    @reviews_namespace.doc(security='apikey')
    @token_required
    def get(self, id):
        """
        Given public_id of the review this method returns the review
        """
        review = Reviews_Model.query.filter_by(public_id=id).first()
        if not review:
            return error_status(constants.ERROR.REVIEW_NOT_EXISTS[0], constants.ERROR.REVIEW_NOT_EXISTS[1])

        review.spammed = True

        return commit_changes()


@reviews_namespace.route('/report/<string:id>')
class ReportReview(Resource):
    @reviews_namespace.doc(security='apikey')
    @token_required
    def put(self, id):
        """
        Given public_id of the review this method returns the review
        """
        review = Reviews_Model.query.filter_by(public_id=id).first()

        if not review:
            return error_status(constants.ERROR.REVIEW_NOT_EXISTS[0], constants.ERROR.REVIEW_NOT_EXISTS[1])

        review.reported = True

        return commit_changes()
