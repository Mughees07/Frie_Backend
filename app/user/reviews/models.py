from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp


class Reviews_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "reviews"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    image_url = db.Column(db.String(200))
    comment = db.Column(db.String(200))
    rating = db.Column(db.Float)
    is_fake = db.Column(db.Boolean, default=False)
    spammed = db.Column(db.Boolean, default=False)
    reported = db.Column(db.Boolean, default=False)
    customer_id = db.Column(db.Integer, db.ForeignKey("customer_profile.id"))
    restaurant_id = db.Column(db.Integer, db.ForeignKey("restaurant_profile.id"), nullable=True)
    event_id = db.Column(db.Integer, db.ForeignKey("events.id"), nullable=True)

