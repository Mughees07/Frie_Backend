from functools import wraps
from flask import request, jsonify
import jwt
from app.user.users_auth.models import User_Model
from app import app


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']

        if not token:
            return jsonify({'message' : 'Token is missing!'})

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User_Model.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message' : 'Token is invalid!'})
        print(current_user.email)
        return f(*args, **kwargs, current_user=current_user)

    return decorated