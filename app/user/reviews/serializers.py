from flask_restplus import fields
from app.user import rest_api_user

pagination = rest_api_user.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

review_api_model = rest_api_user.model('get review', {
    'public_id': fields.String(),
    'image_url': fields.String(),
    'comment': fields.String(),
    'rating': fields.Float(),
    'time_stamp': fields.DateTime(),
    'user_public_id': fields.String(),
    'user_full_name': fields.String(),
    'user_image_url': fields.String()

})



get_all_reviews_api_model = rest_api_user.model('List of all reviews', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'reviews': fields.List(fields.Nested(review_api_model))
})

get_one_review_api_model = rest_api_user.model('get one review', {
    'status': fields.Integer(description='Status of API'),
    'message': fields.String(),
    'review': fields.Nested(review_api_model)
})

add_review_api_model = rest_api_user.model('add review', {
    'image_url': fields.String(required=True),
    'comment': fields.String(required=True),
    'rating': fields.Float(required=True)
})

update_review_api_model = rest_api_user.model('update review', {
    'image_url': fields.String(required=True),
    'comment': fields.String(required=True),
    'rating': fields.Float(required=True)
})
