from flask import jsonify

from app import db
from app.user.reviews.models import Reviews_Model

from app.utils import success_status


def get_review(review):

    review_data = dict()
    review_data['public_id'] = review.public_id
    review_data['image_url'] = review.image_url
    review_data['comment'] = review.comment
    review_data['rating'] = review.rating
    review_data['time_stamp'] = review.created_at
    review_data['user_public_id'] = review.customer.public_id
    review_data['user_image_url'] = review.customer.image_url
    review_data['user_full_name'] = review.customer.name

    return success_status("review", review_data)


def get_all_reviews(reviews):
    output = []
    for review in reviews:
        review_data = dict()
        review_data['public_id'] = review.public_id
        review_data['image_url'] = review.image_url
        review_data['comment'] = review.comment
        review_data['rating'] = review.rating
        review_data['time_stamp'] = review.created_at
        review_data['user_public_id'] = review.customer.public_id
        review_data['user_image_url'] = review.customer.image_url
        review_data['user_full_name'] = review.customer.name
        output.append(review_data)

    return success_status("reviews", output)

def get_restaurant_all_reviews(restaurant):
    reviews = Reviews_Model.query.filter_by(restaurant_id=restaurant.id).all()
    output = []
    for review in reviews:
        review_data = dict()
        review_data['public_id'] = review.public_id
        review_data['image_url'] = review.image_url
        review_data['comment'] = review.comment
        review_data['rating'] = review.rating
        review_data['time_stamp'] = review.created_at
        review_data['user_public_id'] = review.customer.public_id
        review_data['user_image_url'] = review.customer.image_url
        review_data['user_full_name'] = review.customer.name
        output.append(review_data)

    return output

def get_reviews_images(reviews):
    output = []
    for review in reviews:
        review_data = dict()
        review_data['public_id'] = review.public_id
        review_data['image_url'] = review.image_url
        output.append(review_data)

    return success_status("reviews", output)


def getRestaurantReviewCount(r):
    reviews = Reviews_Model.query.filter_by(restaurant_id=r.id).all()
    return  len(reviews)

