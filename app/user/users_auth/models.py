from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp

class User_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "user_auth"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(100), unique=True)
    email = db.Column(db.String(100),unique=True, nullable=True)
    password = db.Column(db.String(200), nullable=True)
    social_key = db.Column(db.String(200), nullable=True)
    social_value = db.Column(db.String(200), nullable=True)
    type = db.Column(db.Integer)
    verified = db.Column(db.Boolean,default=True)
    fcm_token = db.Column(db.String() , unique= True , nullable=True)
    customer = db.relationship('Customer_Profile_Model', backref='customer', uselist=False)
    restaurant = db.relationship('Restaurant_Profile_Model', backref='restaurant', uselist=False)
    driver = db.relationship('Driver_Profile_Model', backref='driver', uselist=False)