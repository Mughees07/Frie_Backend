from flask_restplus import fields
from app.user import rest_api_user


pagination = rest_api_user.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

get_user = rest_api_user.model('get user', {
    'email': fields.String(),
    'password': fields.String(),
    'social_key': fields.String(),
    'social_value': fields.String(),
    'type': fields.Integer(),
    'public_id': fields.String(),
    'verified': fields.Boolean(),
    'fcm_token': fields.String()
})

get_all_user = rest_api_user.model('List of all users', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'users': fields.List(fields.Nested(get_user))
})

add_user = rest_api_user.model('add user', {
    'email': fields.String,
    'password': fields.String,
    'social_key': fields.String,
    'social_value': fields.String,
    'type': fields.Integer
})

update_user = rest_api_user.model('update user', {
    'email': fields.String(required=True),
    'password': fields.String(required=True)
})

login_user = rest_api_user.model('login_model', {
    'email': fields.String(required=True),
    'password': fields.String(required=True)
})

public_id_model = rest_api_user.model('public_id_model', {
    'public_id': fields.String(required=True)
})

forgot_password = rest_api_user.model('forgot_password', {
    'email': fields.String(required=True)
})