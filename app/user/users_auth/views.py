import uuid
import datetime

import jwt
from flask import request, jsonify, make_response
from flask_restplus import Resource
from werkzeug.security import generate_password_hash,check_password_hash

from app import db, app, constants
from app.user.customer_profile.controllers import get_one_customer_profile, get_one_customer_profile_login
from app.user.driver_profile.controllers import get_one_driver_profile
from app.user.restaurant_profile.controllers import getRestaurant
from app.user.users_auth.constants import TOKEN_EXPIRY_TIME
from app.user import rest_api_user
from app.user.users_auth.models import User_Model
from app.user.users_auth.serializers import add_user, update_user,public_id_model, forgot_password,get_all_user
from app.user.users_auth.decorators import token_required, token_required_user
from app.utils import send_email, success_status, AddObject, DeleteObject, error_status, commit_changes
from app.user.users_auth.controllers import get_all_users

user_auth_namespace = rest_api_user.namespace('user_authentication', description='Operations related to Users Authentication')
from sqlalchemy import exc

@user_auth_namespace.route('/')
class User_Auth(Resource):
    @rest_api_user.marshal_with(get_all_user)
    def get(self):
        """
        Returns all the users stored in DB
        """
        users = User_Model.query.all()

        return get_all_users(users)


    @rest_api_user.expect(add_user)
    def post(self):
        """
        Given Email and Password, this method creates a new user and return access token
        """
        data = request.get_json()
        print(data)
        keys = data.keys()
        if 'email' in keys and 'password' in keys:
            user = User_Model.query.filter_by(email=data['email']).first()

            if user:
                return jsonify({"status": 0, 'message': 'Email Already Exists!'})

            hashed_password = generate_password_hash(data['password'], method='sha256')
            new_user = User_Model(public_id=str(uuid.uuid4()), email=data['email'], password=hashed_password, type=data['type'])

        elif 'social_key' in keys and 'social_value' in keys:

            new_user = User_Model(public_id=str(uuid.uuid4()), social_key=data['social_key'], social_value=data['social_value'],
                                  type=data['type'])

        else:
            return error_status(0,"")

        db.session.add(new_user)
        db.session.commit()

        token = jwt.encode({'public_id': new_user.public_id,
                            'exp': datetime.datetime.utcnow() + datetime.timedelta(minutes=TOKEN_EXPIRY_TIME)},
                           app.config['SECRET_KEY'])

        return success_status('token', token.decode('UTF-8'))

    @rest_api_user.expect(update_user)
    @user_auth_namespace.doc(security='apikey')
    @token_required_user
    def put(self,current_user):
        """
        Given Email and Password, this method updates the email and password for the current user,
        authentication token is required
        """
        data = request.get_json()
        if not current_user:
            return jsonify({"status": 0, 'message' : 'No user found!'})

        email_exist = User_Model.query.filter_by(email=data['email']).first()

        if email_exist:
            return jsonify({"status": 0, 'message': 'Email Already Exists!'})

        hashed_password = generate_password_hash(data['password'], method='sha256')
        current_user.email = data['email']
        current_user.password = hashed_password
        db.session.commit()
        return success_status()

@user_auth_namespace.route('/<string:id>')
class User_Auth_One_Item(Resource):

    def delete(self,id):
        """
        Given the public_id this function, deletes the user associated with that id
        """
        user = User_Model.query.filter_by(public_id=id).first()
        return DeleteObject(user)

@user_auth_namespace.route('update_fcm_token/<string:id>')
class Update_FCM_Token(Resource):

    @user_auth_namespace.doc(security='apikey', params={'id': 'FCM Token'})
    @token_required_user
    def put(self,current_user,id):
        """
        Given Email and Password, this method updates the email and password for the current user,
        authentication token is required
        """

        if not current_user:
            return jsonify({"status": 0, 'message': 'No user found!'})

        current_user.fcm_token = id

        return commit_changes()


def success_status_login(current_user,token):

    status = {'status': 1, "message": "success"}
    status['token']=token.decode('UTF-8')
    if(current_user.customer):
        status['user'] = get_one_customer_profile_login(current_user.customer)
    elif(current_user.restaurant):
        status['user'] = getRestaurant(current_user.restaurant)
    elif(current_user.driver):
        status['user'] = get_one_driver_profile(current_user.driver)
    else:
        status['user'] = []
        return status
        # return error_status(constants.ERROR.CUSTOMER_NOT_EXISTS[0], constants.ERROR.CUSTOMER_NOT_EXISTS[1])
    return status


@user_auth_namespace.route('/login')
@user_auth_namespace.response(200, 'Success')
@user_auth_namespace.response(401, 'Incorrect username or password')
class Login(Resource):
    """
    Given email and password, this method returns authentication token
    """
    def post(self):
        auth = request.authorization
        if not auth or not auth.username or not auth.password:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

        user = User_Model.query.filter_by(email=auth.username).first()

        if not user:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

        if check_password_hash(user.password, auth.password):
            token = jwt.encode({'public_id' : user.public_id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=TOKEN_EXPIRY_TIME)}, app.config['SECRET_KEY'])
            return success_status_login(user,token)

        return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

@user_auth_namespace.route('/logout')
class Logout(Resource):
    @user_auth_namespace.doc(security='apikey')
    @token_required_user

    def put(self,current_user):
        """
        Logout the user
        """
        current_user.fcm_token = None
        db.session.commit()

        return success_status()


@user_auth_namespace.route('/social_login/')
@user_auth_namespace.response(200, 'Success')
@user_auth_namespace.response(401, 'Incorrect username or password')
class SocialLogin(Resource):
    """
    using social login, this method returns authentication token
    """
    def post(self):
        auth = request.authorization
        if not auth or not auth.username or not auth.password:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})

        user = User_Model.query.filter_by(social_value=auth.password).first()

        if user:
            token = jwt.encode({'public_id' : user.public_id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=TOKEN_EXPIRY_TIME)}, app.config['SECRET_KEY'])
            return success_status('token',token.decode('UTF-8'))
        else:
            return make_response('Could not verify', 401, {'WWW-Authenticate' : 'Basic realm="Login required!"'})


@user_auth_namespace.route('/forgot_password')
@user_auth_namespace.expect(forgot_password)
class ForgotPassword(Resource):
    def post(self):
        """
        Given a valid  email address, this method sends a new password to the email
        """
        data = request.get_json()
        user = User_Model.query.filter_by(email=data['email']).first()

        if not user:
            return error_status(constants.ERROR.USER_NOT_EXISTS[0],constants.ERROR.USER_NOT_EXISTS[1])

        password = str(uuid.uuid4())
        hashed_password = generate_password_hash(password, method='sha256')
        user.password = hashed_password

        send_email(data['email'],password)
        return commit_changes()
