from functools import wraps
from flask import request, jsonify
import jwt
from app.user.users_auth.models import User_Model
from app import app
from app.utils import error_status


def token_required_user(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']

        if not token:
            return error_status(0,'Token is missing!')

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User_Model.query.filter_by(public_id=data['public_id']).first()
        except:
            return error_status(0, 'Token is invalid!')


        return f(*args, **kwargs, current_user=current_user)

    return decorated


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']

        if not token:
            return error_status(0, 'Token is missing!')

        try:
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User_Model.query.filter_by(public_id=data['public_id']).first()
        except:
            return error_status(0, 'Token is invalid!')

        if not current_user:
            return error_status(0, 'Token is invalid!')

        return f(*args, **kwargs)

    return decorated


def token_required_skip_login(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None

        if 'Authorization' in request.headers:
            token = request.headers['Authorization']

        if not token:
            current_user = None
        else :
            data = jwt.decode(token, app.config['SECRET_KEY'])
            current_user = User_Model.query.filter_by(public_id=data['public_id']).first()




        return f(*args, **kwargs , current_user=current_user)

    return decorated
