import uuid

from flask import request
from flask_restplus import Resource

from app import db, constants
from app.user.driver_profile.models import Driver_Profile_Model, Areas_Model, Vehicle_Type_Model
from app.user.driver_profile.serializers import *
from app.user.driver_profile.controllers import get_driver_profile, add_driver, update_driver
from app.user.users_auth.decorators import token_required, token_required_user
from app.utils import success_status, AddObject, DeleteObject, error_status, commit_changes

driver_profile_namespace = rest_api_user.namespace('driver_profile', description='Operations related to Driver Profile')
delivery_areas_namespace = rest_api_user.namespace('delivery_areas', description='Operations related to Delivery Areas')
vehicle_types_namespace = rest_api_user.namespace('vehicle_types', description='Operations related to Vehicle Types')

@driver_profile_namespace.route('/')
class Driver_Profile(Resource):
    @driver_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.marshal_with(get_all_driver_api_model)
    def get(self,current_user):
        """
        return the driver associated with the current user
        """
        driver = current_user.driver

        if not driver:
            return error_status(constants.ERROR.DRIVER_NOT_EXISTS[0],constants.ERROR.DRIVER_NOT_EXISTS[1])
        elif not driver.delivery_areas_child:
            return error_status(constants.ERROR.DRIVER_AREAS_NOT_DEFINED[0], constants.ERROR.DRIVER_AREAS_NOT_DEFINED[1])
        elif not driver.vehicle_types_child:
            return error_status(constants.ERROR.DRIVER_VEHICLES_NOT_DEFINED[0],constants.ERROR.DRIVER_VEHICLES_NOT_DEFINED[1])


        return get_driver_profile(driver, current_user)

    @driver_profile_namespace.doc(security='apikey')
    @token_required_user
    @rest_api_user.expect(add_driver_api_model)
    def post(self,current_user):
        """
        Given First Name, Last Name and image_url, this method creates a new driver
        """

        return add_driver(current_user)

@driver_profile_namespace.route('/<string:id>')
class Driver_Profile_User(Resource):
    @rest_api_user.expect(update_driver_api_model)
    @driver_profile_namespace.doc(security='apikey',params={'id': 'Driver ID'})
    @token_required
    def put(self,id):
        """
        Given First Name, Last Name and image_url, this method updates the driver details,
        authentication token is required
        """
        driver = Driver_Profile_Model.query.filter_by(public_id=id).first()
        if not driver:
            return error_status(constants.ERROR.DRIVER_NOT_EXISTS[0],constants.ERROR.DRIVER_NOT_EXISTS[1])


        return update_driver(driver)

    @driver_profile_namespace.doc(security='apikey',params={'id': 'Driver ID'})
    @token_required
    def delete(self,id):
        """
        Given Public id of the driver, this method deletes the driver profile
        """
        driver = Driver_Profile_Model.query.filter_by(public_id=id).first()
        if not driver:
            return error_status(constants.ERROR.DRIVER_NOT_EXISTS[0],constants.ERROR.DRIVER_NOT_EXISTS[1])

        return DeleteObject(driver)

@driver_profile_namespace.route('/status/')
class DriverOnlineStatus(Resource):
    @driver_profile_namespace.expect(update_driver_status_api_model)
    @driver_profile_namespace.doc(security='apikey')
    @token_required_user
    def put(self,current_user):
        driver = current_user.driver
        if not driver:
            return error_status(constants.ERROR.DRIVER_NOT_EXISTS[0],constants.ERROR.DRIVER_NOT_EXISTS[1])

        data = request.get_json()
        driver.status = data['status']



        return commit_changes()


@delivery_areas_namespace.route('')
class Delivery_Areas(Resource):
    @rest_api_user.marshal_with(get_all_areas_api_model)
    def get(self):
        """
        return all the available delivery areas
        """
        areas = Areas_Model.query.all()
        if len(areas) == 0:
            return error_status(constants.ERROR.AREA_NOT_EXIST[0],constants.ERROR.AREA_NOT_EXIST[1])

        output = []
        for area in areas:
            area_data = {}
            area_data['public_id'] = area.public_id
            area_data['name'] = area.name
            output.append(area_data)

        return success_status("delivery_areas",output)

    @rest_api_user.expect(add_area_api_model)
    def post(self):
        """
        Given the name of Area, this method adds a delivery area
        """
        data = request.get_json()

        new_area = Areas_Model(public_id=str(uuid.uuid4()),name=data['name'])


        return AddObject(new_area)

@delivery_areas_namespace.route('/<string:id>')
class Delivery_Areas_One_Item(Resource):
    @rest_api_user.expect(update_delivery_areas_api_model)
    @delivery_areas_namespace.doc(params={'id': 'Delivery Area ID'})
    def put(self,id):
        """
        Given the public id and name of Area, this method updates the delivery area name
        """
        data = request.get_json()
        area = Areas_Model.query.filter_by(public_id=id).first()
        if not area:
            return error_status(constants.ERROR.AREA_NOT_EXIST[0],constants.ERROR.AREA_NOT_EXIST[1])
        area.name = data['name']


        return commit_changes()

    @delivery_areas_namespace.doc(params={'id': 'Delivery Area ID'})
    def delete(self,id):
        """
        Given the public_id of Area, this method deletes the delivery area
        """
        area = Areas_Model.query.filter_by(public_id=id).first()
        if not area:
            return error_status(constants.ERROR.AREA_NOT_EXIST[0],constants.ERROR.AREA_NOT_EXIST[1])

        return DeleteObject(area)



@vehicle_types_namespace.route('')
class Vehicle_Types(Resource):
    @rest_api_user.marshal_with(get_all_vehicle_types_api_model)
    def get(self):
        """
        return all the available vehicle types
        """
        vehicles = Vehicle_Type_Model.query.all()

        if len(vehicles) == 0:
            return error_status(constants.ERROR.VEHICLE_NOT_EXIST[0],constants.ERROR.VEHICLE_NOT_EXIST[1])

        output = []
        for vehicle in vehicles:
            vehicle_data = {}
            vehicle_data['public_id'] = vehicle.public_id
            vehicle_data['name'] = vehicle.name
            output.append(vehicle_data)

        return success_status("vehicles",output)

    @rest_api_user.expect(add_vehicle_api_model)
    def post(self):
        """
        Add a new vehicle type
        """
        data = request.get_json()
        new_vehicle = Vehicle_Type_Model(public_id=str(uuid.uuid4()),name=data['name'])

        return AddObject(new_vehicle)

@vehicle_types_namespace.route('/<string:id>')
class Vehicle_Types_One_Item(Resource):
    @rest_api_user.expect(update_delivery_areas_api_model)
    @driver_profile_namespace.doc(params={'id': 'Vehicle Type ID'})
    def put(self,id):
        """
        Given the public id and name of Vehicle Type, this method updates the Vehicle Type name
        """
        data = request.get_json()
        vehicle = Vehicle_Type_Model.query.filter_by(public_id=id).first()
        if not vehicle:
            return error_status(constants.ERROR.VEHICLE_NOT_EXIST[0],constants.ERROR.VEHICLE_NOT_EXIST[1])

        vehicle.name = data['name']


        return commit_changes()

    @driver_profile_namespace.doc(params={'id': 'Vehicle Type ID'})
    def delete(self,id):
        """
        Given the public_id of Vehicle Type, this method deletes the Vehicle Type
        """
        vehicle = Vehicle_Type_Model.query.filter_by(public_id=id).first()
        if not vehicle:
            return error_status(constants.ERROR.VEHICLE_NOT_EXIST[0],constants.ERROR.VEHICLE_NOT_EXIST[1])

        return DeleteObject(vehicle)


