from flask_restplus import fields
from app.user import rest_api_user


pagination = rest_api_user.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

get_areas_api_model = rest_api_user.model('get driver areas', {
    'public_id': fields.String(),
    'name': fields.String()
})

get_vehicles_api_model = rest_api_user.model('get driver vehicles', {
    'public_id': fields.String(),
    'name': fields.String()
})

add_driver_vehicle_api_model = rest_api_user.model('add driver vehicle', {
    'public_id': fields.String(required=True)
})

add_driver_area_api_model = rest_api_user.model('add driver area', {
    'public_id': fields.String(required=True)
})


get_driver_api_model = rest_api_user.model('get driver', {
    'public_id': fields.String(),
    'first_name': fields.String(default="First Name"),
    'last_name': fields.String(default="Last Name"),
    'email': fields.String(default="mail@datics.ai"),
    'status': fields.Integer(default=0),
    'ref_code': fields.String(default="12321"),
    'image_url': fields.String(default="mail@datics.ai"),
    'image_verified': fields.Boolean(default=False),
    'background_check': fields.Boolean(default=False),
    'completed_orders': fields.Integer(default=False),
    'total_earnings': fields.Float(default=0),
    'delivery_areas':  fields.List(fields.Nested(get_areas_api_model),default=[]),
    'vehicles':  fields.List(fields.Nested(get_vehicles_api_model),default=[])
})

get_all_driver_api_model = rest_api_user.model('List of all drivers', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'drivers': fields.List(fields.Nested(get_driver_api_model),default=[])
})

add_driver_api_model = rest_api_user.model('add driver', {
    'first_name': fields.String(required=True),
    'last_name': fields.String(required=True),
    'image_url': fields.String(required=True),
    'vehicles': fields.List(fields.Nested(add_driver_vehicle_api_model)),
    'delivery_areas': fields.List(fields.Nested(add_driver_area_api_model))
})

update_driver_api_model = rest_api_user.model('update driver', {
    'first_name': fields.String(required=True),
    'last_name': fields.String(required=True),
    'image_url': fields.String(required=True),
    'vehicles': fields.List(fields.Nested(add_driver_vehicle_api_model)),
    'delivery_areas': fields.List(fields.Nested(add_driver_area_api_model))
})

update_driver_status_api_model = rest_api_user.model('update driver status', {
    'status': fields.Integer(required=True)
})

get_all_areas_api_model = rest_api_user.model('List of all areas for a driver', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'delivery_areas': fields.List(fields.Nested(get_areas_api_model),default=[])
})

add_area_api_model = rest_api_user.model('add area', {
    'name': fields.String(required=True)
})

update_delivery_areas_api_model = rest_api_user.model('update delivery area', {
    'name': fields.String(required=True)
})
public_id_api_model = rest_api_user.model('public_id', {
    'public_id': fields.String(required=True)
})

add_multiple_driver_delivery_areas_api_model = rest_api_user.model('add multiple driver delivery areas', {
    'areas': fields.List(fields.Nested(public_id_api_model))
})

add_multiple_vehicle_types_api_model = rest_api_user.model('add multiple vehicle types areas', {
    'vehicles': fields.List(fields.Nested(public_id_api_model))
})

get_all_vehicle_types_api_model = rest_api_user.model('List of all Vehicle Types for Driver', {
    'status' : fields.Integer(description='Status of API'),
    'message' : fields.String(),
    'vehicles': fields.List(fields.Nested(get_areas_api_model),default=[])
})

add_vehicle_api_model = rest_api_user.model('add vehicle', {
    'name': fields.String(required=True)
})

add_multiple_driver_vehicle_types_api_model = rest_api_user.model('add multiple driver vehicle types', {
    'vehicles': fields.List(fields.Nested(public_id_api_model))
})

