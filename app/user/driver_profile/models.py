from app import db
from app.models import Created_TimeStamp, Updated_TimeStamp

delivery_areas = db.Table('delivery_areas',
                          db.Column('driver_id',db.Integer,db.ForeignKey('driver_profile.id')),
                          db.Column('areas_id',db.Integer,db.ForeignKey('areas.id')))

driver_vehicles = db.Table('driver_vehicles',
                          db.Column('driver_id',db.Integer,db.ForeignKey('driver_profile.id')),
                          db.Column('vehicle_types_id',db.Integer,db.ForeignKey('vehicle_types.id')))

class Areas_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "areas"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    name = db.Column(db.String(200))

class Vehicle_Type_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "vehicle_types"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    name = db.Column(db.String(200))

class Driver_Profile_Model(db.Model, Created_TimeStamp, Updated_TimeStamp):

    __tablename__ = "driver_profile"
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.String(200))
    first_name = db.Column(db.String(100))
    last_name = db.Column(db.String(100))
    ref_code = db.Column(db.String(100))
    status = db.Column(db.Integer, default=0)
    image_url = db.Column(db.String(200))
    image_verified = db.Column(db.Boolean,default=False)
    background_check = db.Column(db.Boolean,default=False)
    user_auth_id = db.Column(db.Integer, db.ForeignKey("user_auth.id"), unique=True)
    delivery_areas_child = db.relationship('Areas_Model', secondary=delivery_areas,backref=db.backref('areas_driver'
                                                                                                      ,lazy='dynamic',
                                                                                                      cascade="all,delete"))
    vehicle_types_child = db.relationship('Vehicle_Type_Model', secondary=driver_vehicles,
                                          backref=db.backref('driver_vehicle',lazy='dynamic',cascade="all,delete"))

    trips_child = db.relationship('Trips_Model', backref='trip_driver',lazy='dynamic')





