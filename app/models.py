from app import db
from sqlalchemy import func

class Created_TimeStamp(object):
    created_at = db.Column(db.DateTime(timezone=True), nullable=False, server_default=func.now())

class Updated_TimeStamp(object):
    last_updated = db.Column(db.DateTime(timezone=True), server_default=func.now(), onupdate=func.now())